
# Standard Library: malloc

___malloc:
	lw $v0, static_data_end
	addu $a0, $a0, 3
	sra $a0, $a0, 2
	sll $a0, $a0, 2
	addu $a0, $v0, $a0
	sw $a0 static_data_end 
	jr $ra

#====end of malloc===========
