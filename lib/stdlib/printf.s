# Standard Library: printf
	.set noat
___printf:
	or $at, $0, $a0
	addiu $v1, $sp, 4
	j printf_loop
	
printf_exec:
	xori $t0, $a0, '%'
	bne $t0, $0, printf_plain
	
	lb $a0, 0($at)
	addiu $at, $at, 1
	beq $a0, $0, printf_end
	
	xori $t0, $a0, '%'
	beq $t0, $0, printf_plain
	
	xori $t0, $a0, 'd'
	ori $v0, $0, 1
	beq $t0, $0, printf_print_obj
	
	xori $t0, $a0, 'c'
	ori $v0, $0, 11
	beq $t0, $0, printf_print_obj
	
	xori $t0, $a0, 's'
	ori $v0, $0, 4
	beq $t0, $0, printf_print_obj
	
	xori $t0, $a0, '0'
	beq $t0, $0, printf_print_int_with_width
	
	j printf_plain
	
printf_print_int_with_width:
	
	lb $a0, 0($at)
	addiu $at, $at, 1
	beq $a0, $0, printf_end
	
	addiu $a0, $a0, -48
	bltz $a0, printf_end
	slti $t0, $a0, 10
	beq $t0, $0, printf_end		#error
	
	lb $t0, 0($at)
	addiu $at, $at, 1
	beq $t0, $0, printf_end
	
	xori $t0, $t0, 'd'
	bne $t0, $0, printf_end
	
	lw $t0, 0($v1)
	
	lui $v0, 0xF000
	xor $v0, $t0, $v0
	beq $v0, $0, printf_print_int_with_width_U2147483648
	
	slt $v0, $t0, $0
	beq $v0, $0, printf_print_int_with_width_non_neg
	
	subu $t0, $0, $t0;
	addiu $a0, $a0, -1
	sw $t0, 0($v1);		
	
	or $t0, $a0, $0
	addiu $v0, $0, 11
	addiu $a0, $0, 45
	syscall
	
	or $a0, $t0, $0
	
printf_print_int_with_width_non_neg:
	
	lw $t0, 0($v1)
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 9
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 99
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	addiu $v0, $0, 9999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x1
	or $v0, $0, 0x869F 		#99999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0xF
	or $v0, $0, 0x423F 		#999999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x98
	or $v0, $0, 0x967F 		#9999999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x5F5
	or $v0, $0, 0xE0FF 		#99999999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	lui $v0, 0x3B9A
	or $v0, $0, 0xC9FF 		#999999999
	slt $v0, $v0, $t0
	beq $v0, $0, printf_print_int_with_width_print_padding_zeros
	
	addiu $a0, $a0, -1
	
printf_print_int_with_width_print_padding_zeros:
	
	or $t0, $a0, $0
	ori $a0, $0, 48
	or $v0, $0, 11
	
	blez $t0, printf_print_int_with_width_print_padding_zeros_loop_end
printf_print_int_with_width_print_padding_zeros_loop:
	syscall
	addiu $t0, $t0, -1
	bgtz $t0, printf_print_int_with_width_print_padding_zeros_loop
	
printf_print_int_with_width_print_padding_zeros_loop_end:
	
	lw $a0, 0($v1)
	addiu $v1, $v1, 4
	or $v0, $0, 1
	syscall
	
	j printf_loop
	
printf_print_int_with_width_U2147483648:

	addiu $v1, $v1, 4
	or $a0, $0, $t0
	ori $v0, $0, 1
	syscall

	j printf_loop

printf_print_obj:	
	lw $a0, ($v1)
	addiu $v1, $v1, 4
	syscall
	j printf_loop
	
printf_plain:
	ori $v0, $0, 11
	syscall
	
printf_loop:
	lb $a0, 0($at)
	addiu $at, $at, 1
	bne $a0, $0, printf_exec
	
printf_end:
	jr $ra

#====end of printf===========
