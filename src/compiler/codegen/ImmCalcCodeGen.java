package compiler.codegen;

import java.io.PrintWriter;

public class ImmCalcCodeGen {

	public static void loadStoreGenerator(String op, String reg, int offset,
			String locReg, PrintWriter out) {
		// TODO Auto-generated method stub
		// '$at' might be used
		out.printf("\t%s %s, %d(%s)\n", op, reg, offset, locReg);
	}

	public static void adduCodeGen(String dest, String src, int offset,
			PrintWriter out) {
		// TODO Auto-generated method stub
		// '$at' might be used
		out.printf("\taddu %s, %s, %d\n", dest, src, offset);
	}

}
