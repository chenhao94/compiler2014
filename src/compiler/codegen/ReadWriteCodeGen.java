package compiler.codegen;

import java.io.PrintWriter;

import compiler.IR.CheckSimple;
import compiler.IR.VarInfo;
import compiler.IR.BoundVar;
import compiler.IR.OffsettedVar;
import compiler.registerAllocation.Register;
import compiler.semantics.RecordType;

public class ReadWriteCodeGen {

	public static void registerReadGenerator(VarInfo dest, String reg, int size, PrintWriter out)
	{
		// TODO write value of a register to a simple varInfo 'dest'
		// '$at', '$v0' and '$v1' might be used
		if (CheckSimple.notSimple(dest))
			throw new RuntimeException("improper usage of registerReadGenerator");
		
		if (!dest.isSpilled() && !dest.isArg()  && !dest.isReturnValue())
		{
				Register register =  dest.getReg();
				VarInfo tmpVar = register.getVar();
				if (tmpVar == null)
					register.use(dest);
				else if ( !tmpVar.equals(dest) )
					register.use(dest);
				if (!reg.equals(register.getName()))
					out.printf("\txor %s, %s, $0\n", register.getName(), reg);
				return;
		}
		
		if (reg.equals("$at"))
		{
			out.println("\taddu $v1, $at, $0");
			reg = "$v1";
		}
		String op = "sw";
		int loc =dest.getLoc();
		if (size == 0)
			size = dest.getSize();
		if (size == 1)
			op = "sb";
		if (dest.getArgNum() < 0) // not an argument
			if (loc < 0x10000000) // stack
				if (dest instanceof BoundVar)
				{
					String locReg;
					if (reg.equals("$v0"))
						locReg = "$v1";
					else
						locReg = "$v0";
					locReg = RegMemCodeGen.getReg(((BoundVar)dest).getTarget(), locReg, out);
					ImmCalcCodeGen.loadStoreGenerator(op, reg, 0, locReg, out);
					return;
				}
				else
				{
					ImmCalcCodeGen.loadStoreGenerator(op, reg, loc, "$fp", out);
					return;
				}
			else // heap
			{
				loc = loc - 0x10008000;
				
				out.printf("\t%s %s, %d($gp)\n", op, reg, loc);
				return;
			}
		else // argument
			if (dest.getArgNum() < 4)
				out.printf("\txor $a%d, %s, $0\n", dest.getArgNum(), reg);
			else
			{
				out.printf("\tsw %s, %d($fp)\n", reg, dest.getArgNum() * 4);
				return;
			}
	}
		
	public static void writeGenerator(VarInfo dest, Object src, int size, int offset, PrintWriter out/*, int x*/) {
		// TODO write a char, int, string or simple VarInfo 'src', to 'dest' with offset
		// '$at', '$v0' and '$v1' might be used
		if (CheckSimple.notSimple(src))
			throw new RuntimeException("improper usage of writeGenerator");
		
		if (!dest.isSpilled() && !dest.isArg() && !dest.isReturnValue())
		{
			if (offset != 0)
				throw new RuntimeException("something wrong in register allocation");
			Register register =  dest.getReg();
			VarInfo tmpVar = register.getVar();
				
			if (tmpVar == null)
				register.use(dest);
			else if ( !tmpVar.equals(dest) )
				register.use(dest);
				
			String reg = null;
			if (src instanceof Integer)
				reg = RegMemCodeGen.getReg(src, register.getName(), out);
			else
				reg = RegMemCodeGen.getReg(src, "$v0", out);
			
			if (!reg.equals(register.getName()))
				out.printf("\txor %s, %s, $0\n", register.getName(), reg);
			return;
		}
		
		while (dest instanceof OffsettedVar)
		{
			if (offset != 0)
				throw new RuntimeException("Cannot have a offsetted var and offset at the same time");
			offset = ((OffsettedVar)dest).getOffset();
			dest = ((OffsettedVar)dest).getBasicVar();
		}
		if (size == 0)
			size = ((VarInfo)dest).getSize();
		if (src instanceof Character)
			src = (Integer)(int)((Character)src).charValue();
		
		int loc = dest.getLoc() + offset;
		String op = "sw", reg = null;
		if (size == 0)
			size = dest.getSize();
		if (size == 1)
			op = "sb";
		if (dest.getArgNum() < 0) // not an argument
			if (loc < 0x10000000) // stack
				if (dest instanceof BoundVar)
					if (!(src instanceof VarInfo) || !(((VarInfo)src).getType() instanceof RecordType))
					{
						reg = RegMemCodeGen.getReg(src, "$v0", out);
						String locReg = RegMemCodeGen.getReg(((BoundVar)dest).getTarget(), "$at", out);
						ImmCalcCodeGen.loadStoreGenerator(op, reg, offset, locReg, out);
						return;
					}
					else
					{
						reg = RegMemCodeGen.getReg(src, "$v0", out);
						String locReg = RegMemCodeGen.getReg(((BoundVar)dest).getTarget(), "$v1", out);
						RegMemCodeGen.memCpy(locReg, reg, ((VarInfo)src).getSize(), out);
					}
				else
				{
					if (dest.getType() instanceof RecordType &&
							src instanceof VarInfo &&
							((VarInfo)src).getType() instanceof RecordType)
						RegMemCodeGen.memCpy((VarInfo)src, "$fp", loc, out);
					else
					{
						reg = RegMemCodeGen.getReg(src, "$v0", out);
						ImmCalcCodeGen.loadStoreGenerator(op, reg, loc, "$fp", out);
					}
					return;
				}
			else // heap
			{
				offset = loc - 0x10008000;
				if (dest.getType() instanceof RecordType &&
						src instanceof VarInfo &&
						((VarInfo)src).getType() instanceof RecordType)
					RegMemCodeGen.memCpy((VarInfo)src, "$gp", loc, out);
				else
				{
					reg = RegMemCodeGen.getReg(src, "$v0", out);
					out.printf("\t%s %s, %d($gp)\n", op, reg, offset);
				}
				return;
			}
		else if (dest.getType() instanceof RecordType &&
				src instanceof VarInfo &&
				((VarInfo)src).getType() instanceof RecordType) // record
			if (dest.getArgNum() > 3) // not in register
			{
				out.printf("\tlw $at, %d($fp)\n", dest.getArgNum() * 4);
				RegMemCodeGen.memCpy((VarInfo)src, "$fp", dest.getArgNum() * 4 + offset, out);
				return;
			}
			else
			{
				RegMemCodeGen.memCpy((VarInfo)src, "$a" + dest.getArgNum(), offset, out);
				return;
			}
		else
		{
			if (dest.getArgNum() > 3) // not in register
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\t%s %s, %d($fp)\n", op, reg, dest.getArgNum() * 4);
				return;
			}
			else
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\txori $a%d, %s, 0\n", dest.getArgNum(), reg);
				return;
			}
		}
	}
	
	public static void readGenerator(VarInfo dest, Object object, int size, PrintWriter out)
	{
		// TODO read from 'object' to a simple varInfo: 'dest',
		// '$at', '$v0', '$v1' might be used
		if (!dest.isSimple())
			throw new RuntimeException("improper usage of readGenerator");
		if (size == 0)
			size = dest.getSize();
		if (object instanceof Integer)
			writeGenerator(dest, ((Integer)object).intValue(), size, 0, out);
		else if (object instanceof Character)
			writeGenerator(dest, (int)((Character)object).charValue(), size, 0, out);
		else if (object instanceof VarInfo)
			readGenerator(dest, (VarInfo)object, size, out);
		else
			throw new RuntimeException("improper usage of readGenerator");
	}
	
	private static void readGenerator(VarInfo dest, VarInfo src, int size, PrintWriter out) {
		// TODO read from 'src' to a simple varInfo 'dest'
		// '$at', '$v0', '$v1' might be used
		int loc = dest.getLoc();
		
		if (!dest.isSpilled() && !dest.isArg() && !dest.isReturnValue())
		{
			Register register =  dest.getReg();
			VarInfo tmpVar = register.getVar();
			String reg = RegMemCodeGen.getReg(src, "$v0", out);
			
			if (tmpVar == null)
				register.use(dest);
			else if ( !tmpVar.equals(dest) )
				register.use(dest);
			if (!reg.equals(register.getName()))
				out.printf("\txor %s, %s, $0\n", register.getName(), reg);
			return;
		}
		
		String op = "sw", reg = null;
		if (size == 0)
			size = dest.getSize();
		if (size == 1)
			op = "sb";
		if (dest.getArgNum() < 0) // not an argument
			if (loc < 0x10000000) // stack
				{
					//out.printf("\t%s %s, %d($fp)\n", op, reg, loc);
					if (dest.getType() instanceof RecordType)
						RegMemCodeGen.memCpy(src, "$fp", loc, out);
					else
					{
						reg = RegMemCodeGen.getReg(src, "$v0", out);
						ImmCalcCodeGen.loadStoreGenerator(op, reg, loc, "$fp", out);
					}
					return;
				}
			else // heap
			{
				loc -= 0x10008000;
				if (dest.getType() instanceof RecordType &&
						((VarInfo)src).getType() instanceof RecordType)
					RegMemCodeGen.memCpy(src, "$gp", loc, out);
				else
				{
					reg = RegMemCodeGen.getReg(src, "$v0", out);
					out.printf("\t%s %s, %d($gp)\n", op, reg, loc);
				}
				return;
			}
		else if (dest.getType() instanceof RecordType &&
				((VarInfo)src).getType() instanceof RecordType) // record
			if (dest.getArgNum() > 3) // not in register
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\tlw $v1, %d($fp)\n", dest.getArgNum() * 4);
				RegMemCodeGen.memCpy("$v1", reg, dest.getSize(), out);
				return;
			}
			else
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\t%s %s, %d($a%d)\n", op, reg, loc, dest.getArgNum());
				RegMemCodeGen.memCpy("$a" + dest.getArgNum(), reg, dest.getSize(), out);
				return;
			}
		else // argument
		{
			if (dest.getArgNum() > 3) // not in register
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\t%s %s, %d($fp)\n", op, reg, dest.getArgNum() * 4);
				return;
			}
			else
			{
				reg = RegMemCodeGen.getReg(src, "$v0", out);
				out.printf("\txori $a%d, %s, 0\n", dest.getArgNum(), reg);
				return;
			}
		}
	}

}
