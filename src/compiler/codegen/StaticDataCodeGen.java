package compiler.codegen;

import java.io.PrintWriter;

import compiler.IR.GlobalIRs;
import compiler.IR.IR;
import compiler.IR.Quad;
import compiler.IR.StringIR;
import compiler.IR.StringTransform;
import compiler.IR.VarLabel;

public class StaticDataCodeGen {

	public static void generator(PrintWriter out) {
		// TODO Auto-generated method stub
		int addr;
		
		for (IR ir : GlobalIRs.getInstance().block)
		{
			if (ir instanceof VarLabel)
			{
				out.println("\t.globl $" + ((VarLabel)ir).name);
				addr = MemAssign.staticMemAssign(((VarLabel)ir).varInfo, ((VarLabel)ir).align);
				((VarLabel)ir).varInfo.spill();
				out.println("\t.data 0x" + Integer.toHexString(addr));
				out.println("\t.align 0");
				out.println("$" + ((VarLabel)ir).name + ":");
			}
			else if (ir instanceof Quad)
			{
				if (((Quad)ir).op.equals("zero"))
					out.println("\t.space " + ((Quad)ir).arg1);
				else if (((Quad)ir).op.equals("writebyte"))
					out.println("\t.byte " + (int)((Character)((Quad)ir).arg1).charValue());
				else if (((Quad)ir).op.equals("writeword"))
					out.println("\t.word " + ((Quad)ir).arg1);
			}
			else if (ir instanceof StringIR)
			{
				addr = MemAssign.staticMemAssign((StringIR)ir);
				out.println("\t.data 0x" + Integer.toHexString(addr));
				out.println("\t.align 0");
				out.println("$LC" + ((StringIR)ir).getNum() + ":");
				out.println("\t.asciiz " + StringTransform.toExternalRepresentation(((StringIR)ir).getString()));
			}
		}
		
		addr = GlobalIRs.addr;
		if (addr % 4 != 0)
			addr += 4 - addr % 4;
		out.println("\t.globl static_data_end");
		out.println("\t.data 0x" + Integer.toHexString(addr));
		out.println("static_data_end: ");
		addr += 4;
		out.println("\t.word " + addr);
	}

}
