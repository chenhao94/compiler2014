package compiler.codegen;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class StdlibCodeGen {

	public static void generator(PrintWriter out) {
		// TODO Auto-generated method stub
		String[] libs = {
				"/stdlib/main.s",
				"/stdlib/printf.s",
				"/stdlib/printstring.s",
				"/stdlib/malloc.s"
				};
		String addr;
		StringBuilder builder = new StringBuilder();
		char[] buffer = new char[1024];
		
		for (String filename: libs)
		{
			addr = StdlibCodeGen.class.getResource(filename).getPath();
			//System.err.println(addr);
			
			try {
				FileReader input = new FileReader(addr);
				int cnt = -1;
				while ((cnt = input.read(buffer, 0, 1024)) != -1){
					//addr = buffer.toString();
					builder.append(buffer, 0, cnt);
				}
				input.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		out.println(builder);
	}

}
