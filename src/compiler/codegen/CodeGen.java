package compiler.codegen;

import java.io.IOException;
import java.io.PrintWriter;

import compiler.IR.Funcs;
import compiler.IR.Function;
import compiler.IR.IRGenerator;
import compiler.registerAllocation.RegisterPool;
import compiler.semantics.CheckCode;

public class CodeGen {
	
	public static boolean generator(String filename, String target) throws IOException
	{	
		if (!IRGenerator.generator(filename))
			return false;
		if (target == null)
			target = CheckCode.nameWithoutExt + ".s";
		PrintWriter out = new PrintWriter(target);
		StaticDataCodeGen.generator(out);
		out.println("\t.text");
		StdlibCodeGen.generator(out);
		RegisterPool.init();
		for (Function func: Funcs.funcs.values())
		{
			compiler.basicblocks.BlocksGen.generator(func);
			do {
				compiler.basicblocks.LivelinessAnalysis.analysis(func);
				compiler.optimization.ConstPropagation.imply(func);
			}
			while (compiler.optimization.DeadCodeElimination.elimination(func));
			//optimization()
			compiler.registerAllocation.RegisterAllocation.allocate(func);
			//compiler.basicblocks.BlocksGen.printNodes(func);
			compiler.basicblocks.BlocksGen.merge(func);
			MemAssign.memAssign(func);
		}
		compiler.registerAllocation.RegisterUse.analysis();
		compiler.optimization.TailRecursion.imply();
		
		for (Function func: Funcs.funcs.values())
		{
			func.frameSize += 80 + 4 * func.maxCalleeParams;
			if (func.frameSize % 4 != 0)
				func.frameSize += 4 - func.frameSize % 4;
		}
		for (Function func: Funcs.funcs.values())
			FunctionCodeGen.generator(func, out);
		out.close();
		
		return true;
	}

}
