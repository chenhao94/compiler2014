package compiler.codegen;

import java.io.IOException;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String target = null, src = null;
		
		for (int i=0; i < args.length; ++i)
			if (args[i].equals("-o"))
				target = args[i+1];
			else if (isFilename(args[i]))
				src = args[i];
		
		if (!CodeGen.generator(src, target))
			System.exit(1);
		System.exit(0);
	}

	private static boolean isFilename(String string) {
		// TODO Auto-generated method stub
		int len = string.length();
		if (string.charAt(len-2) == '.' && string.charAt(len-1) == 'c')
			return true;
		return false;
	}

}
