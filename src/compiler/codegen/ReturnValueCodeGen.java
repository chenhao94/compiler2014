package compiler.codegen;

import java.io.PrintWriter;

import compiler.IR.Function;
import compiler.IR.VarInfo;

public class ReturnValueCodeGen {

	public static void generator(Object arg1, Function func, PrintWriter out) {
		// TODO Auto-generated method stub
		String reg = null;
		
		if (arg1 instanceof Integer)
		{
			if ((Integer)arg1 == 0)
				out.println("\txor $v0, $0, $0");
			else
				RegMemCodeGen.loadImm("$v0", (Integer)arg1, out);
		}
		else if (arg1 instanceof Character)
		{
			if ((Character)arg1 == 0)
				out.println("\txor $v0, $0, $0");
			else
				RegMemCodeGen.loadImm("$v0", ((Character)arg1).charValue(), out);
		}
		else // VarInfo
			if (((VarInfo)arg1).isReturnValue())
				return;
			else
				if (!((VarInfo)arg1).isInMem()) // put it to $v0 directly
				{	
					reg = RegMemCodeGen.getReg((VarInfo)arg1, "$v0", out);
					if (!reg.equals("$v0"))
						out.printf("\txor $v0, %s, $0\n", reg);
				}
				else // put address to $v0
				{
					reg = RegMemCodeGen.getAddr((VarInfo)arg1, "$v0", 0, out);
					if (!reg.equals("$v0"))
						out.printf("\txor $v0, %s, $0\n", reg);
				}
	}

}
