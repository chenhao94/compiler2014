package compiler.codegen;

import compiler.IR.Function;
import compiler.IR.GlobalIRs;
import compiler.IR.IR;
import compiler.IR.OffsettedVar;
import compiler.IR.Quad;
import compiler.IR.StringIR;
import compiler.IR.VarInfo;

public class MemAssign {
	
	public static void memAssign(Function func)
	{
		for (IR ir: func.block)
			if (ir instanceof Quad)
				if (((Quad)ir).res != null)
				{	
					if (((Quad)ir).res instanceof VarInfo)
						((VarInfo)((Quad)ir).res).allocate(func);
					if (((Quad)ir).arg1 != null && ((Quad)ir).arg1 instanceof VarInfo)
						((VarInfo)((Quad)ir).arg1).allocate(func);
					if (((Quad)ir).arg2 != null && ((Quad)ir).arg2 instanceof VarInfo)
						((VarInfo)((Quad)ir).arg2).allocate(func);
				}
				else if (((Quad)ir).op == "call")
					func.maxCalleeParams = Math.max(func.maxCalleeParams, (int)((Quad)ir).arg1);
	}

	public static void memAssign(VarInfo varInfo, Function func) {
		// TODO Auto-generated method stub
		if (varInfo.getLoc() != -1 || varInfo instanceof OffsettedVar)
			return;
		int loc = func.locStart + func.frameSize + varInfo.getSize();
		if (loc % varInfo.getType().alignSize() != 0)
			loc += varInfo.getType().alignSize() - loc % varInfo.getType().alignSize();
		varInfo.setLoc(-loc);
		func.frameSize = loc - func.locStart;
		
	}
	
	public static int staticMemAssign(VarInfo varInfo, int align)
	{
		int addr = GlobalIRs.addr;
		if (addr % align != 0)
			addr += align - addr % align;
		varInfo.setLoc(addr);
		GlobalIRs.addr = addr + varInfo.getSize();
		
		return addr;
	}

	public static int staticMemAssign(StringIR ir) {
		// TODO Auto-generated method stub
		int addr = GlobalIRs.addr;
		
		if (ir.getLoc() != -1)
			return ir.getLoc();
		ir.setLoc(addr);
		GlobalIRs.addr += ir.getSize() + 1;
		
		return addr;
	}

}
