package compiler.codegen;

import java.io.PrintWriter;

import compiler.IR.BoundVar;
import compiler.IR.CheckSimple;
import compiler.IR.VarInfo;

public class PtrCodeGen {

	public static void addrGen(VarInfo varInfo, int addr, PrintWriter out) {
		// TODO assign addr to a simple varInfo
		// '$at' might be used
		
		if (CheckSimple.notSimple(varInfo))
			throw new RuntimeException("improper usage of addrGen");
		if (addr < 0x10000000) // stack
		{
			ImmCalcCodeGen.adduCodeGen("$at", "$fp", addr, out);
			addrGen(varInfo, "$at", out);
		}
		else // heap
			ReadWriteCodeGen.writeGenerator(varInfo, addr, 4, 0, out);
	}
	
	public static void addrGen(VarInfo varInfo, String addrReg, PrintWriter out)
	{
		if (CheckSimple.notSimple(varInfo))
			throw new RuntimeException("improper usage of addrGen");
		ReadWriteCodeGen.registerReadGenerator(varInfo, addrReg, 4, out);
	}

	public static void ptrStarGen(BoundVar dest, VarInfo ptrVar) {
		// TODO Auto-generated method stub
		dest.bind(ptrVar);
	}

}
