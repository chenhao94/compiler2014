package compiler.codegen;

import java.io.PrintWriter;

import compiler.IR.VarInfo;
import compiler.IR.BoundVar;
import compiler.IR.OffsettedVar;
import compiler.registerAllocation.Register;
import compiler.semantics.ArrayType;
import compiler.semantics.RecordType;

public class RegMemCodeGen {
	
	public static String loadImm(String reg, int i, PrintWriter out)
	{
		if (i == 0)
			return "$0";
		out.println("\tli " + reg + ", " + i);
		return reg;
	}

	public static String getReg(Object object, String reg, PrintWriter out)
	{
		// TODO load the object to a recommended register 'reg', return the loaded register 
		// '$at' might be used
		if (object instanceof VarInfo)
			return getReg((VarInfo)object, reg, out);
		if (object instanceof Integer)
			return loadImm(reg, ((Integer)object).intValue(), out);
		if (object instanceof Character)
			return loadImm(reg, (int)((Character)object).charValue(), out);
		return null;
	}
	
	private static String getReg(VarInfo varInfo, String reg, PrintWriter out) 
	// "reg" is the possible target register ($v0 or $v1)
	// records will get their absolute address into reg
	// '$at' might be used
	{
		if (!varInfo.isSpilled() && !varInfo.isArg() && !varInfo.isReturnValue())
		{
			Register register =  varInfo.getReg();
			VarInfo tmpVar = register.getVar();
			if (tmpVar == null)
				register.use(varInfo);
			else if ( !tmpVar.equals(varInfo) )
				register.use(varInfo);
			return register.getName();
		}
		if (varInfo instanceof OffsettedVar)
			return getReg(((OffsettedVar)varInfo).getBasicVar(), reg, varInfo.getType().getSize(), ((OffsettedVar)varInfo).getOffset(), out);
		if (varInfo.isInMem())
		{	
			if (varInfo.getType() instanceof ArrayType)
			{
				int loc = varInfo.getLoc();
				if (loc < 0x10000000)
				{
					ImmCalcCodeGen.adduCodeGen(reg, "$fp", loc, out);
					return reg;
				}
				else
				{
					loc -= 0x10008000;
					if (loc <= 32767)
						out.printf("\taddu %s, $gp, %d\n", reg, loc);
					else
					{
						loc += 0x10008000;
						reg = RegMemCodeGen.loadImm(reg, loc, out);
					}
					return reg;
				}
				
			}
			else // record
				if (varInfo.getArgNum() < 0) // not an argument
					if (varInfo.getLoc() < 0x10000000) // stack
						if (varInfo instanceof BoundVar)
							return getReg(((BoundVar)varInfo).getTarget(), reg, out);
						else
						{
							ImmCalcCodeGen.adduCodeGen(reg, "$fp", varInfo.getLoc(), out);
							return reg;
						}
					else // heap
					{
						int offset = varInfo.getLoc() - 0x10008000;
						if (-32768 <= offset && offset <= 32767)
							out.printf("\taddu %s, $gp, %d\n", reg, offset);
						else
						{
							offset = varInfo.getLoc();
							ImmCalcCodeGen.adduCodeGen(reg, "$gp", varInfo.getLoc(), out);
						}
						return reg;
					}
				else if (varInfo.getArgNum() > 3) // not in register
				{
					out.printf("\tlw %s, %d($fp)\n", reg, varInfo.getArgNum() * 4);
					return reg;
				}
				else
					return "$a" + varInfo.getArgNum();
		}
		if (varInfo.isReturnValue())
			return "$v0";
		String op = "lw";
		if (varInfo.getSize() == 1)
			op = "lb";
		if (varInfo.getArgNum() < 0) // not an argument
			if (varInfo.getLoc() < 0x10000000) // stack
				if (varInfo instanceof BoundVar)
				{
					String locReg = RegMemCodeGen.getReg(((BoundVar)varInfo).getTarget(), "$at", out);
					out.printf("\t%s %s, (%s)\n", op, reg, locReg);
					return reg;
				}
				else
				{
					ImmCalcCodeGen.loadStoreGenerator(op, reg, varInfo.getLoc(), "$fp", out);
					return reg;
				}
			else // heap
			{
				int offset = varInfo.getLoc() - 0x10008000;
				out.printf("\t%s %s, %d($gp)\n", op, reg, offset);
				return reg;
			}
		else if (varInfo.getArgNum() > 3) // not in register
		{
			out.printf("\t%s %s, %d($fp)\n", op, reg, varInfo.getArgNum() * 4);
			return reg;
		}
		else
			return "$a" + varInfo.getArgNum();
	}
	
	static void regWriteBack(VarInfo varInfo, PrintWriter out) {
		// TODO write the value in register of varInfo back to memory
		String op = "sw", reg = varInfo.getReg().getName();
		if (varInfo.getSize() == 1)
			op = "sb";
		ImmCalcCodeGen.loadStoreGenerator(op, reg, varInfo.getLoc(), "$fp", out);
	}

	public static String getReg(VarInfo varInfo, String reg, int size, int offset, PrintWriter out) {
		// TODO load the object from address (varInfo + (int)offset) to a recommended register 'reg'
				// return the loaded register
				// '$at' might be used
		if (!varInfo.isInMem())
			return getReg(varInfo, reg, out);
		
		String op = "lw";
		if (size == 1)
			op = "lb";
		int loc = varInfo.getLoc();
		
		if (varInfo.isReturnValue())
			out.printf("\t%s %s, %d($v0)\n", op, reg, offset);
		else if (varInfo.isArg())
			if (varInfo.getArgNum() < 4)
				out.printf("\t%s %s, %d($a%d)\n", op, reg, offset, varInfo.getArgNum());
			else
			{
				out.printf("\tlw %s, %d($fp)\n", op, reg, varInfo.getArgNum() * 4);
				out.printf("\t%s %s, %d(%s)\n", op, reg, offset, reg);
			}
		else
			if (loc < 0x10000000) // stack
				if (varInfo instanceof BoundVar)
				{
					String locReg = RegMemCodeGen.getReg(((BoundVar)varInfo).getTarget(), "$at", out);
					ImmCalcCodeGen.loadStoreGenerator(op, reg, offset, locReg, out);
					return reg;
				}
				else
				{
					ImmCalcCodeGen.loadStoreGenerator(op, reg, loc + offset, "$fp", out);
					return reg;
				}
			else // heap
			{
				offset += loc - 0x10008000;
				out.printf("\t%s %s, %d($gp)\n", op, reg, offset);
			}
		return reg;
	}

	public static String getAddr(VarInfo varInfo, String reg, int offset, PrintWriter out)
	{
		// TODO load the absolute address of varInfo to 'reg'
		// return 'reg'
		// '$at' might be used
		if (varInfo instanceof BoundVar)
			return RegMemCodeGen.getReg(((BoundVar)varInfo).getTarget(), reg, out);
		
		int loc = varInfo.getLoc() + offset;
		
		if (varInfo.getType() instanceof RecordType)
			if (varInfo.isArg())
			{
				if (varInfo.getArgNum() < 4)
					if (offset == 0)
						return "$a" + varInfo.getArgNum();
					else
					{
						out.printf("\taddu %s, $a%d, %d\n", reg, varInfo.getArgNum(), offset);
						return reg;
					}
				else
				{
					out.printf("\tlw %s, %d($fp)\n", reg, 4 * varInfo.getArgNum());
					if (offset != 0)
						out.printf("\taddu %s, %s, %d\n", reg, reg, offset);
					return reg;
				}
			}
			else if (varInfo.isReturnValue())
			{
				if (offset == 0)
					return "$v0";
				out.printf("\taddu %s, $v0, %d\n", reg, offset);
				return reg;
			}
		
		if (loc < 0x10000000) // stack
		{
			ImmCalcCodeGen.adduCodeGen(reg, "$fp", loc, out);
			return reg;
		}
		else // heap
		{
			offset = loc - 0x10008000;
			
			if (-32768 <= offset && offset <= 32767)
				out.printf("\taddu %s, $gp, %d\n", reg, offset);
			else
				loadImm(reg, offset, out);
			return reg;
		}	
	}

	public static void memCpy(VarInfo varInfo, String baseReg, int addr, PrintWriter out)
	{
		// TODO copy the whole content of 'varInfo' to address 'addr'
		// '$at', '$v0' and '$v1' might be used.
		// baseReg is the register '$sp'/'$fp' you will used to find the absolute address
		int size = varInfo.getSize();
		if (varInfo.isSimple())
		{
			if (varInfo.isArg())
				if (varInfo.getArgNum() < 4)
					out.printf("\txor $v0, $a%d, $0\n", varInfo.getArgNum());
				else
					out.printf("\tlw $v0, %d($fp)\n", 4 * varInfo.getArgNum());
			else if (!varInfo.isReturnValue())
				getAddr(varInfo, "$v0", 0, out);
			// address of return value has been already in '$v0' (if need memcpy) 
		}
		else if (varInfo instanceof BoundVar)
			getReg(((BoundVar)varInfo).getTarget(), "$v0", out);
		else // OffsettedVar
			getAddr(((OffsettedVar)varInfo).getBasicVar(), "$v0", ((OffsettedVar)varInfo).getOffset(), out);
		String addrReg = loadImm("$v1", addr, out);
		out.printf("\taddu $v1, %s, %s\n", addrReg, baseReg);
		memCpy("$v1", "$v0", size, out);
	}
	
	public static void memCpy(String tarReg, String srcReg, int size, PrintWriter out)
	{
		// TODO copy the 'size'-sized content of address 'tarReg' to address 'srcReg'
				// '$at' might be used.
				// 'tarReg' and 'srcReg' cannot be '$at'
		int pos = size - size % 4;
		for (int i = 0; i < size / 4; ++i)
		{
			out.printf("\tlw $at %d(%s)\n", 4 * i, srcReg);
			out.printf("\tsw $at %d(%s)\n", 4 * i, tarReg);
		}
		if (size % 4 != 0 && size % 2 == 0)
		{
			out.printf("\tlh $at %d(%s)\n", pos, srcReg);
			out.printf("\tsh $at %d(%s)\n", pos, tarReg);
			pos += 2;
		}
		if (size % 2 != 0)
		{
			out.printf("\tlb $at %d(%s)\n", pos, srcReg);
			out.printf("\tsb $at %d(%s)\n", pos, tarReg);
		}
	}

	public static String hasReg(VarInfo varInfo) {
		// TODO Auto-generated method stub
		if (!varInfo.isSpilled() && !varInfo.isArg() && !varInfo.isReturnValue())
		{
			Register register =  varInfo.getReg();
			VarInfo tmpVar = register.getVar();
			if (tmpVar == null)
				register.use(varInfo);
			else if ( !tmpVar.equals(varInfo) )
				register.use(varInfo);
			return register.getName();
		}
		else if (varInfo.isReturnValue())
			return "$v0";
		else if (varInfo.isArg() && varInfo.getArgNum() < 4)
			return "$a" + varInfo.getArgNum();
		
		return null;
	}
}
