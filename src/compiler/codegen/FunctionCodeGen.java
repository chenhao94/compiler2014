package compiler.codegen;

import java.io.PrintWriter;
import java.util.Map.Entry;

import compiler.IR.BoundVar;
import compiler.IR.Funcs;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.OffsettedVar;
import compiler.IR.StringIR;
import compiler.IR.Label;
import compiler.IR.Quad;
import compiler.IR.VarInfo;
import compiler.registerAllocation.Register;
import compiler.registerAllocation.RegisterPool;

public class FunctionCodeGen {
	
	public static int quadID;

	public static void generator(Function func, PrintWriter out) {
		// TODO Auto-generated method stub
		int paramNum = 0, argPos = 80;
		out.println("\t.align 2");
		out.println("\t.set noat");
		out.println("\t.globl ___" + func.name);
		out.println("___" + func.name + ":");
		
		for (Entry<Integer, Register> entry: RegisterPool.registerPool.entrySet())
			entry.getValue().release();
		
		out.println("\t\t\t\t\t\t# begin of " + func.name);
		out.println("\tsubu $sp, $sp, " + func.frameSize);
		out.println("\tsw $31, " + (func.frameSize - 4) + "($sp)");
		out.println("\tsw $fp, " + (func.frameSize - 8) + "($sp)");
		out.println("\taddu $fp, $sp, " + func.frameSize);
		
		out.println("\n___" + func.name + "_tail:");
	
		for (IR ir: func.block)
		{
			if (ir instanceof Label)
				out.println("$L" + ((Label)ir).num + ": ");
			else // Quad
			{
				quadID = ((Quad)ir).getID();
				out.println("\t\t\t\t\t\t# " + ir.toString());
				if (((Quad)ir).op.equals("param"))
				{
					String reg = null;
					int size, align;
					if (((Quad)ir).arg1 instanceof VarInfo)
					{
						VarInfo para = (VarInfo)((Quad)ir).arg1;
						if (para.isInMem())
						{
							size = para.getSize();
							align = para.getType().alignSize();
							argPos += size;
							if (argPos % align != 0)
								argPos += align - argPos % align;
							RegMemCodeGen.memCpy(para, "$sp", -argPos, out);
							out.println("\taddu $v1, $sp, " + -argPos);
							out.printf("\tsw $v1, %d($sp)\n", paramNum * 4);
						}
						else
						{
							reg = RegMemCodeGen.getReg(para, "$v1", out);
							out.printf("\tsw %s, %d($sp)\n", reg, paramNum * 4);
						}
					}
					else
					{
						int value = 0;
						if (((Quad)ir).arg1 instanceof Integer)
							value = (Integer)((Quad)ir).arg1;
						else if (((Quad)ir).arg1 instanceof Character)
							value = (int)((Character)((Quad)ir).arg1).charValue();
						reg = RegMemCodeGen.loadImm("$v1", value, out);
						out.printf("\tsw %s, %d($sp)\n", reg, paramNum * 4);
					}
					++paramNum;
				}
				else if (((Quad)ir).op.equals("call"))
				{
					String funcName = (String)(((Quad)ir).arg2.toString()).substring(1);
					if (funcName.equals("printf") && (int)((Quad)ir).arg1 == 1)
					{
						if (func.argNum > 0)
							out.println("\tsw $a0, ($fp)");
						out.println("\tlw $a0, ($sp)");
						out.println("\tjal ___printstring");
						if (func.argNum > 0)
							out.println("\tlw $a0, ($fp)");
						paramNum = 0; 
						argPos = 80;
						continue;
					}
					if (funcName.equals("printf"))
					{	
						if (func.argNum > 0)
							out.println("\tsw $a0, ($fp)");
						out.println("\tlw $a0, ($sp)");
						out.println("\tsw $t0, -12($fp)");
					}
					else if (funcName.equals("malloc"))
					{
						if (func.argNum > 0)
							out.println("\tsw $a0, ($fp)");
						out.println("\tlw $a0, ($sp)");
					}
					else
					{
						for (int i = 0; i < 4 && i < paramNum; ++i)
						{	
							if (func.argNum > i)
								out.printf("\tsw $a%d, %d($fp)\n", i, i*4);
							out.printf("\tlw $a%d, %d($sp)\n", i, i * 4);						
						}
						for (Register register: RegisterPool.registerPool.values())
							if (register.getVar() != null && 
										register.getVar().getEnd() > quadID && 
										Funcs.funcs.get(funcName).uses(register))
								out.printf("\tsw %s, %d($fp)\n", register.getName(), -8 - 4 * register.getID());
					}
					out.println("\tjal ___" + funcName);
					if (funcName.equals("printf"))
					{
						if (func.argNum > 0)
							out.printf("\tlw $a0, ($fp)\n");
						out.println("\tlw $t0, -12($fp)");
					}
					else if (funcName.equals("malloc"))
					{
						if (func.argNum > 0)
							out.printf("\tlw $a0, ($fp)\n");
					}
					else
					{
						for (int i = 0; i < 4 && i < paramNum; ++i)
							if (func.argNum > i)
								out.printf("\tlw $a%d, %d($fp)\n", i, i * 4);
						for (Register register: RegisterPool.registerPool.values())
							if (register.getVar() != null && 
										register.getVar().getEnd() > quadID && 
										Funcs.funcs.get(funcName).uses(register))
								out.printf("\tlw %s, %d($fp)\n", register.getName(), -8 - 4 * register.getID());
					}
					paramNum = 0; 
					argPos = 80;
				}
				else if (((Quad)ir).op.equals("calltail"))
				{
					String funcName = (String)(((Quad)ir).arg2.toString()).substring(1);
					
					for (int i = 0; /*i < 4 && */i < paramNum; ++i)
					{	
						if (i < 4)
						{	
							out.printf("\tlw $a%d, %d($sp)\n", i, i * 4);
							out.printf("\tsw $a%d, %d($fp)\n", i, i * 4);
						}
						else
						{	
							out.printf("\tlw $v1, %d($sp)\n", i * 4);
							out.printf("\tsw $v1, %d($fp)\n", i * 4);
						}
					}
					
					out.printf("\tsubu $sp, $fp, %d\n", Funcs.funcs.get(funcName).frameSize);
					out.println("\tj ___" + funcName + "_tail");
					
					paramNum = 0; 
					argPos = 80;
				}
				else if (((Quad)ir).op.equals("jump"))
					out.println("\tj $L" + ((Label)((Quad)ir).res).num);
				else if (((Quad)ir).op.equals("readstring"))
				{
					int offset = ((StringIR)((Quad)ir).arg1).getLoc() - 0x10008000;
					if (-32768 <= offset && offset <= 32767)
						out.printf("\taddu $v1, $gp, %d\n", offset);
					else
						RegMemCodeGen.loadImm("$v1", offset + 0x10008000, out);
					ReadWriteCodeGen.registerReadGenerator((VarInfo)((Quad)ir).res, "$v1", 4, out);
				}
				else if (((Quad)ir).op.equals("read"))
					ReadWriteCodeGen.readGenerator((VarInfo)((Quad)ir).res, ((Quad)ir).arg1, 0, out);
				else if (((Quad)ir).op.equals("write"))
					ReadWriteCodeGen.writeGenerator((VarInfo)((Quad)ir).res, ((Quad)ir).arg1, 0, 0, out);
				else if (((Quad)ir).op.equals("writeword"))
					ReadWriteCodeGen.writeGenerator((VarInfo)((Quad)ir).res, 
							((Quad)ir).arg1, 4, 0, out);
				else if (((Quad)ir).op.equals("writebyte"))
					ReadWriteCodeGen.writeGenerator((VarInfo)((Quad)ir).res, 
							((Quad)ir).arg1, 1, 0, out);
				else if (((Quad)ir).op.equals("return"))
				 {
					out.println("\tlw $31, -4($fp)");
					out.println("\tlw $fp, -8($fp)");
					out.println("\taddu $sp, " + func.frameSize);
					out.println("\tjr $31");
				 }
				else if (((Quad)ir).op.equals("returnvalue"))
					ReturnValueCodeGen.generator(((Quad)ir).arg1, func, out);
				else if (((Quad)ir).op.equals("beq") || ((Quad)ir).op.equals("bne") || ((Quad)ir).op.equals("blt") ||
										((Quad)ir).op.equals("bgt") ||((Quad)ir).op.equals("ble") || ((Quad)ir).op.equals("bge") ||
										((Quad)ir).op.equals("bnez") || ((Quad)ir).op.equals("beqz") )
					branchGenerator(((Quad)ir).op, (Label)((Quad)ir).res, ((Quad)ir).arg1, ((Quad)ir).arg2, out);
				else if (((Quad)ir).op.equals("addu") || ((Quad)ir).op.equals("subu") || ((Quad)ir).op.equals("mul") ||
										((Quad)ir).op.equals("and") || ((Quad)ir).op.equals("or") || ((Quad)ir).op.equals("xor") || 
										((Quad)ir).op.equals("sllv") || ((Quad)ir).op.equals("srav") ||
										((Quad)ir).op.equals("slt") || ((Quad)ir).op.equals("sltu"))
				{
					String reg1 = null, reg2 = null, reg = null;
					if (((Quad)ir).arg2 instanceof VarInfo && ((VarInfo)((Quad)ir).arg2).isReturnValue())
						if (((Quad)ir).arg1 instanceof VarInfo && ((VarInfo)((Quad)ir).arg1).isReturnValue())
						{
							reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v0", out);
							out.println("\txor $v1, $v0, $0");
							reg1 = "$v1";
							reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v0", out);
						}
						else
						{
							reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v1", out);
							reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v0", out);
						}
					else
					{
						reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v0", out);
						reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v1", out);
					}
					
					reg = RegMemCodeGen.hasReg((VarInfo)((Quad)ir).res);
					if (reg == null)
					{
						out.printf("\t%s $v1, %s, %s\n", ((Quad)ir).op, reg1, reg2);
						ReadWriteCodeGen.registerReadGenerator((VarInfo)((Quad)ir).res, "$v1", ((VarInfo)((Quad)ir).res).getSize(), out);
					}
					else
						out.printf("\t%s %s, %s, %s\n", ((Quad)ir).op, reg, reg1, reg2);
				}
				else if (((Quad)ir).op.equals("xori"))
				{
					String reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v1", out), reg = null;
					reg = RegMemCodeGen.hasReg((VarInfo)((Quad)ir).res);
					if (reg == null)
					{
						out.printf("\t%s $v1, %s, %d\n", ((Quad)ir).op, reg1, (Integer)((Quad)ir).arg2);
						ReadWriteCodeGen.registerReadGenerator((VarInfo)((Quad)ir).res, "$v1", ((VarInfo)((Quad)ir).res).getSize(), out);
					}
					else
						out.printf("\t%s %s, %s, %d\n", ((Quad)ir).op, reg, reg1, (Integer)((Quad)ir).arg2);
				}
				else if (((Quad)ir).op.equals("nor"))
				{
					String reg = RegMemCodeGen.hasReg((VarInfo)((Quad)ir).res);
					if (reg == null)
					{
						out.println("\tnor $v1, " + RegMemCodeGen.getReg(((Quad)ir).arg1, "$v1", out) + ", $0");
						ReadWriteCodeGen.registerReadGenerator((VarInfo)((Quad)ir).res, "$v1", ((VarInfo)((Quad)ir).res).getSize(), out);
					}
					else
						out.println("\tnor " + reg + ", " + RegMemCodeGen.getReg(((Quad)ir).arg1, "$v1", out) + ", $0");
				}
				else if (((Quad)ir).op.equals("divu"))
				{
					String reg1 = null, reg2 = null;
					if (((Quad)ir).arg2 instanceof VarInfo && ((VarInfo)((Quad)ir).arg2).isReturnValue())
						if (((Quad)ir).arg1 instanceof VarInfo && ((VarInfo)((Quad)ir).arg1).isReturnValue())
						{
							reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v0", out);
							out.println("\txor $v1, $v0, $0");
							reg1 = "$v1";
							reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v0", out);
						}
						else
						{
							reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v1", out);
							reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v0", out);
						}
					else
					{
						reg1 = RegMemCodeGen.getReg(((Quad)ir).arg1, "$v0", out);
						reg2 = RegMemCodeGen.getReg(((Quad)ir).arg2, "$v1", out);
					}
					
					out.printf("\tdivu %s, %s\n", reg1, reg2);
				}
				else if (((Quad)ir).op.equals("mflo") || ((Quad)ir).op.equals("mfhi"))
				{	
					String reg = RegMemCodeGen.hasReg((VarInfo)((Quad)ir).res);
					if (reg == null)
					{
						out.printf("\t%s $v1\n", ((Quad)ir).op);
						ReadWriteCodeGen.registerReadGenerator((VarInfo)((Quad)ir).res, "$v1", ((VarInfo)((Quad)ir).res).getSize(), out);
					}
					else
						out.printf("\t%s %s\n", ((Quad)ir).op, reg);
				}
				else if (((Quad)ir).op.equals("addr")) 
				{
					VarInfo arg1 = (VarInfo)((Quad)ir).arg1;
					String reg;
					if (arg1 instanceof BoundVar)
					{
						reg = RegMemCodeGen.getReg(((BoundVar)arg1).getTarget(), "$v0", out);
						PtrCodeGen.addrGen((VarInfo)((Quad)ir).res, reg, out);
					}
					else if (arg1 instanceof OffsettedVar && 
							(((OffsettedVar)arg1).getBasicVar() instanceof BoundVar ||
									((OffsettedVar)arg1).getBasicVar().isRecordArgRet()	))
					{
						if (((OffsettedVar)arg1).getBasicVar() instanceof BoundVar)
							reg = RegMemCodeGen.getReg(((BoundVar)((OffsettedVar)arg1).getBasicVar()).getTarget(), "$v0", out);
						else
							reg = RegMemCodeGen.getReg(((OffsettedVar)arg1).getBasicVar(), "$v0", out);
						int argNum = ((OffsettedVar)arg1).getBasicVar().getArgNum();
						if (-1 < argNum && argNum < 4)
						{
							ImmCalcCodeGen.adduCodeGen("$v0", reg, ((OffsettedVar)arg1).getOffset(), out);
							reg = "$v0";
						}
						else
							ImmCalcCodeGen.adduCodeGen(reg, reg, ((OffsettedVar)arg1).getOffset(), out);
						PtrCodeGen.addrGen((VarInfo)((Quad)ir).res, reg, out);
					}
					else if (arg1.isRecordArgRet())
					{
						reg = RegMemCodeGen.getReg(arg1, "$v0", out);
						PtrCodeGen.addrGen((VarInfo)((Quad)ir).res, reg, out);
					}
					else
					{
						int addr = arg1.getLoc();
						PtrCodeGen.addrGen((VarInfo)((Quad)ir).res, addr, out);
					}
				}
				else if (((Quad)ir).op.equals("ptrstar"))
				{
					PtrCodeGen.ptrStarGen((BoundVar)((Quad)ir).res, (VarInfo)((Quad)ir).arg1);
				}
				else
					throw new RuntimeException("Wrong spelling somewhere in quads.");
			}
		}
		
		out.println("\t\t\t\t\t\t# endof " + func.name); 
		out.println("\tlw $31, -4($fp)");
		out.println("\tlw $fp, -8($fp)");
		out.println("\taddu $sp, " + func.frameSize);
		out.println("\tjr $31");
	}

	private static void branchGenerator(String op, Label res, Object arg1,
			Object arg2, PrintWriter out) {
		// TODO Auto-generated method stub
		
		String v0 = null, v1 = null;
		
		v0 = RegMemCodeGen.getReg(arg1, "$v0", out);
		
		if (arg2 != null)
			v1 = RegMemCodeGen.getReg(arg2, "$v1", out);
			
		out.printf("\t%s %s", op, v0);
		if (v1 != null)
			out.printf(", %s", v1);
		out.printf(", $L%d\n", res.num);
	}

}
