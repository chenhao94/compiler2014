package compiler.syntax;

%%

%unicode
%cup
//%cupdebug
%line
%column
%implements sym

%{
	StringBuffer string = new StringBuffer();

	private java_cup.runtime.Symbol tok(int type) {
		return new java_cup.runtime.Symbol(type, yyline, yycolumn);
	}

	private java_cup.runtime.Symbol tok(int type, Object value) {
		return new java_cup.runtime.Symbol(type, yyline, yycolumn, value);
	}
%}

%eofval{
	return tok(EOF, null);
%eofval}


	WhiteSpace = \R | [ \t\f\v]

	Comment = ( "/*" ~"*/" ) | ( "//" .* \R )

	Include = "#include" ~\R

	Identifier = [:jletter:] [:jletterdigit:]*
	
	DecInt = 0 | [1-9][0-9]*
	OctInt = 0 [0-7]+
	HexInt = 0 [Xx] [0-9A-Fa-f]+

	IntSuffix = ( [uU] ( [lL]? | l{2} | L{2} ) ) | 
				( ( [lL]? | l{2} | L{2} ) [uU]? )
	
%state STR
%state CHAR
%%

<YYINITIAL>
{

	/*  keywords  */

	"typedef"		{ return tok(TYPEDEF); }
	"void"			{ return tok(VOID); }
	"char"			{ return tok(CHAR); }
	"int"			{ return tok(INT); }
	"struct"		{ return tok(STRUCT); }
	"union"			{ return tok(UNION); }
	"if"			{ return tok(IF); }
	"else"			{ return tok(ELSE); }
	"while"			{ return tok(WHILE); }
	"for"			{ return tok(FOR); }
	"continue"		{ return tok(CONTINUE); }
	"break"			{ return tok(BREAK); }
	"return"		{ return tok(RETURN); }
	"sizeof"		{ return tok(SIZEOF); }

	/*	operators	*/

	"("				{ return tok(LPAR); }
	")"				{ return tok(RPAR); }
	"{"				{ return tok(LBRA); }
	"}"				{ return tok(RBRA); }
	"["				{ return tok(LSQB); }
	"]"				{ return tok(RSQB); }
	";"				{ return tok(SEMI); }
	","				{ return tok(COM); }
	"="				{ return tok(ASS); }
	"*"				{ return tok(STAR); }	//	ptr&num operator
	"|"				{ return tok(BAR); }
	"^"				{ return tok(XOR); }
	"&"				{ return tok(AMP); }
	"+"				{ return tok(PLUS); }
	"-"				{ return tok(MINUS); }
	"/"				{ return tok(DIV); }
	"%"				{ return tok(MOD); }
	"~"				{ return tok(TILD); }
	"!"				{ return tok(EXCL); }
	"."				{ return tok(DOT); }
	"<"				{ return tok(LT); }
	">"				{ return tok(GT); }
	"||"			{ return tok(OR); }
	"&&"			{ return tok(AND); }
	"=="			{ return tok(EQ); }
	"!="			{ return tok(NE); }
	"<="			{ return tok(LE); }
	">="			{ return tok(GE); }
	"<<"			{ return tok(SHL); }
	">>"			{ return tok(SHR); }
	"++"			{ return tok(INC); }
	"--"			{ return tok(DEC); }
	"->"			{ return tok(PTR); }
	"..."			{ return tok(ELPS); }
	"*="			{ return tok(MUAS); }
	"/="			{ return tok(DVAS); }
	"%="			{ return tok(MDAS); }
	"+="			{ return tok(ADDAS); }
	"-="			{ return tok(SBAS); }
	"<<="			{ return tok(SLAS); }
	">>="			{ return tok(SRAS); }
	"&="			{ return tok(ANDAS); }
	"^="			{ return tok(XRAS); }
	"|="			{ return tok(ORAS); }

	/*  Constants  */

	{Identifier} 			{ return tok(ID, yytext()); }
	{DecInt} {IntSuffix}? 	{ return tok(INTEGER, Integer.valueOf(yytext(), 10)); }
	{OctInt} {IntSuffix}? 	{ return tok(INTEGER, Integer.valueOf(yytext(), 8)); }
	{HexInt} {IntSuffix}? 	{ return tok(INTEGER, Integer.valueOf(yytext().substring(2), 16)); }
	\"						{ string.setLength(0); yybegin(STR); }
	'						{ string.setLength(0); yybegin(CHAR); }

	/*  Others  */
	{Comment}				{ /* ignore */ }
	{WhiteSpace}			{ /* ignore */ }
	{Include}				{ /* ignore */ }
}

<STR>
{
	\"				{ yybegin(YYINITIAL); return tok(STRING, string.toString()); }
	[^\"\\\R]+		{ string.append(yytext()); }
	\\'				{ string.append('\''); }
	\\\"			{ string.append('\"'); }
	\\\?			{ string.append('?'); }
	\\\\			{ string.append('\\'); }
//	\\a 			{ string.append('\a'); }
	\\b				{ string.append('\b'); }
	\\f				{ string.append('\f'); }
	\\n				{ string.append('\n'); }
	\\r				{ string.append('\r'); }
	\\t				{ string.append('\t'); }
//	\\v				{ string.append('\v'); }

	[^]				{ throw new RuntimeException("Line " + (yyline + 1) + ":" + (yycolumn + 1) + " : '\"' need to be matched."); }
}

<CHAR>
{
	[^'\\\R]'		{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)(yytext().charAt(0))); }
	\\''			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\'')); }
	\\\"'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\"')); }
	\\\?'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('?')); }
	\\\\'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\\')); }
//	\\a'			{ yybegin(YYINITIAL); 
//					  return tok(CHARACTER, (Character)('\a')); }
	\\b'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\b')); }
	\\f'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\f')); }
	\\n'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\n')); }
	\\r'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\r')); }
	\\t'			{ yybegin(YYINITIAL); 
					  return tok(CHARACTER, (Character)('\t')); }
//	\\v'			{ yybegin(YYINITIAL); 
//					  return tok(CHARACTER, (Character)('\v')); }
	
	\\[0-7]{1,3}'	{ yybegin(YYINITIAL);
					  return tok(CHARACTER, (Character)(char)(Integer.valueOf(yytext().substring(1, yytext().length()-1), 8).intValue()));	}

	\\x[0-9a-fA-F]+'
					{ yybegin(YYINITIAL);
					  return tok(CHARACTER, (Character)(char)(Integer.valueOf(yytext().substring(2, yytext().length()-1), 16).intValue()));}

	[^]				{ throw new RuntimeException("Line " + (yyline + 1) + ":" + (yycolumn + 1) + " : Illegal character."); }
}

/*  Error  */
[^]					{ throw new RuntimeException("Line " + (yyline + 1) + ":" + (yycolumn + 1) + " : Illegal syntax \"" + yytext() + "\""); }
