package compiler.optimization;

import java.util.BitSet;

import compiler.IR.BoundVar;
import compiler.IR.Function;
import compiler.IR.OffsettedVar;
import compiler.IR.Quad;
import compiler.IR.VarInfo;
import compiler.basicblocks.Node;

public class DeadCodeElimination {

	public static boolean elimination(Function func) 
	{
		// return true iff something changed during elimination
		boolean changed = false;
		java.util.BitSet live;
		java.util.ListIterator<Quad> itr;
		Quad quad;
		VarInfo tmpVar;
		
		for (Node node: func.getNode())
		{
			live = (BitSet) node.out.clone();
			itr = node.block.listIterator(node.block.size());
			while (itr.hasPrevious())
			{
				quad = itr.previous();
				
				if (quad.op == null)
				{
					itr.remove();
					changed = true;
					continue;
				}
				
				if (quad.res instanceof VarInfo)
				{
					tmpVar = (VarInfo)quad.res;
					if (!tmpVar.isSpilled() && !tmpVar.isArg())
					{
						if (!live.get(tmpVar.getID()))
						{
							itr.remove();
							changed = true;
							continue;
						}
						live.set(tmpVar.getID(), false);
					}
					if (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
					{
						while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
						{	
							if (tmpVar instanceof OffsettedVar)
								tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
							else
								tmpVar = ((BoundVar)tmpVar).getTarget();
						}
						live.set(tmpVar.getID(), true);
					}
				}
				
				if (quad.arg1 instanceof VarInfo)
				{
					tmpVar = (VarInfo)quad.arg1;
					while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
					{	
						if (tmpVar instanceof OffsettedVar)
							tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
						else
							tmpVar = ((BoundVar)tmpVar).getTarget();
					}
					live.set(tmpVar.getID(), true);
				}
				
				if (quad.arg2 instanceof VarInfo)
				{
					tmpVar = (VarInfo)quad.arg2;
					while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
					{	
						if (tmpVar instanceof OffsettedVar)
							tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
						else
							tmpVar = ((BoundVar)tmpVar).getTarget();
					}
					live.set(tmpVar.getID(), true);
				}
			}
		}
		
		return changed;
	}
}
