package compiler.optimization;

import compiler.IR.Function;
import compiler.IR.Quad;
import compiler.IR.VarInfo;
import compiler.basicblocks.Node;
import compiler.semantics.IntType;

public class ConstPropagation {

	private static java.util.HashSet<VarInfo> CPVars = new java.util.HashSet<VarInfo>();
	private static java.util.HashSet<VarInfo> noCPVars = new java.util.HashSet<VarInfo>();
	
	public static void imply(Function func)
	{
		VarInfo hiVar = VarInfo.newTmpVar(IntType.getInstance(), false);
		VarInfo loVar = VarInfo.newTmpVar(IntType.getInstance(), false);
		int arg1, arg2;
		noCPVars.clear();
		for (Node node: func.getNode())
			for (Quad quad: node.block)
				if (quad.op.equals("addr"))
					noCPVars.add((VarInfo)quad.arg1);
		for (Node node: func.getNode())
		{	
			CPVars.clear();
			
			for (Quad quad: node.block)
			{	
				if (quad.op.equals("ptrstar") || quad.op.equals("addr"))
					continue;
				if (noCPVars.contains(quad.res))
					continue;
				if (quad.res instanceof VarInfo && 
						((VarInfo)quad.res).isSimple())
					CPVars.add((VarInfo)quad.res);
				if (quad.arg1 instanceof VarInfo && (((VarInfo)quad.arg1).isConst()))
					quad.arg1 = ((VarInfo)quad.arg1).getConst();
				if (quad.arg2 instanceof VarInfo && (((VarInfo)quad.arg2).isConst()))
					quad.arg2 = ((VarInfo)quad.arg2).getConst();
				if (quad.op.equals("divu"))
				{
					if (quad.arg1 instanceof Integer && quad.arg2 instanceof Integer)
					{
						arg1 = (Integer)quad.arg1;
						arg2 = (Integer)quad.arg2;
					}
					else
					{
						hiVar.releaseConst();
						loVar.releaseConst();
						continue;
					}
					hiVar.setConst(arg1 % arg2);
					loVar.setConst(arg1 / arg2);
					quad.op = null;
					continue;
				}
				if (!(quad.res instanceof VarInfo) || !((VarInfo)quad.res).isSimple())
					continue;
				if (quad.op.equals("read") || quad.op.equals("write") || quad.op.equals("writeword"))
				{	
					if (quad.arg1 instanceof Integer)
						((VarInfo)quad.res).setConst((Integer)quad.arg1);
					else
						((VarInfo)quad.res).releaseConst();
				}
				else if (quad.op.equals("mflo"))
				{
					if (loVar.isConst())
						((VarInfo)quad.res).setConst(loVar.getConst());
					else
						((VarInfo)quad.res).releaseConst();
				}
				else if (quad.op.equals("mfhi"))
				{
					if (hiVar.isConst())
						((VarInfo)quad.res).setConst(hiVar.getConst());
					else
						((VarInfo)quad.res).releaseConst();
				}
				else
				{
					if (quad.arg1 instanceof Integer && quad.arg2 instanceof Integer)
					{
						arg1 = (Integer)quad.arg1;
						arg2 = (Integer)quad.arg2;
					}
					else
					{
						((VarInfo)quad.res).releaseConst();
						continue;
					}
					if (quad.op.equals("addu"))
						((VarInfo)quad.res).setConst(arg1 + arg2);
					else if (quad.op.equals("subu"))
						((VarInfo)quad.res).setConst(arg1 - arg2);
					else if (quad.op.equals("mul"))
						((VarInfo)quad.res).setConst(arg1 * arg2);
					else if (quad.op.equals("and"))
						((VarInfo)quad.res).setConst(arg1 & arg2);
					else if (quad.op.equals("or"))
						((VarInfo)quad.res).setConst(arg1 | arg2);
					else if (quad.op.equals("xor") || quad.op.equals("xori"))
						((VarInfo)quad.res).setConst(arg1 ^ arg2);
					else if (quad.op.equals("nor"))
						((VarInfo)quad.res).setConst(~arg1 & ~arg2);
					else if (quad.op.equals("sllv"))
						((VarInfo)quad.res).setConst(arg1 << arg2);
					else if (quad.op.equals("srav"))
						((VarInfo)quad.res).setConst(arg1 >> arg2);
					else if (quad.op.equals("slt"))
						((VarInfo)quad.res).setConst(arg1 < arg2 ? 1: 0);
					else if (quad.op.equals("sltu"))
						((VarInfo)quad.res).setConst(getUnsigned(arg1) < getUnsigned(arg2)? 1: 0);
					else
						((VarInfo)quad.res).releaseConst();
					
				}
			}
		
		for (VarInfo varInfo: CPVars)
			varInfo.releaseConst();
		}
	}

	private static long getUnsigned(int x) {
		// TODO Auto-generated method stub
		return ((long)x) & 0xffffffffl;
	}
	
}
