package compiler.optimization;

import compiler.IR.Funcs;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.Quad;
import compiler.IR.VarInfo;

public class TailRecursion {

	public static void imply() {
		// TODO Auto-generated method stub
		java.util.LinkedList<Function> funcQueue = new java.util.LinkedList<Function>();
		Function nowFunc;
		String funcName;
		for (Function func: Funcs.funcs.values())
		{
			int len = func.block.size();
			IR ir, next;
			for (int i = 0; i < len - 1; ++i)
			{
				ir = func.block.get(i);
				if (ir instanceof Quad && ((Quad)ir).op.equals("call"))
				{
					funcName = ((VarInfo)((Quad)ir).arg2).toString().substring(1);
					if (funcName.equals("printf") || funcName.equals("malloc"))
						continue;
					if (Funcs.funcs.get(funcName).cannotTailCall())
						continue;
					next = func.block.get(i + 1);
					if (!(next instanceof Quad))
						continue;
					if (((Quad)next).op.equals("return"))
					{
						((Quad)ir).op = "calltail";
						Funcs.funcs.get(funcName).calledTailBy(func);
						continue;
					}
					if (((Quad)next).op.equals("returnvalue") && 
							((Quad)next).arg1 instanceof VarInfo && 
							((VarInfo)((Quad)next).arg1).isReturnValue())
					{
						next = func.block.get(i + 2);
						if (!(next instanceof Quad))
							continue;
						if (((Quad)next).op.equals("return"))
						{	
							((Quad)ir).op = "calltail";
							Funcs.funcs.get(funcName).calledTailBy(func);
						}
					}
				}
			}
			funcQueue.add(func);
		}
		
		while (!funcQueue.isEmpty())
		{
			nowFunc = funcQueue.poll();
			for (Function caller: nowFunc.getTailedCallers())
				if (caller.maxCalleeParams < nowFunc.maxCalleeParams)
				{
					caller.maxCalleeParams = nowFunc.maxCalleeParams;
					if (!funcQueue.contains(caller))
						funcQueue.add(caller);
				}
		}
	}

}
