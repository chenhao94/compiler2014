package compiler.symbol;

import java.io.PrintWriter;
import java.util.Map;

import compiler.ast.ASTNode;
import compiler.semantics.ArrayType;
import compiler.semantics.CompilationInfo;
import compiler.semantics.StructType;
import compiler.semantics.Type;
import compiler.semantics.VoidType;

public class Env {
	private java.util.HashMap<String, EnvInfo> basicTable;
	private java.util.HashMap<String, EnvInfo> recordTable;
	
	static private java.util.HashMap<String, EnvInfo> stdlibTable = new java.util.HashMap<String, EnvInfo>(); 
	
	private Env next;
	
	public Env(Env env)
	{
		basicTable = new java.util.HashMap<String, EnvInfo>();
		recordTable = new java.util.HashMap<String, EnvInfo>();
		next = env;
	}
	
	public EnvInfo putToBasic(String s, Type t, ASTNode node)
	{
		if (stdlibTable.get(s) != null)
			CompilationInfo.addError("Cannot override the stdlib functions.", node.line, node.column);
		
		if (t.equals(VoidType.getInstance()))
			CompilationInfo.addError("Cannot declare a void type variable", node.line, node.column);
		
		EnvInfo info = basicTable.get(s);
		Type type = null;
		
		if (info != null)
		{
			type = info.type;
			if (this.next == null) // top-level
			{
				if (type instanceof ArrayType)
				{
					if (!(t instanceof ArrayType))
						CompilationInfo.addError("Ambigious redeclaration.", node.line, node.column);
					if (!((ArrayType)t).type.equals(((ArrayType)type).type))
						CompilationInfo.addError("Ambigious redeclaration.", node.line, node.column);
					
					if (!t.isComplete())
						return info;
					else// if (t.isComplete())
					{
						if (type.isComplete())
						{
							if (!t.equals(type))
								CompilationInfo.addError("Ambigious redefinition.", node.line, node.column);
							return info;
						}
						else
							basicTable.remove(s);
					}
				}
				else
				{
					if (!t.equals(type))
						CompilationInfo.addError("Ambigious redeclaration.", node.line, node.column);
					return info;
				}
			}
			else
				CompilationInfo.addError("Redeclaration of variable \'" + s + "\'.", node.line, node.column);
		}
		info = new EnvInfo(s, t, false);
		basicTable.put(s, info);
		return info;
	}
	
	public EnvInfo putToRecord(String s, Type t, ASTNode node)
	{
		if (stdlibTable.get(s) != null)
		{
			CompilationInfo.addError("Cannot override the stdlib functions.", node.line, node.column);
			return recordTable.get(s);
		}
		
		EnvInfo info = recordTable.get(s);
		Type type = null;
		
		if (info != null)
		{
			type = info.type;
			if (!t.isComplete())
			{
				if ((t instanceof StructType) ^ (type instanceof StructType))
					CompilationInfo.addError("Ambigious redeclaration.", node.line, node.column);
				return info;
			}
			else // if (t.isComplete())
			{
				if (type.isComplete())
					CompilationInfo.addError("Ambigious redefinition.", node.line, node.column);
				if ((t instanceof StructType) ^ (type instanceof StructType))
					CompilationInfo.addError("Ambigious redeclaration.", node.line, node.column);
				recordTable.remove(s);
			}
		}
		info = new EnvInfo(s, t, false);
		recordTable.put(s, info);
		return info;
	}
	
	public static void putToStdLib(String s, Type t, ASTNode node) 
	{
		EnvInfo info = stdlibTable.get(s);
		
		if (info != null)
		{
			CompilationInfo.addError("Function in standard library is redefined.", node.line, node.column);
			return;
		}
		info = new EnvInfo(s, t, true);
		stdlibTable.put(s, info);
	}
	
	public EnvInfo findBasic(String s)
	{
		Env env = this;
		EnvInfo tmp = stdlibTable.get(s);
		
		if (tmp != null)
			return tmp;
		
		while (env != null)
		{
			tmp = env.basicTable.get(s);
			if (tmp != null) return tmp;
			env = env.next;
		}
		return null;
	}
	
	public EnvInfo findRecordInThis(String s)
	{
		EnvInfo tmp = recordTable.get(s);
		if (tmp == null)
			return null;
		return tmp;
	}
	
	public EnvInfo findRecordInAll(String s)
	{
		Env env = this;
		EnvInfo tmp = null;
		
		while (env != null)
		{
			tmp = env.recordTable.get(s);
			if (tmp != null) return tmp;
			env = env.next;
		}
		return null;
	}
	
	public void printVariables(PrintWriter out)
	{
		String name;
		EnvInfo info;
		for (Map.Entry<String, EnvInfo> entry: basicTable.entrySet())
		{
			name = entry.getKey();
			info = entry.getValue();
			
			out.printf("%s: %s type: %s, size: %d, align: %d\n", 
					name, info.varInfo.getName(), info.type.toString(),
					info.type.getSize(), info.type.alignSize());
		}
	}
}
