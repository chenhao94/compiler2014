package compiler.symbol;

public class Symbol
{
	private String name;

	private Symbol(String s)
	{
		name = s;
	}

	public String toString()
	{
		return name;
	}

	private static java.util.HashMap<String, Symbol> dict = new java.util.HashMap<String, Symbol>();

	public static Symbol symbol(String n)
	{
		String u = n.intern();
		Symbol s = dict.get(u);

		if (s == null)
		{
			s = new Symbol(u);
			dict.put(u,s);
		}

		return s;
	}
}
