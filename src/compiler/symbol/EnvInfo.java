package compiler.symbol;

import compiler.semantics.Type;

public class EnvInfo
{
	public Type type;
	public boolean init, printed;
	public compiler.IR.VarInfo varInfo;
	
	public EnvInfo(String name, Type t, boolean i)
	{
		boolean inMem = false;
		
		type = t;
		init = i;
		if (!(t.isIntegerType() || t.isPurePtr()))
			inMem = true;
		varInfo = new compiler.IR.VarInfo(name, type, inMem);
		printed = false;
	}
	
	public boolean initialized()
	{
		return init;
	}
}

