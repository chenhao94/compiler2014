package compiler.IR;

import compiler.ast.CmpStmt;
import compiler.ast.Decls;
import compiler.ast.Stmts;
import compiler.symbol.Env;

public class CmpStmtIRGen {

	public static void generator(CmpStmt stmt, Function func, LoopInfo loopInfo) {
		// TODO Auto-generated method stub
		Env env = stmt.env;
		Decls decls = stmt.decls;
		Stmts stmts = stmt.stmts;
		
		while (decls != null)
		{
			DeclIRGen.generator(decls.decl, func, env);
			decls = decls.next;
		}
		
		while (stmts != null)
		{
			StmtIRGen.generator(stmts.stmt, func, env, loopInfo);
			stmts = stmts.next;
		}
		
	}

}
