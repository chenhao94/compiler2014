package compiler.IR;

public class Quad extends IR {
	
	public String op;
	public Object res, arg1, arg2;
	private int ID; // used for liveliness analysis
	
	public Quad(String _op, Object _res, Object _arg1, Object _arg2)
	{
		op = _op;
		res = _res;
		arg1 = _arg1;
		arg2 = _arg2;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String sr = "", sa1 = "", sa2 = "";
		if (res != null)
			sr = " " + res.toString();
		if (arg1 != null)
		{	
			sa1 = " " + arg1.toString();
			if (arg1 instanceof Character)
				sa1 = StringTransform.charToExternalRepresentation(sa1);
		}
		if (arg2 != null)
		{	
			sa2 = " " + arg2.toString();
			if (arg2 instanceof Character)
				sa2 = StringTransform.charToExternalRepresentation(sa2);
		}
		return "\t" + op + sr + sa1 + sa2;
	}
	
	public void setID(int id)
	{
		ID = id;
	}
	
	public int getID()
	{
		return ID;
	}

}
