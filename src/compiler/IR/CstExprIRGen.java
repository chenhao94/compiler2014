package compiler.IR;

import compiler.ast.CstExpr;
import compiler.ast.Expr;
import compiler.semantics.CharType;
import compiler.semantics.Type;
import compiler.symbol.Env;

public class CstExprIRGen {

	public static VarInfo generator(CstExpr expr, IRContainer container, Env env) {
		// TODO Auto-generated method stub
		
		Expr tmpExpr = expr;
		VarInfo tmpVar1, tmpVar2;
		Type type = null;
		boolean charFlag = false;
		
		while (tmpExpr instanceof CstExpr)
		{
			type = tmpExpr.type;
			if (type instanceof CharType)
				charFlag = true;
			tmpExpr = ((CstExpr)tmpExpr).expr;
		}
		
		if (type == null)
			return tmpVar1 = ExprIRGen.generator(expr.expr, container, env);
			
		tmpVar1 = ExprIRGen.generator(expr.expr, container, env);
		tmpVar2 = VarInfo.newTmpVar(type, tmpVar1.isInMem());
		if (charFlag && tmpVar1.getSize()>1)
			container.addIR(new Quad("writebyte", tmpVar2, tmpVar1, null));
		else
			container.addIR(new Quad("write", tmpVar2, tmpVar1, null));
		
		return tmpVar2;
	}

}
