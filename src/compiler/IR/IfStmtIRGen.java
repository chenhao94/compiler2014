package compiler.IR;

import compiler.ast.IfStmt;
import compiler.symbol.Env;

public class IfStmtIRGen {

	public static void generator(IfStmt stmt, Function func, Env env,
			LoopInfo loopInfo) {
		// TODO Auto-generated method stub
		Label alter = new Label("alter" + ((Integer)Label.ifCnt).toString());
		Label endIf = new Label("endIf" + ((Integer)Label.ifCnt).toString());
		++Label.ifCnt;
		
		if (stmt.alter == null) // if-then
		{
			ExprIRGen.generatorForBranch(stmt.expr, func, env, null, endIf);
			StmtIRGen.generator(stmt.stmt, func, env, loopInfo);
			func.addIR(endIf);
		}
		else // if-else
		{
			ExprIRGen.generatorForBranch(stmt.expr, func, env, null, alter);
			StmtIRGen.generator(stmt.stmt, func, env, loopInfo);
			func.addIR(new Quad("jump", endIf, null, null));
			func.addIR(alter);
			StmtIRGen.generator(stmt.alter, func, env, loopInfo);
			func.addIR(endIf);
		}
	}

}
