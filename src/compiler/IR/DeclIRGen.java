package compiler.IR;

import compiler.ast.Decl;
import compiler.ast.InitDeclrtrs;
import compiler.symbol.Env;

public class DeclIRGen {

	public static void generator(Decl decl, IRContainer container ,Env env) {
		// TODO Auto-generated method stub
		InitDeclrtrs initDeclrtrs = decl.initDeclrtrs;
		
		while (initDeclrtrs != null)
		{
			InitDeclrtrIRGen.generator(initDeclrtrs.initDeclrtr, container, env);
			initDeclrtrs = initDeclrtrs.next;
		}
	}

}
