package compiler.IR;

public class StringTransform {

	public static String toExternalRepresentation(String _string) {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		int length = _string.length();
		char c;
		
		for (int i = 0; i < length; ++i)
		{
			c = _string.charAt(i);
			switch (c)
			{
			case '\"': builder.append("\\\""); break;
			case '\\': builder.append("\\'"); break;
			case '\b': builder.append("\\b"); break;
			case '\f': builder.append("\\f"); break;
			case '\n': builder.append("\\n"); break;
			case '\r': builder.append("\\r"); break;
			case '\t': builder.append("\\t"); break;
			default:
				builder.append(c);
			}
		}
		
		return "\"" + builder.toString() + "\"";
	}

	public static String charToExternalRepresentation(String _string) {
		// TODO Auto-generated method stub
		StringBuilder builder = new StringBuilder();
		int length = _string.length();
		char c;
		
		for (int i = 0; i < length; ++i)
		{
			c = _string.charAt(i);
			switch (c)
			{
			case '\'': builder.append("\\'"); break;
			case '\"': builder.append("\\\""); break;
			case '\\': builder.append("\\'"); break;
			case '\b': builder.append("\\b"); break;
			case '\f': builder.append("\\f"); break;
			case '\n': builder.append("\\n"); break;
			case '\r': builder.append("\\r"); break;
			case '\t': builder.append("\\t"); break;
			default:
				builder.append(c);
			}
		}
		
		return "\'" + builder.toString() + "\'";
	}
}
