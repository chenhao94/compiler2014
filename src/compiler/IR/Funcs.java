package compiler.IR;

public class Funcs {
	
	public static java.util.HashMap<String, Function> funcs = new java.util.HashMap<String, Function>();
	
	public static void add(Function func)
	{
		funcs.put(func.name, func);
	}
}
