package compiler.IR;

public class Label extends IR {
	
	public String name;
	public int num;
	
	static int ifCnt = 0, whileCnt = 0, forCnt = 0, tmpCnt = 0, totCnt = 0;
	static java.util.HashSet<String> namingPool = new java.util.HashSet<String>();
	
	public Label(String string)
	{
		String tmp = string;
		
		if (!namingPool.contains(tmp)) 
		{
			name = tmp;
			namingPool.add(name);
		}
		else
			for (int i = 0; ; ++i)
			{
				name = tmp + "$$" + ((Integer)i).toString();
				if (!namingPool.contains(name))
				{
					namingPool.add(name);
					break;
				}
			}
		
		num = totCnt++;
	}
	
	public String getName()
	{
		return name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + ":";
	}
	
}
