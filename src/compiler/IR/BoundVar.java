package compiler.IR;

import compiler.semantics.Type;

public class BoundVar extends VarInfo {

	// A type of special variables that bound with a memory address
	// no need to assign memory location to these variables
	final private VarInfo target;
	
	public BoundVar(Type type, VarInfo _tar)
	{
		super(type, !type.isIntegerType() && !type.isPurePtr());
		name = "BoundVar@" + VarInfo.newTmpName();
		target = _tar;
	}
	
	public void bind(VarInfo _target)
	{
		//target = _target;
		if (_target.equals(target))
			return;
		throw new RuntimeException("error in binding!");
	}
	
	public VarInfo getTarget()
	{
		return target;
	}
	
	@Override
	public void allocate(Function func)
	{
		// do nothing
		return;
	}
	
	@Override
	public boolean isSimple()
	{
		return false;
	}
}
