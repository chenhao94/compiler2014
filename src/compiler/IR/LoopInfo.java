package compiler.IR;

public class LoopInfo {
	
	public Label continueLabel, breakLabel;
	
	public LoopInfo(Label c, Label b)
	{
		continueLabel = c;
		breakLabel = b;
	}

}
