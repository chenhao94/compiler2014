package compiler.IR;

import compiler.semantics.Type;

public class OffsettedVar extends VarInfo {

	private int offset;
	private VarInfo basicVar;
	
	public OffsettedVar(VarInfo varInfo, Type _type, int _offset) {
		super(_type, !_type.isIntegerType() && !_type.isPurePtr());
		// TODO Auto-generated constructor stub
		name = varInfo.getName();
		offset = _offset;
		basicVar = varInfo;
	}
	
	public int getOffset()
	{
		return offset;
	}
	
	public VarInfo getBasicVar()
	{
		return basicVar;
	}
	
	@Override
	public void allocate(Function func)
	{
		if (!memAlloc)
		{
			memAlloc = true;
			compiler.codegen.MemAssign.memAssign(basicVar, func);
		}
	}
	
	@Override
	public int getLoc()
	{
		return basicVar.getLoc() + offset;
	}
	
	@Override
	public VarInfo offsettedVar(Type type, int _offset, IRContainer container)
	{
		return new OffsettedVar(basicVar, type, offset + _offset);
	}
	
	@Override
	public String toString()
	{
		return basicVar.toString() + "(" + offset + ")";
	}
	
	@Override
	public boolean isSimple()
	{
		return false;
	}
}
