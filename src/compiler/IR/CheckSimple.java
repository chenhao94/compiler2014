package compiler.IR;

public class CheckSimple {

	public static boolean notSimple(Object obj) {
		// TODO Auto-generated method stub
		if (obj instanceof VarInfo)
			return !(((VarInfo)obj).isSimple());
		else if (obj instanceof Integer || obj instanceof Character)
			return false; 
		return true;
	}

}
