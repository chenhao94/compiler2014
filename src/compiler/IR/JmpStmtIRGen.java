package compiler.IR;

import compiler.ast.JmpStmt;
import compiler.ast.ReturnStmt;
import compiler.ast.JmpStmt.JmpType;
import compiler.symbol.Env;

public class JmpStmtIRGen {

	public static void generator(JmpStmt stmt, Function func, Env env,
			LoopInfo loopInfo) {
		// TODO Auto-generated method stub
		
		if (stmt.jmpType == JmpType.BREAK)
			func.addIR(new Quad("jump", loopInfo.breakLabel, null, null));
		else if (stmt.jmpType == JmpType.CONTINUE)
			func.addIR(new Quad("jump", loopInfo.continueLabel, null, null));
		else //RETURN
			returnGenertor((ReturnStmt)stmt, func, env);
		
	}

	private static void returnGenertor(ReturnStmt stmt, Function func, Env env) {
		// TODO Auto-generated method stub
		
		VarInfo returnValue = null;
		if (stmt.expr != null)
		{
			returnValue = ExprIRGen.generator(stmt.expr, func, env);
			if (returnValue != null)
				func.addIR(new Quad("returnvalue", null, returnValue, null));
		}
		func.addIR(new Quad("return", null, null, null));
	}

}
