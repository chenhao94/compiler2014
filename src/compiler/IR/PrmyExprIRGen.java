package compiler.IR;

import compiler.ast.CharConst;
import compiler.ast.Constant;
import compiler.ast.Expr;
import compiler.ast.IntConst;
import compiler.ast.PrmyExpr;
import compiler.semantics.CharType;
import compiler.semantics.IntType;
import compiler.semantics.PtrType;
import compiler.symbol.Env;
import compiler.symbol.Symbol;

public class PrmyExprIRGen {

	public static VarInfo generator(PrmyExpr expr, IRContainer container,
			Env env) {
		// TODO Auto-generated method stub
		
		VarInfo tmpVar = null;
		
		if (expr.obj instanceof Symbol)
			return expr.envInfo.varInfo;
		else if (expr.obj instanceof Constant)
		{
			if (expr.obj instanceof IntConst)
			{
				tmpVar = VarInfo.newTmpVar(IntType.getInstance(), false);
				container.addIR(new Quad("read", tmpVar, ((IntConst)expr.obj).value, 0));
			}
			else //	if (((PrmyExpr)expr).obj instanceof CharConst)
			{
				tmpVar = VarInfo.newTmpVar(CharType.getInstance(), false);
				container.addIR(new Quad("read", tmpVar, (int)((CharConst)expr.obj).value, 0));
			}
			return tmpVar;
		}
		else if (expr.obj instanceof String)
		{
			tmpVar = VarInfo.newTmpVar(new PtrType(CharType.getInstance()), false);
			StringIR stringVar = StringIR.getInstance((String)expr.obj);
			if (!stringVar.isDeclared())
			{
				GlobalIRs.getInstance().addIR(stringVar);
				stringVar.declare();
			}
			container.addIR(new Quad("readstring", tmpVar, stringVar, 0));
			return tmpVar;
		}
		else// if (((PrmyExpr)expr).obj instanceof Expr)
			return ExprIRGen.generator((Expr)expr.obj, container, env);
	}

}
