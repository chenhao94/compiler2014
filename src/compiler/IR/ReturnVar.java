package compiler.IR;

import compiler.semantics.Type;

public class ReturnVar extends VarInfo {
	
	public ReturnVar(Type type, boolean inMem) // for generating a return value variable
	{
		super(type, inMem);
		name = "$~returnValue~$";
	}
	
	@Override
	public boolean isReturnValue()
	{
		return true;
	}
}
