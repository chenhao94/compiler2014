package compiler.IR;

import compiler.ast.WhileStmt;
import compiler.symbol.Env;

public class WhileStmtIRGen {

	public static void generator(WhileStmt stmt, Function func, Env env) {
		// TODO Auto-generated method stub
		
		Label beginWhile = new Label("beginWhile" + ((Integer)Label.whileCnt).toString());
		Label endWhile = new Label("endWhile" + ((Integer)Label.whileCnt).toString());
		LoopInfo loopInfo = new LoopInfo(beginWhile, endWhile);
		
		func.addIR(beginWhile);
		if (stmt.expr != null)
			ExprIRGen.generatorForBranch(stmt.expr, func, env, null, endWhile);
		StmtIRGen.generator(stmt.stmt, func, env, loopInfo);
		func.addIR(new Quad("jump", beginWhile, null, null));
		func.addIR(endWhile);
		
		++Label.whileCnt;
	}

}
