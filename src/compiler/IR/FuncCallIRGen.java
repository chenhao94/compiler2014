package compiler.IR;

import compiler.ast.Argmts;
import compiler.ast.Expr;
import compiler.symbol.Env;
import compiler.semantics.ArrayType;
import compiler.semantics.FuncType;
import compiler.semantics.PtrType;

public class FuncCallIRGen {

	public static VarInfo generator(Expr expr, Argmts argmts,
			IRContainer container, Env env) {
		// TODO Auto-generated method stub
		int cnt = 0;
		VarInfo funcInfo = null, paramVar = null, tmpVar = null;
		java.util.ArrayList<VarInfo> param = new java.util.ArrayList<VarInfo>();
		
		while (argmts != null)
		{
			tmpVar = ExprIRGen.generator(argmts.assExpr, container, env);
			if (tmpVar.isReturnValue())
			{
				paramVar = VarInfo.newTmpVar(tmpVar.getType(), tmpVar.isInMem());
				container.addIR(new Quad("write", paramVar, tmpVar, null));
			}
			else if (tmpVar.getType() instanceof ArrayType)
			{
				paramVar = VarInfo.newTmpVar(new PtrType(((ArrayType)tmpVar.getType()).type), false);
				container.addIR(new Quad("read", paramVar, tmpVar, null));
			}
			else
				paramVar = tmpVar;
			param.add(paramVar);
			
			++cnt;
			argmts = argmts.next;
		}
		
		
		funcInfo = ExprIRGen.generator(expr, container, env);
		for (VarInfo varInfo: param)
			container.addIR(new Quad("param", null, varInfo, null));
		container.addIR(new Quad("call", null, cnt, funcInfo));
		
		return VarInfo.returnValue(((FuncType)funcInfo.getType()).getReturnType());
	}

}
