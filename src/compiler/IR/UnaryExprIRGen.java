package compiler.IR;

import compiler.ast.UnaryExpr;
import compiler.semantics.IntType;
import compiler.semantics.PtrType;
import compiler.symbol.Env;

public class UnaryExprIRGen {

	public static VarInfo generator(UnaryExpr expr, IRContainer container,
			Env env) {
		// TODO Auto-generated method stub
		if (expr.opType == UnaryExpr.OpType.NOP)
			return ExprIRGen.generator(expr.expr, container, env);
		
		VarInfo e1 = ExprIRGen.generator(expr.expr, container, env);
		VarInfo tmpVar = null;
		
		switch (expr.opType)
		{
			case PINC:
				if (e1.getType().isPtrType())
					container.addIR(new Quad("addu", e1, e1, ((PtrType)e1.getType()).type.getSize()));
				else
					container.addIR(new Quad("addu", e1, e1, 1));
				return e1;
			case PDEC:
				if (e1.getType().isPtrType())
					container.addIR(new Quad("subu", e1, e1, ((PtrType)e1.getType()).type.getSize()));
				else
					container.addIR(new Quad("subu", e1, e1, 1));
				return e1;
			case AMP:
				tmpVar = VarInfo.newTmpVar(new PtrType(e1.getType()), false);
				container.addIR(new Quad("addr", tmpVar, e1, null));
				return tmpVar;
			case STAR:
				tmpVar = new BoundVar(((PtrType)e1.getType()).type, e1);
				container.addIR(new Quad("ptrstar", tmpVar, e1, null));
				return tmpVar;
			case POS:
				return e1;
			case NEG:
				tmpVar = VarInfo.newTmpVar(e1.getType(), false);
				container.addIR(new Quad("subu", tmpVar, 0, e1));
				return tmpVar;
			case TILD:
				tmpVar = VarInfo.newTmpVar(e1.getType(), false);
				container.addIR(new Quad("nor", tmpVar, e1, 0));
				return tmpVar;
			case EXCL:
				tmpVar = VarInfo.newTmpVar(IntType.getInstance(), false);
				container.addIR(new Quad("sltu", tmpVar, e1, 1));
				return tmpVar;
			case SIZEOF		:
				tmpVar = VarInfo.newTmpVar(IntType.getInstance(), false);
				if (expr.typName != null)
					container.addIR(new Quad("read", tmpVar, expr.type.getSize(), 0));
				else
					container.addIR(new Quad("read", tmpVar, ExprIRGen.generator(expr.expr, container, env).getSize(), 0));
				return tmpVar;
				
			default: return null;
		}
	}

	public static void generatorForBranch(UnaryExpr expr, Function func,
			Env env, Label trueLabel, Label falseLabel) {
		// TODO Auto-generated method stub
			if (expr.opType == UnaryExpr.OpType.EXCL)
				ExprIRGen.generatorForBranch(expr, func, env, falseLabel, trueLabel);
			else
			{
				VarInfo e1 = ExprIRGen.generator(expr, func, env);
				
				if (trueLabel != null && falseLabel != null)
				{
					func.addIR(new Quad("bnez", trueLabel, e1, null));
					func.addIR(new Quad("beqz", falseLabel, e1, null));
				}
				else if (trueLabel != null)
					func.addIR(new Quad("bnez", trueLabel, e1, null));
				else if (falseLabel != null)
					func.addIR(new Quad("beqz", falseLabel, e1, null));
			}
	}

}
