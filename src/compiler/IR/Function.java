package compiler.IR;

import compiler.basicblocks.Node;
import compiler.registerAllocation.Register;
import compiler.semantics.RecordType;
import compiler.semantics.Type;

class ArgListEntry
{
	public int size, loc; // loc is for record types
	public boolean inMem;
	
	ArgListEntry(int _size, boolean _inMem)
	{
		size = _size;
		inMem = _inMem;
	}
}

public class Function extends IRContainer {
	
	public String name;
	public int argNum, returnSize, frameSize, maxCalleeParams, locStart;
	public boolean eclipse, inMem;
	private boolean recordInArg;
	public java.util.ArrayList<ArgListEntry> argList;
	private java.util.LinkedList<Node> nodes;
	private java.util.HashSet<VarInfo> varsForRegAlloc = new java.util.HashSet<VarInfo>();
	private java.util.HashSet<Register> registerUse;
	private java.util.HashSet<Function> callers;
	private java.util.HashSet<Function> tailcallers;
	
	public Function(String _name, Type type)
	{
		super();
		name = _name;
		returnSize = type.getSize();
		eclipse = false;
		argNum = frameSize = maxCalleeParams = 0;
		argList = new java.util.ArrayList<ArgListEntry>();
		nodes = null;
		inMem = !(type.isIntegerType() || type.isPurePtr());
		locStart = 80;
		registerUse = new java.util.HashSet<Register>();
		callers = new java.util.HashSet<Function>();
		tailcallers = new java.util.HashSet<Function>();
		recordInArg = false;
	}
	
	public Function(String _name, Type type, boolean _eclipse)
	{
		super();
		name = _name;
		returnSize = type.getSize();
		eclipse = _eclipse;
		argNum = 0;
		argList = new java.util.ArrayList<ArgListEntry>();
		nodes = null;
		inMem = !(type.isIntegerType() || type.isPurePtr());
		locStart = 80;
		registerUse = new java.util.HashSet<Register>();
		callers = new java.util.HashSet<Function>();
		tailcallers = new java.util.HashSet<Function>();
		recordInArg = false;
	}
	
	public void addArg(Type type)
	{
		int size = type.getSize();
		int loc, align = type.alignSize();
		ArgListEntry entry = new ArgListEntry (size, !(type.isIntegerType() || type.isPurePtr()));
		
		if (entry.inMem)
		{
			loc = locStart + frameSize + size;
			if (loc % align != 0)
				loc += align - loc % align;
			entry.loc = loc;
			frameSize = loc - locStart;
		}
		++argNum;
		argList.add(entry);
		if (type instanceof RecordType)
			recordInArg = true;
	}
	
	public void setNode(java.util.LinkedList<Node> _nodes)
	{
		nodes = _nodes;
	}
	
	public java.util.LinkedList<Node> getNode()
	{
		return nodes;
	}

	public int locOfKthArg(int k)
	{
		return argList.get(k).loc;
	}
	
	public void addVarForRegAlloc(VarInfo varInfo)
	{
		this.varsForRegAlloc.add(varInfo);
	}
	
	public java.util.HashSet<VarInfo> getVarsForRegAlloc()
	{
		return this.varsForRegAlloc;
	}
	
	public java.util.HashSet<Register> getRegisters()
	{
		return registerUse;
	}
	
	public java.util.HashSet<Function> getCallers()
	{
		return callers;
	}
	
	public boolean uses(Register reg)
	{
		return registerUse.contains(reg);
	}
	
	public void addUse(Register reg)
	{
		registerUse.add(reg);
	}
	
	public void calledBy(Function func)
	{
		callers.add(func);
	}
	
	public boolean mergeRegsFromCallee(Function func)
	{
		if (registerUse.containsAll(func.getRegisters()))
			return false;
		registerUse.addAll(func.getRegisters());
		return true;
	}

	public void calledTailBy(Function func) {
		// TODO Auto-generated method stub
		tailcallers.add(func);
	}

	public java.util.HashSet<Function> getTailedCallers() {
		// TODO Auto-generated method stub
		return tailcallers;
	}

	public boolean cannotTailCall() {
		// TODO Auto-generated method stub
		return recordInArg;
	}
	
}
