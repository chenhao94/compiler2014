package compiler.IR;

import compiler.ast.CmpStmt;
import compiler.ast.Expr;
import compiler.ast.ForStmt;
import compiler.ast.IfStmt;
import compiler.ast.JmpStmt;
import compiler.ast.Stmt;
import compiler.ast.WhileStmt;
import compiler.symbol.Env;

public class StmtIRGen {

	public static void generator(Stmt stmt, Function func, Env env, LoopInfo loopInfo) {
		// TODO Auto-generated method stub
		if (stmt instanceof Expr)
			ExprIRGen.generator(((Expr)stmt), func, env);
		else if (stmt instanceof CmpStmt)
			CmpStmtIRGen.generator((CmpStmt)stmt, func, loopInfo);
		else if (stmt instanceof IfStmt)
			IfStmtIRGen.generator((IfStmt)stmt, func, env, loopInfo);
		//		ItrStmt
		else if (stmt instanceof ForStmt)
			ForStmtIRGen.generator((ForStmt)stmt, func, env);
		else if (stmt instanceof WhileStmt)
			WhileStmtIRGen.generator((WhileStmt)stmt, func, env);
		else if (stmt instanceof JmpStmt)
			JmpStmtIRGen.generator((JmpStmt)stmt, func, env, loopInfo);
		else
			return;
	}

}
