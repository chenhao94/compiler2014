package compiler.IR;

import compiler.ast.Decl;
import compiler.ast.FuncDef;
import compiler.ast.Program;
import compiler.symbol.Env;

public class ProgramIRGen {

	public static void generator(Program program) {
		// TODO Auto-generated method stub
		Env env = program.env;
		Object obj;
		
		while (program != null)
		{
			obj = program.object;
			if (obj instanceof Decl)
				DeclIRGen.generator((Decl)obj, GlobalIRs.getInstance(), env);
			else // FuncDef
				FuncDefIRGen.generator((FuncDef)obj, env);
			
			program = program.next;
		}
		
		if (IRPrinter.printVar == true)
			env.printVariables(IRPrinter.varOut);
	}

}
