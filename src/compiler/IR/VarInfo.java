package compiler.IR;

import compiler.registerAllocation.Register;
import compiler.registerAllocation.RegisterPool;
import compiler.semantics.RecordType;
import compiler.semantics.Type;

class LifeSpan
{
	int start, end;
	
	LifeSpan(int _start, int _end)
	{
		start = _start;
		end = _end;
	}
	
	void livelyIn(int time)
	{
		start = Math.min(start, time);
		end = Math.max(end, time);
	}
}

public class VarInfo {
	
	protected String name;
	private Type type;
	private int size, loc; // loc < 0 ($fp + loc) : in stack; loc >= 0 (la loc)
	private int argNum; // >= 0, the argNum-th argument of a function  
	private boolean inMem;
	private int reg; // the ID (defined by myself) of register that allocate to this variable
	private LifeSpan lifeSpan;
	protected boolean spill, memAlloc;
	private static int tmpCnt = 0;
	final private int ID; // used for bitset
	static private java.util.ArrayList<LifeSpan> lifeSpans = new java.util.ArrayList<LifeSpan>();
	private Integer constant;
	
	static private int IDtot = 0; 
	
	static java.util.HashSet<String> namingPool = new java.util.HashSet<String>();
	
	public VarInfo(String string, Type _type, boolean _inMem)
	{
		ID = IDtot++;
		String tmp = "$" + string;
		
		if (!namingPool.contains(tmp)) 
		{
			name = tmp;
			namingPool.add(name);
		}
		else
			for (int i = 0; ; ++i)
			{
				name = tmp + "$$" + ((Integer)i).toString();
				if (!namingPool.contains(name))
				{
					namingPool.add(name);
					break;
				}
			}
		type = _type;
		size = type.getSize();
		inMem = _inMem;
		loc = -1;
		spill = memAlloc = false;
		reg = 0;
		argNum = -1;
		lifeSpan = new LifeSpan(Integer.MAX_VALUE, Integer.MIN_VALUE);
		lifeSpans.add(ID, lifeSpan);
		constant = null;
	}
	
	protected VarInfo(Type _type, boolean _inMem) {
		// TODO Auto-generated constructor stub
		ID = IDtot++;
		type = _type;
		size = type.getSize();
		inMem = _inMem;
		loc = -1;
		spill = memAlloc = false;
		reg = 0;
		argNum = -1;
		lifeSpan = new LifeSpan(Integer.MAX_VALUE, Integer.MIN_VALUE);
		lifeSpans.add(ID, lifeSpan);
		constant = null;
	}

	public static VarInfo newTmpVar(Type type, boolean inMem)
	{
		String tmp = "tmp$" + ((Integer)tmpCnt).toString();
		++tmpCnt;
		return new VarInfo(tmp, type, inMem);
	}
	
	public static String newTmpName()
	{
		String tmp = "tmp$" + ((Integer)tmpCnt).toString();
		String name;
		++tmpCnt;
		if (!namingPool.contains(tmp)) 
		{
			namingPool.add(tmp);
			return tmp;
		}
		else
			for (int i = 0; ; ++i)
			{
				name = tmp + "$$" + ((Integer)i).toString();
				if (!namingPool.contains(name))
				{
					namingPool.add(name);
					return name;
				}
			}
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getID()
	{
		return ID;
	}
	
	public boolean isConst()
	{
		return constant != null;
	}
	
	public int getConst()
	{
		return constant;
	}
	
	public void setConst(int _const)
	{
		constant = _const;
	}
	
	public void releaseConst()
	{
		constant = null;
	}
	
	public void livelyIn(int time)
	{
		lifeSpan.livelyIn(time);
	}
	
	static public void livelyIn(int index, int time)
	{
		lifeSpans.get(index).livelyIn(time);
	}
	
	public int getStart()
	{
		return lifeSpan.start;
	}
	
	public int getEnd()
	{
		return lifeSpan.end;
	}
	
	public int getLoc()
	{
		return loc;
	}
	
	public void setLoc(int l)
	{
		loc = l;
	}
	
	public int getArgNum()
	{
		return argNum;
	}
	
	public void setArgNum(int a)
	{
		argNum = a;
	}
	
	public boolean isArg()
	{
		return argNum >= 0;
	}
	
	public int getSize()
	{
		return size;
	}	
	
	public Type getType()
	{
		return type;
	}
	
	public boolean isInMem()
	{
		return inMem;
	}
	
	public boolean isReturnValue()
	{
		return false;
	}
	
	public boolean isSimple()
	{
		return true;
	}

	public static VarInfo returnValue(Type t) {
		boolean inMem = !(t.isIntegerType() || t.isPurePtr());
		return new ReturnVar(t, inMem);
	}

	public VarInfo offsettedVar(Type type, int offset, IRContainer container)
	{
		return new OffsettedVar(this, type, offset);
	}
	
	@Override
	public String toString()
	{
		return name;
	}

	public IR varLabel() {
		// TODO Auto-generated method stub
		spill = true;
		memAlloc = true;
		return new VarLabel(this);
	}

	public boolean isSpilled()
	{
		return (CheckSimple.notSimple(this) || inMem || spill);
	}
	
	public void spill()
	{
		spill = true;
		reg = 0;
	}
	
	public void setReg(int _reg)
	{
		reg = _reg;
	}
	
	public Register getReg()
	{
		return RegisterPool.registerPool.get(reg);
	}
	
	public int getRegID()
	{
		return reg;
	}
	
	public void allocate(Function func)
	{
		if (!memAlloc && this.isSpilled())
		{
			memAlloc = true;
			compiler.codegen.MemAssign.memAssign(this, func);
		}
	}

	public boolean isRecordArgRet() {
		// TODO Auto-generated method stub
		return (isArg() || isReturnValue()) && getType() instanceof RecordType;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof VarInfo)
			return ID == ((VarInfo)obj).ID;
		return false;
	}

	public void initForLiveliness() {
		// TODO Auto-generated method stub
		lifeSpan.start = Integer.MAX_VALUE;
		lifeSpan.end = Integer.MIN_VALUE;
	}
}
