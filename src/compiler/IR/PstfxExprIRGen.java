package compiler.IR;

import compiler.ast.ArgPstfx;
import compiler.ast.DotPstfx;
import compiler.ast.Expr;
import compiler.ast.OpPstfx;
import compiler.ast.Pstfx;
import compiler.ast.PstfxExpr;
import compiler.ast.PtrPstfx;
import compiler.ast.SqbPstfx;
import compiler.semantics.ArrayType;
import compiler.semantics.IntType;
import compiler.semantics.RecordType;
import compiler.semantics.PtrType;
import compiler.semantics.Type;
import compiler.symbol.Env;

public class PstfxExprIRGen {

	public static VarInfo generator(PstfxExpr expr, IRContainer container,
			Env env) {
		// TODO Auto-generated method stub
		
		Pstfx pstfx = expr.pstfx;
		VarInfo tmpVar1, tmpVar2, tmpVar3;
		Type type = null;
		
		if (pstfx == null)
			return ExprIRGen.generator(expr.expr, container, env);
		
		if (pstfx instanceof ArgPstfx)
			return FuncCallIRGen.generator(expr.expr, ((ArgPstfx)pstfx).argmts, container, env);
		else if (pstfx instanceof DotPstfx)
		{
			String name = ((DotPstfx)pstfx).symbol.toString();
			Type entryType = null;
			
			tmpVar1 = ExprIRGen.generator(expr.expr, container, env);
			type = tmpVar1.getType();
			int offset = ((RecordType)type).getOffset(name);
			entryType = ((RecordType)type).getEntry(name);
			
			return tmpVar1.offsettedVar(entryType, offset, container);
		}
		else if (pstfx instanceof PtrPstfx)
		{
			String name = ((PtrPstfx)pstfx).symbol.toString();
			Type entryType = null;
			
			tmpVar1 = ExprIRGen.generator(expr.expr, container, env);
			type = ((PtrType)tmpVar1.getType()).type;
			tmpVar2 = new BoundVar(type, tmpVar1);
			int offset = ((RecordType)type).getOffset(name);
			entryType = ((RecordType)type).getEntry(name);
			
			container.addIR(new Quad("ptrstar", tmpVar2, tmpVar1, null));
			return tmpVar2.offsettedVar(entryType, offset, container);
		}
		else if (pstfx instanceof OpPstfx)
		{
			tmpVar1 = ExprIRGen.generator(expr.expr, container, env);
			tmpVar2 = VarInfo.newTmpVar(tmpVar1.getType(), tmpVar1.isInMem());
			container.addIR(new Quad("read", tmpVar2, tmpVar1, 0));
			int number = 1;
			if (tmpVar1.getType().isPtrType())
				number = ((PtrType)tmpVar1.getType()).type.getSize();
			if (((OpPstfx)pstfx).opType == OpPstfx.OpType.INC)
				container.addIR(new Quad("addu", tmpVar1, tmpVar1, number));
			else
				container.addIR(new Quad("subu", tmpVar1, tmpVar1, number));
			return tmpVar2;
		}
		else if (pstfx instanceof SqbPstfx)
		{
			tmpVar1 = ptrGetAddr(expr.expr, container, env);
			Type baseType = ((PtrType)tmpVar1.getType()).type;
			
			tmpVar2 = ExprIRGen.generator(((SqbPstfx)pstfx).expr, container, env);
			tmpVar3 = VarInfo.newTmpVar(IntType.getInstance(), false);
			container.addIR(new Quad("mul", tmpVar3, tmpVar2, baseType.getSize()));
			
			tmpVar2 = VarInfo.newTmpVar(tmpVar1.getType(), false); // pointer
			container.addIR(new Quad("addu", tmpVar2, tmpVar1, tmpVar3));
			
			if (baseType instanceof ArrayType)
			{
				//baseType = new PtrType(((ArrayType)baseType).type);
				return tmpVar2;
			}
			tmpVar1 = new BoundVar(baseType, tmpVar2);
			container.addIR(new Quad("ptrstar", tmpVar1, tmpVar2, null));
			return tmpVar1;
		}
		
		return null;
	}

	private static VarInfo ptrGetAddr(Expr expr,
			IRContainer container, Env env) {
		// TODO get the address stored in 'expr', expr have to be a pointer/array
		VarInfo tmpVar1, tmpVar2, tmpVar3;
		Type baseType;
		if (expr instanceof PstfxExpr && ((PstfxExpr)expr).pstfx instanceof SqbPstfx)
		{
			Expr tmpExpr = ((PstfxExpr)expr).expr;
			SqbPstfx pstfx = (SqbPstfx)((PstfxExpr)expr).pstfx;
			tmpVar1 = ptrGetAddr(tmpExpr, container, env);
			tmpVar2 = ExprIRGen.generator(pstfx.expr, container, env);
			baseType = ((PtrType)tmpVar1.getType()).type;
			tmpVar3 = VarInfo.newTmpVar(IntType.getInstance(), false);
			container.addIR(new Quad("mul", tmpVar3, tmpVar2, baseType.getSize()));
			if (baseType instanceof ArrayType)
			{	
				baseType = new PtrType(((PtrType)baseType).type);
				tmpVar2 = VarInfo.newTmpVar(baseType, false);
				container.addIR(new Quad("addu", tmpVar2, tmpVar1, tmpVar3));
				return tmpVar2;
			}
			else
			{
				tmpVar2 = VarInfo.newTmpVar(tmpVar1.getType(), false);
				container.addIR(new Quad("addu", tmpVar2, tmpVar1, tmpVar3));
				tmpVar3 = new BoundVar(baseType, tmpVar2);
				container.addIR(new Quad("ptrstar", tmpVar3, tmpVar2, null));
				return tmpVar3;
			}
		}
		else
		{
			tmpVar1 = ExprIRGen.generator(expr, container, env);
			if (tmpVar1.getType().isPurePtr())
				return tmpVar1;
			tmpVar2 = VarInfo.newTmpVar(new PtrType(((PtrType)tmpVar1.getType()).type), false);
			container.addIR(new Quad("addr", tmpVar2, tmpVar1, null));
			return tmpVar2;
		}
	}

}
