package compiler.IR;

import compiler.ast.FuncDef;
import compiler.semantics.FuncType;
import compiler.semantics.Type;
import compiler.symbol.Env;

public class FuncDefIRGen {

	public static void generator(FuncDef funcDef, Env env) {
		// TODO Auto-generated method stub
		String name = funcDef.plnDeclrtr.symbol.toString();
		FuncType funcType = (FuncType)env.findBasic(name).type;
		Function func = new Function(name, funcType.getReturnType());
		
		for (Type type: funcType.getArguments())
			func.addArg(type);
		
		CmpStmtIRGen.generator(funcDef.cmpStmt, func, null);
		
		Funcs.add(func);
	}

}
