package compiler.IR;

import compiler.ast.BinExpr;
import compiler.ast.CstExpr;
import compiler.ast.Expr;
import compiler.ast.PrmyExpr;
import compiler.ast.PstfxExpr;
import compiler.ast.UnaryExpr;
import compiler.symbol.Env;

public class ExprIRGen {

	public static void generatorForBranch(Expr expr, Function func, Env env,
			Label trueLabel, Label falseLabel) {
		// TODO Auto-generated method stub
		
		if (expr instanceof BinExpr)
			BinExprIRGen.generatorForBranch((BinExpr)expr, func, env,trueLabel, falseLabel);
		else if (expr instanceof UnaryExpr)
			UnaryExprIRGen.generatorForBranch((UnaryExpr)expr, func, env,trueLabel, falseLabel);
		else
		{
			VarInfo e1 = ExprIRGen.generator(expr, func, env);
			
			if (trueLabel != null && falseLabel != null)
			{
				func.addIR(new Quad("bnez", trueLabel, e1, null));
				func.addIR(new Quad("beqz", falseLabel, e1, null));
			}
			else if (trueLabel != null)
				func.addIR(new Quad("bnez", trueLabel, e1, null));
			else if (falseLabel != null)
				func.addIR(new Quad("beqz", falseLabel, e1, null));
		}
	}

	public static VarInfo generator(Expr expr, IRContainer container, Env env) {
		// TODO Auto-generated method stub
		
		if (expr instanceof BinExpr)
			return BinExprIRGen.generator((BinExpr)expr, container, env);
		else if (expr instanceof CstExpr)
			return CstExprIRGen.generator((CstExpr)expr, container, env);
		else if (expr instanceof PrmyExpr)
			return PrmyExprIRGen.generator((PrmyExpr)expr, container, env);
		else if (expr instanceof PstfxExpr)
			return PstfxExprIRGen.generator((PstfxExpr)expr, container, env);
		else if (expr instanceof UnaryExpr)
			return UnaryExprIRGen.generator((UnaryExpr)expr, container, env);
		return null;
	}

}
