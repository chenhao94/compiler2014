package compiler.IR;

import java.io.IOException;

import compiler.ast.Program;
import compiler.semantics.CompilationInfo;

public class IRGenerator {
	
	public static boolean generator(String filename) throws IOException
	{	
		Program program = compiler.semantics.CheckCode.checkCode(filename);
		
		CompilationInfo.printMsg();
		if (program == null)
		{
			System.err.println("Compilation Error.");
			return false;
		}
		
		ProgramIRGen.generator(program);
		return true;
	}

}
