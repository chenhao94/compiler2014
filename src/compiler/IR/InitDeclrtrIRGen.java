package compiler.IR;

import compiler.ast.BinExpr;
import compiler.ast.InitDeclrtr;
import compiler.ast.Initials;
import compiler.semantics.ArrayType;
import compiler.semantics.CharType;
import compiler.semantics.IntType;
import compiler.semantics.PtrType;
import compiler.semantics.Type;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;

public class InitDeclrtrIRGen {
	
	public static void generator(InitDeclrtr initDeclrtr,
			IRContainer container, Env env) {
		// TODO Auto-generated method stub
		Type type = initDeclrtr.type;
		if (container instanceof GlobalIRs)
			globalGenerator(type, initDeclrtr, (GlobalIRs)container, env);
		else // Function
			localGenerator(type, initDeclrtr, (Function)container, env);
	}

	public static void globalGenerator(Type type, InitDeclrtr initDeclrtr,
			GlobalIRs container, Env env) {
		// TODO Auto-generated method stub
		
		EnvInfo envInfo = initDeclrtr.envInfo;
		int typeSize = type.getSize();
		VarInfo varInfo = envInfo.varInfo;
		
		if (initDeclrtr.initial == null)
		{
			if (envInfo.initialized())
				return;
			else if (envInfo.printed)
				return;
			container.addIR(varInfo.varLabel());
			container.addIR(new Quad("zero", null, typeSize, null));
			envInfo.printed = true;
			return;
		}
		
		container.addIR(varInfo.varLabel());
		
		if (initDeclrtr.initial.object instanceof BinExpr)
		{
			int value = initDeclrtr.initial.getValue();
			if (type instanceof CharType)
				container.addIR(new Quad("writebyte", varInfo, 
						(char)value, null));
			else if (type instanceof IntType || type instanceof PtrType) // ArrayType is impossible
				container.addIR(new Quad("writeword", varInfo, 														// because of semantic checking
						(int)value, null));
			else // record type
				throw new RuntimeException("It is impossible to be here!");
		}
		else // initials
			globalArrayInitialGenerator(varInfo, (ArrayType)type, (Initials)initDeclrtr.initial.object, container, env);
		
	}
	
	private static void localGenerator(Type type, InitDeclrtr initDeclrtr,
			Function function, Env env) {
		// TODO Auto-generated method stub
		EnvInfo envInfo = initDeclrtr.envInfo;
		VarInfo varInfo = envInfo.varInfo;
		
		if (initDeclrtr.initial == null)
			return;
		
		if (initDeclrtr.initial.object instanceof BinExpr)
		{
			if (type instanceof CharType)
				function.addIR(new Quad("writebyte", varInfo, 
						BinExprIRGen.generator((BinExpr)initDeclrtr.initial.object, function, env), null));
			else if (type instanceof IntType || type instanceof PtrType) // ArrayType is impossible
				function.addIR(new Quad("writeword", varInfo, 														// because of semantic checking
						BinExprIRGen.generator((BinExpr)initDeclrtr.initial.object, function, env), null));
			else // record type
				function.addIR(new Quad("write", varInfo, 
						BinExprIRGen.generator((BinExpr)initDeclrtr.initial.object, function, env), null));
		}
		else // initials
			localArrayInitialGenerator(envInfo.varInfo, 0, (ArrayType)type, 
					(Initials)initDeclrtr.initial.object, function, env);
	}	

	private static void globalArrayInitialGenerator(VarInfo varInfo, ArrayType arrayType, Initials initials,
			GlobalIRs container, Env env) {
		// TODO Auto-generated method stub
		
		int cnt = 0;
		Type elementType = arrayType.type;
		
		if (initials == null)
		{
			container.addIR(new Quad("zero", varInfo, arrayType.getSize(), null));
			return;
		}
		
		while (initials != null)
		{
			if (!(elementType instanceof ArrayType))
			{
				int value = initials.initial.getValue();
				
				if (elementType instanceof CharType)
					container.addIR(new Quad("writebyte", varInfo, 
							(char)value, null));
				else if (elementType instanceof IntType || elementType instanceof PtrType) // ArrayType is impossible
					container.addIR(new Quad("writeword", varInfo, 														// because of semantic checking
							value, null));
				else // record type
					throw new RuntimeException("It is impossible to be here!");
			}
			else
				globalArrayInitialGenerator(varInfo, (ArrayType)elementType, (Initials)initials.initial.object, container, env);
			
			initials = initials.next;
			++cnt;
		}
		
		if (arrayType.getCap()-cnt >0)
			container.addIR(new Quad("zero", varInfo, arrayType.getSize() - cnt * elementType.getSize(), null));
		
		return;
	}

	private static void localArrayInitialGenerator(VarInfo varInfo, int offset, ArrayType arrayType,
			Initials initials, Function function, Env env) {
		// TODO Auto-generated method stub
		
		int cnt = 0;
		Type elementType = arrayType.type;
		
		if (initials == null)
			return;
		
		while (initials != null)
		{
			if (!(elementType instanceof ArrayType))
			{
				if (elementType instanceof CharType)
					function.addIR(new Quad("writebyte", varInfo.offsettedVar(elementType, offset, function), 
							BinExprIRGen.generator((BinExpr)initials.initial.object, function, env), null));
				else if (elementType instanceof IntType || elementType instanceof PtrType) // ArrayType is impossible
					function.addIR(new Quad("writeword", varInfo.offsettedVar(elementType, offset, function), 														// because of semantic checking
							BinExprIRGen.generator((BinExpr)initials.initial.object, function, env), null));
				else // record type
					function.addIR(new Quad("write", varInfo.offsettedVar(elementType, offset, function), 
							BinExprIRGen.generator((BinExpr)initials.initial.object, function, env), null));
			}
			else
				localArrayInitialGenerator(varInfo, offset, (ArrayType)elementType, 
						(Initials)initials.initial.object, function, env);
			
			initials = initials.next;
			++cnt;
			offset += elementType.getSize();
		}
		
		if (arrayType.getCap()-cnt >0)
			function.addIR(new Quad("zero", null, arrayType.getSize() - cnt * elementType.getSize(), null));
		
		return;
	}
}
