package compiler.IR;

import java.io.IOException;
import java.io.PrintWriter;

import compiler.semantics.CheckCode;

public class IRPrinter {

	static boolean printVar = false;
	static java.io.PrintWriter varOut;
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		if (!IRGenerator.generator(args[0]))
			System.exit(1);
		
		PrintWriter IROut = new PrintWriter(CheckCode.nameWithoutExt+".ir");
		
		// global definition
		for (IR ir : GlobalIRs.getInstance().block)
			IROut.println("\t" + ir.toString());
		
		// functions
		for (Function func: Funcs.funcs.values())
		{
			IROut.println(func.name + ":");
			for (IR ir: func.block)
				IROut.println("\t" + ir.toString());
		}
		
		IROut.close();
	}

}
