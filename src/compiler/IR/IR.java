package compiler.IR;

abstract public class IR {

	@Override
	abstract public String toString();
	
	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof IR))
			return false;
		return this.toString().equals(obj.toString());
	}
	
}
