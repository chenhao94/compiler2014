package compiler.IR;

public class IRContainer {
	
	public java.util.LinkedList<IR> block;
	
	public IRContainer()
	{
		block = new java.util.LinkedList<IR>();
	}
	
	public void addIR(IR ir)
	{
		VarInfo tmpVar = null, res = null;
		if (ir instanceof Quad)
		{
			if (((Quad)ir).op.equals("beq") || ((Quad)ir).op.equals("bne") || ((Quad)ir).op.equals("blt") ||
					((Quad)ir).op.equals("bgt") ||((Quad)ir).op.equals("ble") || ((Quad)ir).op.equals("bge"))		
			{
				if (CheckSimple.notSimple(((Quad)ir).arg1))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg1).getType(), false);
					addIR(new Quad("read", tmpVar, ((Quad)ir).arg1, null));
					((Quad) ir).arg1 = tmpVar;
					addIR(ir);
				}
				else if (CheckSimple.notSimple(((Quad)ir).arg2))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg2).getType(), false);
					addIR(new Quad("read", tmpVar, ((Quad)ir).arg2, null));
					((Quad) ir).arg2 = tmpVar;
					addIR(ir);
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("bnez") || ((Quad)ir).op.equals("beqz") || 
					((Quad)ir).op.equals("divu"))
			{
				if (CheckSimple.notSimple(((Quad)ir).arg1))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg1).getType(), false);
					addIR(new Quad("read", tmpVar, ((Quad)ir).arg1, null));
					((Quad) ir).arg1 = tmpVar;
					addIR(ir);
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("addu") || ((Quad)ir).op.equals("subu") || ((Quad)ir).op.equals("mul") ||
					((Quad)ir).op.equals("and") || ((Quad)ir).op.equals("or") || ((Quad)ir).op.equals("xor") || 
					((Quad)ir).op.equals("sllv") || ((Quad)ir).op.equals("srav") || ((Quad)ir).op.equals("nor") ||
					((Quad)ir).op.equals("slt") || ((Quad)ir).op.equals("sltu") || ((Quad)ir).op.equals("xori"))
			{
				if (CheckSimple.notSimple(((Quad)ir).arg1))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg1).getType(), false);
					addIR(new Quad("read", tmpVar, ((Quad)ir).arg1, null));
					((Quad) ir).arg1 = tmpVar;
					addIR(ir);
				}
				else if (CheckSimple.notSimple(((Quad)ir).arg2))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg2).getType(), false);
					addIR(new Quad("read", tmpVar, ((Quad)ir).arg2, null));
					((Quad) ir).arg2 = tmpVar;
					addIR(ir);
				}
				else if (CheckSimple.notSimple(((Quad)ir).res))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).res).getType(), false);
					res = (VarInfo)((Quad)ir).res;
					((Quad) ir).res = tmpVar;
					addIR(ir);
					addIR(new Quad("write", res, tmpVar, null));
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("mflo") || ((Quad)ir).op.equals("mfhi"))
			{
				if (CheckSimple.notSimple(((Quad)ir).res))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).res).getType(), false);
					res = (VarInfo)((Quad)ir).res;
					((Quad) ir).res = tmpVar;
					addIR(ir);
					addIR(new Quad("write", res, tmpVar, null));
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("read"))
			{
				if (CheckSimple.notSimple(((Quad)ir).res))
				{
					if (CheckSimple.notSimple(((Quad)ir).arg1) || (	((Quad)ir).arg2 != null && 
																																					(!(((Quad)ir).arg2 instanceof Integer) ||
																																					((Integer)((Quad)ir).arg2) != 0 ))	)
					{
						tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).res).getType(), false);
						res = (VarInfo)((Quad)ir).res;
						((Quad) ir).res = tmpVar;
						addIR(ir);
						addIR(new Quad("write", res, tmpVar, null));
					}
					else
					{
						((Quad)ir).op = "write";
						block.add(ir);
					}
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("readstring"))
			{
				if (CheckSimple.notSimple(((Quad)ir).res))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).res).getType(), false);
					res = (VarInfo)((Quad)ir).res;
					((Quad) ir).res = tmpVar;
					addIR(ir);
					addIR(new Quad("write", res, tmpVar, null));
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("write") || ((Quad)ir).op.equals("writebyte") || 
					((Quad)ir).op.equals("writeword"))
			{
				if (CheckSimple.notSimple(((Quad)ir).arg1))
				{
					if (CheckSimple.notSimple(((Quad)ir).res)	)
					{
						tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).arg1).getType(), false);
						addIR(new Quad("read", tmpVar, ((Quad)ir).arg1, null));
						((Quad) ir).arg1 = tmpVar;
						addIR(ir);
					}
					else
					{
						((Quad)ir).op = "read";
						block.add(ir);
					}
				}
				else
					block.add(ir);
			}
			else if (((Quad)ir).op.equals("addr"))
			{
				((VarInfo)((Quad)ir).arg1).spill();
				if (CheckSimple.notSimple(((Quad)ir).res))
				{
					tmpVar = VarInfo.newTmpVar(((VarInfo)((Quad)ir).res).getType(), false);
					res = (VarInfo)((Quad)ir).res;
					((Quad) ir).res = tmpVar;
					addIR(ir);
					addIR(new Quad("write", res, tmpVar, null));
				}
				else
					block.add(ir);
			}
			else
				block.add(ir);
		}
		else
			block.add(ir);
	}
	
	public void concat(IRContainer container)
	{
		block.addAll(container.block);
	}

}
