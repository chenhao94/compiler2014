package compiler.IR;

import compiler.ast.BinExpr;
import compiler.semantics.IntType;
import compiler.semantics.PtrType;
import compiler.semantics.TypePromotion;
import compiler.symbol.Env;

public class BinExprIRGen {

	public static void generatorForBranch(BinExpr expr, Function func, Env env,
			Label trueLabel, Label falseLabel) {
		// TODO Auto-generated method stub
		VarInfo e1 = null, e2 = null, tmpVar = null;
		String ifop = null, iffalseop = null;
		
		switch (expr.opType)
		{
			case EQ	: 	ifop = "beq"; iffalseop = "bne"; break;
			case NE	:  ifop = "bne"; iffalseop = "beq"; break;
			case LT	:  ifop = "blt"; iffalseop = "bge"; break;
			case GT	:  ifop = "bgt"; iffalseop = "ble"; break;
			case LE	:  ifop = "ble"; iffalseop = "bgt"; break;
			case GE	:  ifop = "bge"; iffalseop = "blt"; break;
			default:
					break;
		}
		
		switch (expr.opType)
		{
			case OR		: 
				if (trueLabel != null)
				{
					ExprIRGen.generatorForBranch(expr.expr1, func, env, trueLabel, null);
					ExprIRGen.generatorForBranch(expr.expr2, func, env, trueLabel, falseLabel);
				}
				else
				{
					Label newLabel = new Label("tmp@if" + ((Integer)Label.ifCnt).toString());
					
					ExprIRGen.generatorForBranch(expr.expr1, func, env, newLabel, null);
					ExprIRGen.generatorForBranch(expr.expr2, func, env, trueLabel, falseLabel);
					func.addIR(newLabel);
				}
				return;
				
			case AND		: 
				if (falseLabel != null)
				{
					ExprIRGen.generatorForBranch(expr.expr1, func, env, null, falseLabel);
					ExprIRGen.generatorForBranch(expr.expr2, func, env, trueLabel, falseLabel);
				}
				else
				{
					Label newLabel = new Label("tmp@if" + ((Integer)Label.ifCnt).toString());
					
					ExprIRGen.generatorForBranch(expr.expr1, func, env, null, newLabel);
					ExprIRGen.generatorForBranch(expr.expr2, func, env, trueLabel, falseLabel);
					func.addIR(newLabel);
				}
				return;
				
			case EQ: case NE: case LT: case GT: case LE: case GE: 
				e1 = ExprIRGen.generator(expr.expr1, func, env);
				if (e1.isReturnValue())
				{
					tmpVar = VarInfo.newTmpVar(e1.getType(), e1.isInMem());
					func.addIR(new Quad("read", tmpVar, e1, null));
					e1 = tmpVar;
				}
				e2 = ExprIRGen.generator(expr.expr2, func, env);
				if (e2.isReturnValue())
				{
					tmpVar = VarInfo.newTmpVar(e2.getType(), e2.isInMem());
					func.addIR(new Quad("read", tmpVar, e2, null));
					e2 = tmpVar;
				}
				if (trueLabel != null && falseLabel != null)
				{
					func.addIR(new Quad(ifop, trueLabel, e1, e2));
					func.addIR(new Quad("jump", falseLabel, null, null));
				}
				else if (trueLabel != null)
					func.addIR(new Quad(ifop, trueLabel, e1, e2));
				else if (falseLabel != null)
					func.addIR(new Quad(iffalseop, falseLabel, e1, e2));
				return;
				
			case NOP		: 
				ExprIRGen.generatorForBranch(expr.expr2, func, env, trueLabel, falseLabel);
				return;
				
			default:
				e1 = ExprIRGen.generator(expr, func, env);
				if (trueLabel != null && falseLabel != null)
				{
					func.addIR(new Quad("bnez", trueLabel, e1, null));
					func.addIR(new Quad("jump", falseLabel, null, null));
				}
				else if (trueLabel != null)
					func.addIR(new Quad("bnez", trueLabel, e1, null));
				else if (falseLabel != null)
					func.addIR(new Quad("beqz", falseLabel, e1, null));
				
				return;
		}
	}

	public static VarInfo generator(BinExpr expr, IRContainer container, Env env) {
		// TODO Auto-generated method stub
		
		if (expr.opType == BinExpr.OpType.NOP)
			return ExprIRGen.generator(expr.expr2, container, env);
		VarInfo tmpVar = null, tmpVar1 = null;
		VarInfo e1 = ExprIRGen.generator(expr.expr1, container, env); 
		if (e1.isReturnValue())
		{
			tmpVar = VarInfo.newTmpVar(e1.getType(), e1.isInMem());
			container.addIR(new Quad("read", tmpVar, e1, null));
			e1 = tmpVar;
		}
		VarInfo e2 = ExprIRGen.generator(expr.expr2, container, env);
		if (e2.isReturnValue())
		{
			tmpVar = VarInfo.newTmpVar(e2.getType(), e2.isInMem());
			container.addIR(new Quad("read", tmpVar, e2, null));
			e2 = tmpVar;
		}
		Label l1 = new Label("tmp" + ((Integer)(Label.tmpCnt++)).toString());
		int typeSize;
		
		switch (expr.opType)
		{
			case COM	: case ASS	: case ADDAS: case ANDAS: case DVAS: case MDAS:
				case MUAS: case ORAS: case SBAS: case SLAS: case SRAS: case XRAS:
					break;
			
			default:
				tmpVar = VarInfo.newTmpVar(TypePromotion.promote(e1.getType(),(e2.getType())), false);
		}
		
		switch (expr.opType)
		{
			case COM			: 
				return e2;
			case OR			: 
				container.addIR(new Quad("read", tmpVar, 1, null));
				container.addIR(new Quad("bnez", l1, e1, null));
				container.addIR(new Quad("bnez", l1, e2, null));
				container.addIR(new Quad("read", tmpVar, 0, null));
				container.addIR(l1);
				return tmpVar;
			case AND			: 
				container.addIR(new Quad("read", tmpVar, 0, null));
				container.addIR(new Quad("beqz", l1, e1, null));
				container.addIR(new Quad("beqz", l1, e2, null));
				container.addIR(new Quad("read", tmpVar, 1, null));
				container.addIR(l1);
				return tmpVar;
			case LT			: 
				container.addIR(new Quad("slt", tmpVar, e1, e2));
				return tmpVar;
			case GT			: 
				container.addIR(new Quad("slt", tmpVar, e2, e1));
				return tmpVar;
			case LE			: 
				container.addIR(new Quad("slt", tmpVar, e2, e1));
				container.addIR(new Quad("xori", tmpVar, tmpVar, 1));
				return tmpVar;
			case GE			: 
				container.addIR(new Quad("slt", tmpVar, e1, e2));
				container.addIR(new Quad("xori", tmpVar, tmpVar, 1));
				return tmpVar;
			case EQ			: 
				container.addIR(new Quad("xor", tmpVar, e1, e2));
				container.addIR(new Quad("sltu", tmpVar, tmpVar, 1));
				return tmpVar;
			case NE			: 
				container.addIR(new Quad("xor", tmpVar, e1, e2));
				container.addIR(new Quad("sltu", tmpVar, 0, tmpVar));
				return tmpVar;				
			case ASS			:
				container.addIR(new Quad("read", e1, e2, null)); 
				return e1;
			case ADDAS	: 
				if (e1.getType().isPtrType() && (typeSize = ((PtrType)e1.getType()).type.getSize()) > 1)
					container.addIR(new Quad("mul", tmpVar1, e2, typeSize));
				else
					tmpVar1 = e2;
				container.addIR(new Quad("addu", e1, e1, tmpVar1)); 
				return e1;
			case SBAS		:
				if (e1.getType().isPtrType() && (typeSize = ((PtrType)e1.getType()).type.getSize()) > 1)
					container.addIR(new Quad("mul", tmpVar1, e2, typeSize));
				else
					tmpVar1 = e2;
				container.addIR(new Quad("subu", e1, e1, tmpVar1)); 
				return e1;
			case MUAS		: 
				container.addIR(new Quad("mul", e1, e1, e2));
				return e1;
			case DVAS		:
				container.addIR(new Quad("divu", null, e1, e2));
				container.addIR(new Quad("mflo", e1, null, null));
				return e1;
			case MDAS		:
				container.addIR(new Quad("divu", null, e1, e2));
				container.addIR(new Quad("mfhi", e1, null, null));
				return e1;
			case ANDAS	: 
				container.addIR(new Quad("and", e1, e1, e2));
				return e1;
			case ORAS		:
				container.addIR(new Quad("or", e1, e1, e2));
				return e1;
			case XRAS		:
				container.addIR(new Quad("xor", e1, e1, e2));
				return e1;
			case SLAS		:
				container.addIR(new Quad("sllv", e1, e1, e2));
				return e1;
			case SRAS		:
				container.addIR(new Quad("srav", e1, e1, e2));
				return e1;			
			case BAR			: 
				container.addIR(new Quad("or", tmpVar, e1, e2));
				return tmpVar;
			case XOR			: 
				container.addIR(new Quad("xor", tmpVar, e1, e2));
				return tmpVar;
			case AMP			: 
				container.addIR(new Quad("and", tmpVar, e1, e2));
				return tmpVar;
			case SHL			: 
				container.addIR(new Quad("sllv", tmpVar, e1, e2)); 
				return tmpVar;
			case SHR			:
				container.addIR(new Quad("srav", tmpVar, e1, e2)); 
				return tmpVar;			
			case PLUS		:
				if (e1.getType().isPtrType() || e2.getType().isPtrType())
				{
					tmpVar1 = VarInfo.newTmpVar(IntType.getInstance(), false);
					if (e1.getType().isIntegerType())
					{
						typeSize = ((PtrType)e2.getType()).type.getSize();
						if (typeSize > 1)
							container.addIR(new Quad("mul", tmpVar1, e1, typeSize));
						else
							tmpVar1 = e1;
						container.addIR(new Quad("addu", tmpVar, tmpVar1, e2));
					}
					else
					{	
						typeSize = ((PtrType)e1.getType()).type.getSize();
						if (typeSize > 1)
							container.addIR(new Quad("mul", tmpVar1, e2, typeSize));			
						else
							tmpVar1 = e2;
						container.addIR(new Quad("addu", tmpVar, e1, tmpVar1));
					}
				}
				else
					container.addIR(new Quad("addu", tmpVar, e1, e2));
				return tmpVar;
			case MINUS	:
				if (e1.getType().isPtrType() && (typeSize = ((PtrType)e1.getType()).type.getSize()) > 1)
					container.addIR(new Quad("mul", tmpVar1, e2, typeSize));
				else
					tmpVar1 = e2;
				container.addIR(new Quad("subu", tmpVar, e1, tmpVar1));
				return tmpVar;
			case MUL			: 
				container.addIR(new Quad("mul", tmpVar, e1, e2)); 
				return tmpVar;
			case DIV			: 
				container.addIR(new Quad("divu", null, e1, e2));
				container.addIR(new Quad("mflo", tmpVar, null, null));
				return tmpVar;
			case MOD			: 
				container.addIR(new Quad("divu", null, e1, e2));
				container.addIR(new Quad("mfhi", tmpVar, null, null));
				return tmpVar;
			default			:
				return tmpVar;
		}
		
	}

}
