package compiler.IR;

public class GlobalIRs extends IRContainer {
	
	private static GlobalIRs instance = new GlobalIRs();
	public static int addr = 0x10000000;
	
	private GlobalIRs()
	{
		super();
	}
	
	public static GlobalIRs getInstance()
	{
		return instance;
	}

}
