package compiler.IR;

public class VarLabel extends IR {

	public String name;
	public int align;
	public VarInfo varInfo;
	
	public VarLabel(VarInfo _varInfo)
	{
		name = _varInfo.getName().substring(1);
		align = _varInfo.getType().alignSize();
		varInfo = _varInfo;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + ":";
	}

}
