package compiler.IR;

import compiler.ast.ForStmt;
import compiler.symbol.Env;

public class ForStmtIRGen {

	public static void generator(ForStmt stmt, Function func, Env env) {
		// TODO Auto-generated method stub
		Label beginFor = new Label("beginFor" + ((Integer)Label.forCnt).toString());
		Label continueFor = new Label("continueFor" + ((Integer)Label.forCnt).toString());
		Label endFor = new Label("endFor" + ((Integer)Label.forCnt).toString());
		LoopInfo loopInfo = new LoopInfo(continueFor, endFor);
		
		if (stmt.expr1 != null)
			ExprIRGen.generator(stmt.expr1, func, env);
		func.addIR(beginFor);
		if (stmt.expr2 != null)
			ExprIRGen.generatorForBranch(stmt.expr2, func, env, null, endFor);
		StmtIRGen.generator(stmt.stmt, func, env, loopInfo);
		func.addIR(continueFor);
		if (stmt.expr3 != null)
			ExprIRGen.generator(stmt.expr3, func, env);
		func.addIR(new Quad("jump", beginFor, null, null));
		func.addIR(endFor);
		
		++Label.forCnt;
	}

}
