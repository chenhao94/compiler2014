package compiler.registerAllocation;

import compiler.IR.Funcs;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.VarInfo;
import compiler.IR.Quad;

public class RegisterUse {
	
	public static void analysis() {
		// TODO Auto-generated method stub
		String funcName;
		Function callee, nowFunc;
		java.util.LinkedList<Function> funcQueue = new java.util.LinkedList<Function>();
		for (Function func: Funcs.funcs.values())
		{
			for (IR quad: func.block)
				if (quad instanceof Quad && ((Quad)quad).op.equals("call"))
				{
					funcName = ((VarInfo)((Quad)quad).arg2).toString().substring(1);
					callee = Funcs.funcs.get(funcName);
					if (callee != null && !callee.equals(func))
						callee.calledBy(func);
				}
			funcQueue.add(func);
		}
		
		while (!funcQueue.isEmpty())
		{
			nowFunc = funcQueue.poll();
			for (Function caller: nowFunc.getCallers())
				if (caller.mergeRegsFromCallee(nowFunc) && !funcQueue.contains(caller))
					funcQueue.add(caller);
		}
	}

}
