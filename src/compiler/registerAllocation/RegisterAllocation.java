package compiler.registerAllocation;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import compiler.IR.Function;
import compiler.IR.VarInfo;

public class RegisterAllocation {
	
	static private java.util.ArrayList<VarInfo> liveIntervals = new java.util.ArrayList<VarInfo>();
	static private java.util.PriorityQueue<VarInfo> active = 
			new java.util.PriorityQueue<VarInfo> (32, new Comparator<VarInfo>(){
				@Override
				public int compare(VarInfo arg1, VarInfo arg2)
				{
					return arg2.getEnd() - arg1.getEnd();
				}
			});
	
	public static void allocate(Function func)
	{
		liveIntervals.clear();
		active.clear();
		for (VarInfo varInfo: func.getVarsForRegAlloc())
			liveIntervals.add(varInfo);
		Collections.sort(liveIntervals, new Comparator<VarInfo>() {
			@Override
			public int compare(VarInfo arg1, VarInfo arg2)
			{
				return arg1.getStart() - arg2.getStart();
			}
		});
		RegisterPool.readyToAllocate();
		Register reg;
		for (VarInfo varInfo: liveIntervals)
		{
			expireOldIntervals(varInfo);
			reg = RegisterPool.getReg();
			func.addUse(reg);
			if (reg == null)
				spillAtInterval(varInfo);
			else
			{
				varInfo.setReg(reg.getID());
				active.add(varInfo);
			}
		}
		
	}

	private static void spillAtInterval(VarInfo varInfo) {
		// TODO Auto-generated method stub
		
		VarInfo spill = active.peek();
		if (spill.getEnd() > varInfo.getEnd())
		{
			varInfo.setReg(spill.getRegID());
			spill.spill();
			active.poll();
			active.add(varInfo);
			
			
		}
		else
			varInfo.spill();
		
	}

	private static void expireOldIntervals(VarInfo varInfo) {
		// TODO Auto-generated method stub
		Iterator<VarInfo> itr = active.iterator();
		VarInfo activeVar = null;
		while (itr.hasNext())
		{	
			activeVar = itr.next();
			if (activeVar.getEnd() < varInfo.getStart())
			{
				RegisterPool.putReg(activeVar.getReg());
				itr.remove();
			}
		}
	}

}
