package compiler.registerAllocation;

import compiler.IR.VarInfo;

public class Register {
	
	final private int ID;
	final private String name;
	private VarInfo varInfo; // != null means not write back yet
	
	protected Register(int _id, String _name)
	{
		ID = _id;
		name = _name;
		varInfo = null;
	}
	
	public int getID()
	{
		return ID;
	}
	
	public VarInfo getVar()
	{
		return varInfo;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void use(VarInfo _var)
	{
		varInfo = _var;
	}
	
	public void release()
	{
		varInfo = null;
	}

}
