package compiler.registerAllocation;

import java.util.Map.Entry;
import java.util.Random;

public class RegisterPool {
	
	public static java.util.HashMap<Integer, Register> registerPool = new java.util.HashMap<Integer, Register>();
	private static java.util.LinkedList<Register> availableRegister = new java.util.LinkedList<Register>();
	private static String[] registerNamingPool = {
		"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9", 
		"$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7"
	};
	
	public static void init()
	{
		Register reg = null;
		int id = 1;
		
		for (String registerName: registerNamingPool)
		{
			reg = new Register(id, registerName);
			registerPool.put(id++, reg);
		}
	}
	
	public static String getRegName(int id)
	{
		return registerNamingPool[id];
	}
	
	public static Register getReg(int id)
	{
		return registerPool.get(id);
	}
	
	public static void readyToAllocate()
	{
		availableRegister.clear();
		for (Entry<Integer, Register> entry: registerPool.entrySet())
			availableRegister.push(entry.getValue());
	}
	
	public static Register getReg()
	{
		if (availableRegister.isEmpty())
			return null;
		int length = availableRegister.size(), loc = Math.abs(new Random().nextInt()) % length;
		Register reg = availableRegister.get(loc);
		availableRegister.remove(loc);
		return reg;
	}
	
	public static void putReg(Register reg)
	{
		availableRegister.push(reg);
	}

	public static int availableSize() {
		// TODO Auto-generated method stub
		return availableRegister.size();
	}

}
