package compiler.basicblocks;

import java.util.BitSet;

import compiler.IR.BoundVar;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.OffsettedVar;
import compiler.IR.Quad;
import compiler.IR.VarInfo;

public class LivelinessAnalysis {
	
	public static void analysis(Function func)
	{
		int varID, quadID;
		VarInfo tmpVar;
		boolean inChangedFlag = true;
		java.util.BitSet tmpIn, tmp;
		
		init(func);
		
		for (Node node: func.getNode())
		{
			for (IR ir: node.block)
				if (ir instanceof Quad)
				{
					quadID = ((Quad)ir).getID();
					
					if (((Quad)ir).arg1 instanceof VarInfo)
					{
						tmpVar = (VarInfo)((Quad)ir).arg1;
						while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
						{	
							if (tmpVar instanceof OffsettedVar)
								tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
							else
								tmpVar = ((BoundVar)tmpVar).getTarget();
						}
						if (!tmpVar.isSpilled() && !tmpVar.isArg())
						{
							// use
							varID = tmpVar.getID();
							if (!node.def.get(varID))
								node.use.set(varID);
							tmpVar.livelyIn(quadID);
							if (!tmpVar.isReturnValue())
								func.addVarForRegAlloc(tmpVar);
						}
					}
					
					if (((Quad)ir).arg2 instanceof VarInfo)
					{
						tmpVar = (VarInfo)((Quad)ir).arg2;
						while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
						{	
							if (tmpVar instanceof OffsettedVar)
								tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
							else
								tmpVar = ((BoundVar)tmpVar).getTarget();
						}
						if (!tmpVar.isSpilled() && !tmpVar.isArg())
						{
							// use
							varID = tmpVar.getID();
							if (!node.def.get(varID))
								node.use.set(varID);
							tmpVar.livelyIn(quadID);
							if (!tmpVar.isReturnValue())
								func.addVarForRegAlloc(tmpVar);
						}
					}
					
					if (((Quad)ir).res instanceof VarInfo)
					{
						tmpVar = (VarInfo)((Quad)ir).res;
						if (!tmpVar.isSpilled() && !tmpVar.isArg())
						{
							// def
							varID = tmpVar.getID();
							if (!node.use.get(varID))
								node.def.set(varID);
							tmpVar.livelyIn(quadID + 1);
							if (!tmpVar.isReturnValue())
								func.addVarForRegAlloc(tmpVar);
						}
						while (tmpVar instanceof OffsettedVar || tmpVar instanceof BoundVar)
						{	
							if (tmpVar instanceof OffsettedVar)
								tmpVar = ((OffsettedVar)tmpVar).getBasicVar();
							else
								tmpVar = ((BoundVar)tmpVar).getTarget();
						}
						if (!tmpVar.isSpilled() && !tmpVar.isArg())
						{
							// def
							varID = tmpVar.getID();
							if (!node.def.get(varID))
								node.use.set(varID);
							tmpVar.livelyIn(quadID);
							if (!tmpVar.isReturnValue())
								func.addVarForRegAlloc(tmpVar);
						}
					}
					
				}
			node.in.clear();
			
		}
		
		while (inChangedFlag)
		{
			inChangedFlag = false;
			for (Node node: func.getNode())
				if (!node.isExit())
				{
					tmpIn = (BitSet) node.in.clone();
					node.out.clear();
					for (Node succ: node.edges)
						node.out.or(succ.in);
					node.in = (BitSet) node.use.clone();
					tmp = ((BitSet)node.out.clone());
					tmp.andNot(node.def);
					node.in.or(tmp);
					
					if (!node.in.equals(tmpIn))
						inChangedFlag = true;
				}
		}
		
		for (Node node: func.getNode())
			if (node.begin <= node.end)
			{
				for (int i = node.in.nextSetBit(0); i >= 0; i = node.in.nextSetBit(i+1))
					VarInfo.livelyIn(i, node.begin);
				
				for (int i = node.out.nextSetBit(0); i >= 0; i = node.out.nextSetBit(i+1))
					VarInfo.livelyIn(i, node.end);
			}
		
		
	}

	private static void init(Function func) {
		// TODO Auto-generated method stub
		for (VarInfo varInfo: func.getVarsForRegAlloc())
			varInfo.initForLiveliness();
	}
	
	
}
