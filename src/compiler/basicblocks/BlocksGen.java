package compiler.basicblocks;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.Label;
import compiler.IR.Quad;

public class BlocksGen {
	
	public static void generator(Function func)
	{
		Node.init();
		java.util.LinkedList<Node> nodes = new java.util.LinkedList<Node>();
		Node node = new Node(null), nextNode = null;
		String op;
		int id = 0;
		node.begin = 0;
		nodes.add(node);
		node.end = id - 1;
		node = new Node(null);
		node.begin = id;
		nodes.add(node);
		for (IR ir: func.block)
		{
			if (ir instanceof Label)
			{
				node.end = id - 1;
				node = new Node((Label)ir);
				node.begin = id;
				nodes.add(node);
			}
			else
			{
				((Quad)ir).setID(id++);
				op = ((Quad)ir).op;
				if (op.charAt(0) == 'b' || op.equals("jump"))
				{
					node.addContent((Quad)ir);
					node.addBranch((Quad)ir);
					node.end = id - 1;
					node = new Node(null);
					node.begin = id;
					nodes.add(node);
				}
				else
					node.addContent((Quad)ir);
			}
		}
		node.end = id - 1;
		node = new Node(null);
		node.begin = id;
		node.end = id - 1;
		nodes.add(node);
		
		java.util.ListIterator<Node> itr = nodes.listIterator(0);
		
		while (itr.hasNext())
		{
			node = itr.next();
			if (node.endBranch != null)
				op = node.endBranch.op;
			else
				op = "x";
			if (!op.equals("jump") && itr.hasNext())
			{	
				nextNode = itr.next();
				itr.previous();
				node.addEdge(nextNode);
			}
			if (op.charAt(0) == 'b' || op.equals("jump"))
				for (Node targetNode: nodes)
					if (node.endBranch.res.equals(targetNode.startLabel))
					{
						node.addEdge(targetNode);
						break;
					}
		}
		
		func.setNode(nodes);
	}

	public static void merge(Function func)
	{
		java.util.LinkedList<IR> IRs = new java.util.LinkedList<IR>();
		
		for (Node node: func.getNode())
		{
			if (node.startLabel != null)
				IRs.add(node.startLabel);
			IRs.addAll(node.block);
		}
		
		func.block = IRs;
	}

	public static void printNodes(Function func) throws FileNotFoundException
	{
		
		PrintWriter out = new PrintWriter("/home/ch94/basicBlocksReport/" + func.name.toString() + ".log");
		
		for (Node node: func.getNode())
		{
			out.println("=== " + node.getID() + " from " + node.begin + " to " + node.end + " ===========");
			if (node.startLabel != null)
				out.println(node.startLabel.toString());
			out.println("================================");
			for (IR ir: node.block)
				out.println("ID:" + ((Quad)ir).getID() + "\t\t" + ir.toString());
			out.println("================================");
			if (node.endBranch != null)
				out.println("ID:" + node.endBranch.getID() + "\t\t" + node.endBranch.toString());
			out.print("======== goto ID: ");
			for (Node nextNode: node.edges)
				out.print(nextNode.getID() + " ");
			out.println("===============\n");
			
		}
		
		out.close();
	}
	
}
