package compiler.basicblocks;

import compiler.IR.Label;
import compiler.IR.Quad;

public class Node {
	
	public java.util.LinkedList<Node> edges;
	public Label startLabel;
	public java.util.LinkedList<Quad> block;
	public Quad endBranch;
	private int nodeID;
	public java.util.BitSet in, out, def, use;
	public int begin, end; // quadID
	static private int nodeTot = 0;
	
	public Node(Label start)
	{
		edges = new java.util.LinkedList<Node>();
		block = new java.util.LinkedList<Quad>();
		startLabel = start;
		endBranch = null;
		nodeID = nodeTot++;
		in = new java.util.BitSet();
		out = new java.util.BitSet();
		def = new java.util.BitSet();
		use = new java.util.BitSet();
	}
	
	public void addEdge(Node n)
	{
		edges.add(n);
	}
	
	public int getID()
	{
		return nodeID;
	}
	
	public void addContent(Quad ir)
	{
		block.add(ir);
	}
	
	public void addBranch(Quad ir)
	{
		endBranch = ir;
	}
	
	public boolean isEntry()
	{
		return nodeID == 0;
	}
	
	public boolean isExit()
	{
		return nodeID == nodeTot - 1;
	}
	
	public static void init()
	{	
		nodeTot = 0;
	}
	
}
