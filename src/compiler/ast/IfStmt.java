package compiler.ast;

import java.io.PrintWriter;

public class IfStmt extends Stmt implements PrintTree
{
	public Expr expr;
	public Stmt stmt, alter;

	public IfStmt(Expr e, Stmt s, Stmt a, int l, int c)
	{
		super(l, c);
		expr = e;
		stmt = s;
		alter = a;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		if (alter == null)
		{
			out.printf("IF\n");
			expr.printTree(level+1, out);
			stmt.printTree(level+1, out);
		}
		else
		{
			out.printf("IFELSE\n");
			expr.printTree(level+1, out);
			stmt.printTree(level+1, out);
			alter.printTree(level+1, out);
		}
	}
}

