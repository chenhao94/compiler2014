package compiler.ast;

import java.io.PrintWriter;

public class PstfxExpr extends Expr implements PrintTree
{
	public Expr expr;
	public Pstfx pstfx;

	public PstfxExpr(Expr e, Pstfx p, int l, int c)
	{
		super(l, c);
		expr = e;
		pstfx = p;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (pstfx != null)
			pstfx.printTree(level, out);
		expr.printTree(level+1, out);
	}

	@Override
	public boolean lvalue() {
		if (pstfx == null)
			return expr.lvalue();
		else if (pstfx instanceof ArgPstfx)
			return false;
		else if (pstfx instanceof DotPstfx)
			return true;
		else if (pstfx instanceof OpPstfx)
			return false;
		else if (pstfx instanceof PtrPstfx)
			return true;
		else if (pstfx instanceof SqbPstfx)
			return true;
		return false;
	}
}
