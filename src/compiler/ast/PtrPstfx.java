package compiler.ast;

import java.io.PrintWriter;

import compiler.symbol.Symbol;

public class PtrPstfx extends Pstfx implements PrintTree
{
	public Symbol symbol;

	public PtrPstfx(Symbol s, int l, int c)
	{
		super(l, c);
		symbol = s;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println("->" + symbol.toString());
	}
}
