package compiler.ast;

import java.io.PrintWriter;

public class Decls extends ASTNode implements PrintTree
{
	public Decl decl;
	public Decls next;

	public Decls(Decl s, Decls n, int l, int c)
	{
		super(l, c);
		decl = s;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		decl.printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
