package compiler.ast;

import java.io.PrintWriter;

public class ReturnStmt extends JmpStmt implements PrintTree
{
	public Expr expr;

	public ReturnStmt(Expr e, int l, int c)
	{
		super(JmpStmt.JmpType.RETURN, l, c);
		expr = e;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.printf("RETURN\n");
		expr.printTree(level+1, out);
	}
}
