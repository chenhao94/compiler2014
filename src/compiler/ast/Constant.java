package compiler.ast;

import java.io.PrintWriter;

abstract public class Constant extends ASTNode
{

	abstract public void printTree(int level, PrintWriter out);
	
	public Constant(int l, int c)
	{
		super(l, c);
	}

}
