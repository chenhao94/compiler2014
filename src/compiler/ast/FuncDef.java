package compiler.ast;

import java.io.PrintWriter;

public class FuncDef extends ASTNode implements PrintTree
{
	public TypSpec typeSpec;
	public PlnDeclrtr plnDeclrtr;
	public Paramtrs paramtrs;
	public CmpStmt cmpStmt;

	public FuncDef(TypSpec t, PlnDeclrtr pd, Paramtrs pm, CmpStmt c, int li, int co)
	{
		super(li, co);
		typeSpec = t;
		plnDeclrtr = pd;
		paramtrs = pm;
		cmpStmt = c;
	}

	public void printTree(int level, PrintWriter out)
	{
		typeSpec.printTree(level, out);
		plnDeclrtr.printTree(level+1, out);
		if (paramtrs != null)
			paramtrs.printTree(level+2, out);
		
		cmpStmt.printTree(level+1, out);
		
	}
}
