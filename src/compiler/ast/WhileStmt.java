package compiler.ast;

import java.io.PrintWriter;

public class WhileStmt extends ItrStmt implements PrintTree
{
	public Expr expr;
	public Stmt stmt;

	public WhileStmt(Expr e, Stmt s, int l, int c)
	{
		super(l, c);
		expr = e;
		stmt = s;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.printf("WHILE\n");
		expr.printTree(level+1, out);
		stmt.printTree(level+1, out);
	}
}


