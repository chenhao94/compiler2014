package compiler.ast;

import java.io.PrintWriter;

import compiler.semantics.Type;

public class Decl extends ASTNode implements PrintTree
{
	public TypSpec typeSpec;
	public InitDeclrtrs initDeclrtrs;

	public Decl(TypSpec t, InitDeclrtrs i, int l, int c)
	{
		super(l, c);
		typeSpec = t;
		initDeclrtrs = i;
	}

	public void printTree(int level, PrintWriter out)
	{
		typeSpec.printTree(level, out);
		if (initDeclrtrs != null)
			initDeclrtrs.printTree(level+1, out);
	}
}
