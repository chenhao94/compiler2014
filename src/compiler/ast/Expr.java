package compiler.ast;

import java.io.PrintWriter;

import compiler.semantics.Type;

abstract public class Expr extends Stmt implements PrintTree
{
	public Type type = null;
	
	public Expr(int l, int c)
	{
		super(l, c);
	}
	
	abstract public void printTree(int level, PrintWriter out);
	
	abstract public boolean lvalue();
}
