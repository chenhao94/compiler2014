package compiler.ast;

import java.io.PrintWriter;

public class IntConst extends Constant implements PrintTree
{
	public int value;

	public IntConst(int v, int l, int c)
	{
		super(l, c);
		value = v;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println(value);
	}
}
