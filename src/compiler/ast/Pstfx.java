package compiler.ast;

import java.io.PrintWriter;

abstract public class Pstfx extends ASTNode implements PrintTree
{
	public Pstfx(int l, int c)
	{
		super(l, c);
	}
	
	abstract public void printTree(int level, PrintWriter out);	
}
