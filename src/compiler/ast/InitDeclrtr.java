package compiler.ast;

import java.io.PrintWriter;

import compiler.semantics.Type;
import compiler.symbol.EnvInfo;

public class InitDeclrtr extends ASTNode implements PrintTree
{
	public Declrtr declrtr;
	public Initial initial;
	public EnvInfo envInfo;
	public Type type;

	public InitDeclrtr(Declrtr d, Initial i, int l, int c)
	{
		super(l, c);
		declrtr = d;
		initial = i;
		envInfo = null;
	}

	public void printTree(int level, PrintWriter out)
	{
		declrtr.printTree(level, out);
		if (initial != null)
			initial.printTree(level+1, out);
	}
}
