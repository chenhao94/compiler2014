package compiler.ast;

import java.io.PrintWriter;

public class RecordComps extends ASTNode implements PrintTree
{
	public TypSpec typSpec;
	public Declrtrs declrtrs;
	public RecordComps next;

	public RecordComps(TypSpec t, Declrtrs d, RecordComps n, int l, int c)
	{
		super(l, c);
		typSpec = t;
		declrtrs = d;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		typSpec.printTree(level, out);
		declrtrs.printTree(level+1, out);
		if (next != null)
			next.printTree(level, out);
	}
}
