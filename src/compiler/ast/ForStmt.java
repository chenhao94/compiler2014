package compiler.ast;

import java.io.PrintWriter;

public class ForStmt extends ItrStmt implements PrintTree
{
	public Expr expr1, expr2, expr3;
	public Stmt stmt;

	public ForStmt(Expr e1, Expr e2, Expr e3, Stmt s, int l, int c)
	{
		super(l, c);
		expr1 = e1;
		expr2 = e2;
		expr3 = e3;
		stmt = s;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.printf("FOR\n");
		expr1.printTree(level+1, out);
		expr2.printTree(level+1, out);
		expr3.printTree(level+1, out);
		stmt.printTree(level+1, out);
	}
}

