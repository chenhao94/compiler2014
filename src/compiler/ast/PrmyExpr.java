package compiler.ast;

import compiler.symbol.EnvInfo;
import compiler.symbol.Symbol;
import java.io.PrintWriter;

public class PrmyExpr extends Expr implements PrintTree
{
	public Object obj;
	public EnvInfo envInfo;

	public PrmyExpr(Object o, int l, int c)
	{
		super(l, c);
		envInfo = null;
		obj = o;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (obj instanceof Symbol)
		{
			PrintLevel.printLevel(level, out);
			out.println(((Symbol)obj).toString());
		}
		else if (obj instanceof Constant)
			((Constant)obj).printTree(level, out);
		else if (obj instanceof String)
		{
			PrintLevel.printLevel(level, out);
			out.println((String)obj);
		}
		else if (obj instanceof Expr)
			((Expr)obj).printTree(level, out);
	}

	@Override
	public boolean lvalue() {
		if (obj instanceof Symbol)
			return true;
		else if (obj instanceof Expr)
			return ((Expr)obj).lvalue();
		else
			return false;
	}
	
}
