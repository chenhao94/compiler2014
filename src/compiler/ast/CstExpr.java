package compiler.ast;

import java.io.PrintWriter;

public class CstExpr extends Expr implements PrintTree
{
	public TypSpec typeSpec;
	public Expr expr;

	public CstExpr(TypSpec t, Expr e, int l, int c)
	{
		super(l, c);
		typeSpec = t;
		expr = e;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (typeSpec == null)
			expr.printTree(level, out);
		else
		{
			typeSpec.printTree(level, out);
			expr.printTree(level+1, out);
		}
	}

	@Override
	public boolean lvalue() {
		if (typeSpec == null)
			return expr.lvalue();
		else
			return false;
	}
}
