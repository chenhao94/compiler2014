package compiler.ast;

import java.io.PrintWriter;

public class Stmts extends ASTNode implements PrintTree
{
	public Stmt stmt;
	public Stmts next;

	public Stmts(Stmt s, Stmts n, int l, int c)
	{
		super(l, c);
		stmt = s;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		stmt.printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
