package compiler.ast;

import java.io.PrintWriter;

import compiler.symbol.Env;

public class CmpStmt extends Stmt implements PrintTree
{
	public Decls decls;
	public Stmts stmts;
	public Env env;

	public CmpStmt(Decls d, Stmts s, int l, int c)
	{
		super(l, c);
		decls = d;
		stmts = s;
		env = null;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println("<BEGIN>");
		
		if (decls != null)
			decls.printTree(level+2, out);
		if (stmts != null)
			stmts.printTree(level+2, out);
		
		PrintLevel.printLevel(level, out);
		out.println("<END>");
	}
}

