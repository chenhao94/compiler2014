package compiler.ast;

public abstract class ASTNode{

	public int line, column;
	
	public ASTNode(int l, int c)
	{
		line = l;
		column = c;
	}
}
