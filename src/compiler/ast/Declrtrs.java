package compiler.ast;

import java.io.PrintWriter;

public class Declrtrs extends ASTNode implements PrintTree
{
	public Declrtr declrtr;
	public Declrtrs next;

	public Declrtrs(Declrtr d, Declrtrs n, int l, int c)
	{
		super(l, c);
		declrtr = d;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		declrtr.printTree(level, out);
		if (next != null)
			next.printTree(level+1, out);
	}
}
