package compiler.ast;

import java.io.PrintWriter;

public class InitDeclrtrs extends ASTNode
{
	public InitDeclrtr initDeclrtr;
	public InitDeclrtrs next;

	public InitDeclrtrs(InitDeclrtr i, InitDeclrtrs n, int l, int c)
	{
		super(l, c);
		initDeclrtr = i;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		initDeclrtr.printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
