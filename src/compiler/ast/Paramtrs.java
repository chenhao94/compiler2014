package compiler.ast;

import java.io.PrintWriter;

public class Paramtrs extends ASTNode implements PrintTree
{
	public PlnDecl plnDecl;
	public Paramtrs next;

	public Paramtrs(PlnDecl p, Paramtrs n, int l, int c)
	{
		super(l, c);
		plnDecl = p;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		plnDecl.printTree(level, out);
		if (next != null)
			next.printTree(level+1, out);
	}

}
