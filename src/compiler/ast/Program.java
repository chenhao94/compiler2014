package compiler.ast;

import java.io.PrintWriter;

import compiler.symbol.Env;

public class Program extends ASTNode implements PrintTree
{
	public Object object;
	public Program next;
	public Env env;

	public Program(Object o, Program n, int l, int c)
	{
		super(l, c);
		object = o;
		next = n;
		env = null;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (object instanceof Decl)
			((Decl)object).printTree(level, out);
		else
			((FuncDef)object).printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
