package compiler.ast;

import java.io.PrintWriter;

import compiler.semantics.Type;

public class UnaryExpr extends Expr implements PrintTree
{
	public Expr expr;
	public TypSpec typName;
	public Type type;
	public OpType opType;

	public static enum OpType
	{
		PINC, PDEC, AMP, STAR, POS, NEG, TILD, EXCL, SIZEOF, NOP
	}

	public UnaryExpr(Expr e, OpType op, int l, int c)
	{
		super(l, c);
		expr = e;
		typName = null;
		type = null;
		if (op != null)
			opType = op;
		else
			opType = OpType.NOP;
	}

	public UnaryExpr(TypSpec t, OpType op, int l, int c)
	{
		super(l, c);
		expr = null;
		typName = t;
		opType = op;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (typName == null)
		{
			if (opType == null)
				expr.printTree(level, out);
			else
			{
				PrintLevel.printLevel(level, out);
				switch (opType)
				{
					case PINC: out.printf("PINC\n"); break;
					case PDEC: out.printf("PDEC\n"); break;
					case AMP: out.printf("AMP\n");  break;
					case STAR: out.printf("STAR\n"); break;
					case POS: out.printf("POS\n"); break;
					case NEG: out.printf("NEG\n"); break;
					case TILD: out.printf("TILD\n"); break;
					case EXCL: out.printf("EXCL\n"); break;
					case SIZEOF: out.printf("SIZEOF\n"); break;
				}
				expr.printTree(level+1, out);
			}
		}
		else
		{
			PrintLevel.printLevel(level, out);
			out.printf("SIZEOF\n");
			typName.printTree(level+1, out);
		}
	}

	@Override
	public boolean lvalue() {
		if (typName == null)
		{
			if (opType == OpType.NOP)
				return expr.lvalue();
			else if (opType == OpType.STAR)
				return true;
			else
				return false;
		}
		else
			return false;
	}
}
