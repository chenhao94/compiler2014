package compiler.ast;

import java.io.PrintWriter;

public class CharConst extends Constant implements PrintTree
{
	public char value;

	public CharConst(char v, int l, int c)
	{
		super(l, c);
		value = v;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println(value);
	}
}
