package compiler.ast;

import java.io.PrintWriter;

public class Initials extends ASTNode implements PrintTree
{
	public Initial initial;
	public Initials next;

	public Initials(Initial i, Initials n, int l, int c)
	{
		super(l, c);
		initial = i;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		initial.printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
