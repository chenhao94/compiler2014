package compiler.ast;

import java.io.PrintWriter;

public class OpPstfx extends Pstfx implements PrintTree
{
	public OpType opType;

	public static enum OpType
	{
		INC, DEC
	}

	public OpPstfx(OpType o, int l, int c)
	{
		super(l, c);
		opType = o;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		if (opType == OpType.INC)
			out.println("INC");
		else
			out.println("DEC");
	}
}
