package compiler.ast;

import java.io.PrintWriter;

public class SqbPstfx extends Pstfx implements PrintTree
{
	public Expr expr;

	public SqbPstfx(Expr e, int l, int c)
	{
		super(l, c);
		expr = e;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println("[]");
		expr.printTree(level+1, out);
	}
}
