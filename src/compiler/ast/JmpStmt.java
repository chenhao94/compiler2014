package compiler.ast;

import java.io.PrintWriter;

public class JmpStmt extends Stmt implements PrintTree
{
	public JmpType jmpType;

	public static enum JmpType
	{
		CONTINUE, BREAK, RETURN
	}

	public JmpStmt(JmpType t, int l, int c)
	{
		super(l, c);
		jmpType = t;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		if (jmpType == JmpType.CONTINUE)
			out.printf("CONTINUE\n");
		else if (jmpType == JmpType.BREAK)
			out.printf("BREAK\n");
	}
}

