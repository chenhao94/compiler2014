package compiler.ast;

import java.io.PrintWriter;

import compiler.symbol.Symbol;

public class TypSpec extends ASTNode implements PrintTree
{
	public TypeID typeID;
	public Symbol symbol;
	public RecordComps comps;
	public int starCnt;

	public static enum TypeID
	{
		VOID, CHAR, INT, STRUCT, UNION
	}

	public TypSpec(TypeID t, int l, int c)
	{
		super(l, c);
		typeID = t;
		symbol = null;
		comps = null;
		starCnt = 0;
	}

	public TypSpec(TypeID t, Symbol s, RecordComps c, int li, int co)
	{
		super(li, co);
		typeID = t;
		symbol = s;
		comps = c;
		starCnt = 0;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		if (starCnt > 0)
			out.printf("%d*", starCnt);
		switch (typeID)
		{
			case VOID: out.printf("VOID");break;
			case CHAR: out.printf("CHAR");break;
			case INT: out.printf("INT");break;
			case STRUCT: out.printf("STRUCT");break;
			case UNION: out.printf("UNION");break;
		}
		if (comps == null)
		{
			if (symbol != null)
				out.print("  " + symbol.toString());
			out.println();
		}
		else
		{
			if (symbol != null)
				out.print("  " + symbol.toString());
			out.println();
			comps.printTree(level+1, out);
		}
	}
}
