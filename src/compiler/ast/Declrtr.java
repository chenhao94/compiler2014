package compiler.ast;

import java.io.PrintWriter;

public class Declrtr extends ASTNode implements PrintTree
{
	public PlnDeclrtr plnDeclrtr;
	public Object suffix;
	boolean hasParamtr;

	public Declrtr(PlnDeclrtr p, Object s, boolean hasPar, int l, int c)
	{
		super(l, c);
		plnDeclrtr = p;
		suffix = s;
		hasParamtr = hasPar;
	}

	public void printTree(int level, PrintWriter out)
	{
		plnDeclrtr.printTree(level, out);
		if (suffix == null)
			return;
		if (hasParamtr == true)
		{
			PrintLevel.printLevel(level+1, out);
			out.printf("()\n");
			((Paramtrs)suffix).printTree(level+2, out);
			
		}
		else
		{
			((Subscripts)suffix).printTree(level+1, out);
		}
	}
	
	public boolean hasParamtr()
	{
		return hasParamtr;
	}
}
