package compiler.ast;

import java.io.PrintWriter;

public class Subscripts extends ASTNode implements PrintTree
{
	public SqbPstfx sqbPstfx;
	public Subscripts next;

	public Subscripts(BinExpr e, Subscripts s, int l, int c)
	{
		super(l, c);
		sqbPstfx = new SqbPstfx(e, l, c);
		next = s;
	}

	public void printTree(int level, PrintWriter out)
	{
		sqbPstfx.printTree(level, out);
		if (next != null)
			next.printTree(level+1, out);
	}
}
