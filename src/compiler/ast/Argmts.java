package compiler.ast;

import java.io.PrintWriter;

public class Argmts extends ASTNode implements PrintTree
{
	public BinExpr assExpr;
	public Argmts next;

	public Argmts(BinExpr e, Argmts n, int l, int c)
	{
		super(l, c);
		assExpr = e;
		next = n;
	}

	public void printTree(int level, PrintWriter out)
	{
		assExpr.printTree(level, out);
		if (next != null)
			next.printTree(level, out);
	}
}
