package compiler.ast;

import java.io.PrintWriter;

public class PlnDecl extends ASTNode implements PrintTree
{
	public TypSpec typSpec;
	public Declrtr declrtr;

	public PlnDecl(TypSpec t, Declrtr d, int l, int c)
	{
		super(l, c);
		typSpec = t;
		declrtr = d;
	}

	public void printTree(int level, PrintWriter out)
	{
		typSpec.printTree(level, out);
		declrtr.printTree(level+1, out);
	}
}
