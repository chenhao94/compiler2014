package compiler.ast;

import java.io.PrintWriter;

public class ArgPstfx extends Pstfx implements PrintTree
{
	public Argmts argmts;

	public ArgPstfx(Argmts a, int l, int c)
	{
		super(l, c);
		argmts = a;
	}

	public void printTree(int level, PrintWriter out)
	{
		PrintLevel.printLevel(level, out);
		out.println("()");
		if (argmts != null)
			argmts.printTree(level+1, out);
	}
}
