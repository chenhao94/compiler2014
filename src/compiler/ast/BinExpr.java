package compiler.ast;

import java.io.PrintWriter;

public class BinExpr extends Expr implements PrintTree
{
	public Expr expr1, expr2;
	public OpType opType;

	public static enum OpType
	{
		COM, ASS, MUAS, DVAS, MDAS, ADDAS, SBAS, SLAS, SRAS, ANDAS, XRAS, ORAS, OR, AND, BAR, XOR, AMP, EQ, NE, LT, GT, LE, GE, SHL, SHR, PLUS, MINUS, MUL, DIV, MOD, NOP
	}

	public BinExpr(Expr e, int l, int c)
	{
		super(l, c);
		expr1 = null;
		expr2 = e;
		opType = OpType.NOP;
	}

	public BinExpr(Expr e1, OpType op, Expr e2, int l, int c)
	{
		super(l, c);
		expr1 = e1;
		opType = op;
		expr2 = e2;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (opType == OpType.NOP)
		{
			expr2.printTree(level, out);
		}
		else
		{
			PrintLevel.printLevel(level, out);
			switch (opType)
			{
				case COM: out.printf("COM\n");break;
				case ASS: out.printf("ASS\n");break;
				case MUAS: out.printf("MUAS\n");break;
				case DVAS: out.printf("DVAS\n");break;
				case MDAS: out.printf("MDAS\n");break;
				case ADDAS: out.printf("ADDAS\n");break;
				case SBAS: out.printf("SBAS\n");break;
				case SLAS: out.printf("SLAS\n");break;
				case SRAS: out.printf("SRAS\n");break;
				case ANDAS: out.printf("ANDAS\n");break;
				case XRAS: out.printf("XRAS\n");break;
				case ORAS: out.printf("ORAS\n");break;
				case OR: out.printf("OR\n");break;
				case AND: out.printf("AND\n");break;
				case BAR: out.printf("BAR\n");break;
				case XOR: out.printf("XOR\n");break;
				case AMP: out.printf("AMP\n");break;
				case EQ: out.printf("EQ\n");break;
				case NE: out.printf("NE\n");break;
				case LT: out.printf("LT\n");break;
				case GT: out.printf("GT\n");break;
				case LE: out.printf("LE\n");break;
				case GE: out.printf("GE\n");break;
				case SHL: out.printf("SHL\n");break;
				case SHR: out.printf("SHR\n");break;
				case PLUS: out.printf("PLUS\n");break;
				case MINUS: out.printf("MINUS\n");break;
				case MUL: out.printf("MUL\n");break;
				case DIV: out.printf("DIV\n");break;
				case MOD: out.printf("MOD\n");break;
			}
			expr1.printTree(level+1, out);
			expr2.printTree(level+1, out);
		}
	}

	@Override
	public boolean lvalue() {
		if (opType == OpType.NOP)
			return expr2.lvalue();
		else
		{
			switch (opType)
			{
				case COM: return expr2.lvalue();
				case ASS:  case MUAS:  case DVAS:  case MDAS:  case ADDAS:  
					case SBAS:  case SLAS: case SRAS:  case ANDAS:  case XRAS:  case ORAS:
						return true;
				case OR:  case AND:  case BAR:  case XOR:  case AMP:
					case EQ:  case NE:  case LT:  case GT:  case LE:  case GE: 
						case SHL:  case SHR:  case PLUS:  case MINUS:  case MUL:  case DIV:  case MOD:
							return false;
			}
			return false;
		}
	}

}
