package compiler.ast;

import java.io.PrintWriter;

public interface PrintTree
{
	void printTree(int level, PrintWriter out);
}

