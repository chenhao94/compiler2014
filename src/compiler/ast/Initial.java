package compiler.ast;

import java.io.PrintWriter;

import compiler.IR.GlobalIRs;
import compiler.IR.StringIR;
import compiler.codegen.MemAssign;

public class Initial extends ASTNode implements PrintTree
{
	public Object object;
	private int value;

	public Initial(Object obj, int l, int c)
	{
		super(l, c);
		object = obj;
		value = 0;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (object instanceof BinExpr)
			((BinExpr)object).printTree(level, out);
		else
			((Initials)object).printTree(level, out);
	}
	
	public void setValue(Object _value)
	{
		if (_value instanceof Integer)
			value = ((Integer)_value).intValue();
		else if (_value instanceof Character)
			value = (int)((Character)_value).charValue();
		else// String
		{	
			StringIR string = StringIR.getInstance((String)_value);
			MemAssign.staticMemAssign(string);
			if (!string.isDeclared())
			{
				GlobalIRs.getInstance().addIR(string);
				string.declare();
			}
			value = string.getLoc();
		}
	}
	
	public int getValue()
	{
		return value;
	}
}
