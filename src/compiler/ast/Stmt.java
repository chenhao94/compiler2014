package compiler.ast;

import java.io.PrintWriter;

abstract public class Stmt extends ASTNode implements PrintTree
{
	public Stmt(int l, int c)
	{
		super(l, c);
	}
	
	abstract public void printTree(int i, PrintWriter out);
}

