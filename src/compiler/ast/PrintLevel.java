package compiler.ast;

import java.io.PrintWriter;

public class PrintLevel
{
	public static void printLevel(int level, PrintWriter out)
	{
		for (int i = 1; i <= level; ++i)
			out.printf("\t");
	}
}
