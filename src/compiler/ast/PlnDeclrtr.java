package compiler.ast;

import java.io.PrintWriter;

import compiler.symbol.Symbol;

public class PlnDeclrtr extends ASTNode implements PrintTree
{
	public Symbol symbol;
	public int starCnt;

	public PlnDeclrtr(Symbol s, int l, int c)
	{
		super(l, c);
		symbol = s;
		starCnt = 0;
	}

	public void printTree(int level, PrintWriter out)
	{
		if (starCnt > 0)
		{
			PrintLevel.printLevel(level, out);
			out.printf("%d*\n", starCnt);
			PrintLevel.printLevel(level+1, out);
		}
		else
			PrintLevel.printLevel(level, out);
		out.println(symbol.toString());
	}
}
