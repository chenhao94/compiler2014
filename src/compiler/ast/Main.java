package compiler.ast;

import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import compiler.syntax.*;

public class Main
{
	public static void main(String[] args) throws IOException
	{
		Program program;
		try
		{
			program = buildAST("/home/ch94/Data.c");
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
			return;
		}
		PrintWriter out = new PrintWriter("/home/ch94/Data.ast");

		program.printTree(0, out);

		out.close();
	}

	private static Program buildAST(String filename) throws IOException
	{
		InputStream inp = new FileInputStream(filename);

		parser parser = new parser(inp);
		java_cup.runtime.Symbol parseTree = null;
		try
		{
			parseTree = parser.parse();
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		finally
		{
			inp.close();
		}

		return (Program) parseTree.value;
	}
}
