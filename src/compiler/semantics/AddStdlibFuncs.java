package compiler.semantics;

import compiler.symbol.Env;

public class AddStdlibFuncs {

	public static void addStdlibFuncs()
	{
		java.util.LinkedList<Type> argList;
		Type funcType;
		
		//-------------malloc----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(IntType.getInstance());
		funcType = FuncType.genStdLibFunc(new PtrType(VoidType.getInstance()), argList, false);
		Env.putToStdLib("malloc", funcType, null);
		//-------------getchar----------------
		argList = new java.util.LinkedList<Type> ();
		funcType = FuncType.genStdLibFunc(IntType.getInstance(), argList, false);
		Env.putToStdLib("getchar", funcType, null);
		//-------------putchar----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(IntType.getInstance());
		funcType = FuncType.genStdLibFunc(IntType.getInstance(), argList, false);
		Env.putToStdLib("putchar", funcType, null);
		//-------------strcpy----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(new PtrType(CharType.getInstance()));
		argList.add(new PtrType(CharType.getInstance()));
		funcType = FuncType.genStdLibFunc(new PtrType(CharType.getInstance()), argList, false);
		Env.putToStdLib("strcpy", funcType, null);		
		//-------------strcmp----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(new PtrType(CharType.getInstance()));
		argList.add(new PtrType(CharType.getInstance()));
		funcType = FuncType.genStdLibFunc(IntType.getInstance(), argList, false);
		Env.putToStdLib("strcmp", funcType, null);		
		//-------------strcat----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(new PtrType(CharType.getInstance()));
		argList.add(new PtrType(CharType.getInstance()));
		funcType = FuncType.genStdLibFunc(new PtrType(CharType.getInstance()), argList, false);
		Env.putToStdLib("strcat", funcType, null);		
		//-------------printf----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(new PtrType(CharType.getInstance()));
		funcType = FuncType.genStdLibFunc(IntType.getInstance(), argList, true);
		Env.putToStdLib("printf", funcType, null);		
		//-------------scanf----------------
		argList = new java.util.LinkedList<Type> ();
		argList.add(new PtrType(CharType.getInstance()));
		funcType = FuncType.genStdLibFunc(IntType.getInstance(), argList, true);
		Env.putToStdLib("scanf", funcType, null);
	}
}
