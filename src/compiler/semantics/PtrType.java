package compiler.semantics;

public class PtrType extends Type {
	
	public Type type;
	
	public PtrType(Type t)
	{
		type = t;
	}
	
	@Override
	public int getSize() {
		return 4;
	}

	@Override
	public boolean isComplete() {
		return true;
	}
	
	public boolean equals(Object t)
	{
		if (!(t instanceof PtrType))
			return false;
		return type.equals(((PtrType)t).type);
	}

	@Override
	public boolean isIntegerType() {
		return false;
	}

	@Override
	public boolean isPtrType() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isScalarType() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean compatible(Type type) {
		return (type.isIntegerType() || type.isPtrType());
	}

	@Override
	public boolean noWarning(Type type) {
		return equals(type);
	}

	@Override
	public int alignSize() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	@Override
	public boolean isPurePtr() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public Type baseType()
	{
		Type tmp = type;
		while (tmp.isPtrType())
			tmp = ((PtrType)tmp).type;
		return tmp;
	}

	public Type sqbLevel(int sqbCnt) {
		// TODO Auto-generated method stub
		Type tmpType = this;
		
		while (sqbCnt > 0)
		{
			tmpType = ((PtrType)tmpType).type;
			--sqbCnt;
		}
		
		return tmpType;
	}
}
