package compiler.semantics;

import compiler.ast.Constant;
import compiler.ast.Expr;
import compiler.ast.IntConst;
import compiler.ast.PrmyExpr;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;
import compiler.symbol.Symbol;

public class CheckPrmyExpr {

	public static Object getIntConstValue(PrmyExpr expr, Env env)
	{
		if (expr.obj instanceof Constant)
			return CheckConstant.getIntValue((Constant)expr.obj);
		else if (expr.obj instanceof Expr)
			return CheckExpr.getIntConstValue((Expr)expr.obj, env);
		else if (expr.obj instanceof String)
			return expr.obj;//CompilationInfo.addError("Cannot convert a string into integer.", expr.line, expr.column);
		else
			CompilationInfo.addError("Have to be a valid constant here.", expr.line, expr.column);
		return 0;
	}
	
	public static Type getType(PrmyExpr expr, Env env)
	{
		if (expr.obj instanceof Symbol)
		{
			String name = ((Symbol)expr.obj).toString();
			EnvInfo envInfo = env.findBasic(name);
			if (envInfo == null)
			{
				CompilationInfo.addError("Undefined identifier: \'" + name + "\'.", expr.line, expr.column);
				return IntType.getInstance();
			}
			expr.envInfo = envInfo;
			return envInfo.type;
		}
		else if (expr.obj instanceof Constant)
		{
			if (expr.obj instanceof IntConst)
				return IntType.getInstance();
			else //	if (((PrmyExpr)expr).obj instanceof CharConst)
				return CharType.getInstance();
		}
		else if (expr.obj instanceof String)
			return new PtrType(CharType.getInstance());
		else// if (((PrmyExpr)expr).obj instanceof Expr)
			return CheckExpr.getType((Expr)expr.obj, env);
	}
}
