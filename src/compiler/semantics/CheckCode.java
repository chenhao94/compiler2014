package compiler.semantics;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import compiler.ast.Program;
import compiler.syntax.parser;

public class CheckCode {
	
	public static String nameWithoutExt;
	
	public static Program checkCode(String filename) throws IOException
	{
		int extPos = filename.lastIndexOf('.');
		String extName = filename.substring(extPos);
		nameWithoutExt = filename.substring(0,extPos);
		
		if (extPos == -1 || !extName.equals(".c"))
		{
			System.err.println("Illegal filename. (Should end up with \".c\")");
			System.exit(1);
		}
		
		InputStream inp = new FileInputStream(filename);
		parser parser = new parser(inp);
		java_cup.runtime.Symbol parseTree = null;
		
		Program program;
		
		try
		{
			parseTree = parser.parse();
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
			return null;
		}
		finally
		{
			inp.close();
		}
		
		program = (Program)parseTree.value;
		
		try
		{
			AddStdlibFuncs.addStdlibFuncs();
			CheckProgram.checkProgram(program);
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
			return null;
		}

		if (CompilationInfo.error)
			return null;
		return program;
	}

}
