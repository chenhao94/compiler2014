package compiler.semantics;

import compiler.ast.CharConst;
import compiler.ast.Constant;
import compiler.ast.IntConst;

public class CheckConstant {
	
	public static int getIntValue(Constant constant)
	{
		if (constant instanceof IntConst)
			return ((IntConst)constant).value;
		else if (constant instanceof CharConst)
			return (int)((CharConst)constant).value;
		else
		{
			CompilationInfo.addError("Cannot convert to an integer constant.", constant.line, constant.column);
			return 0;
		}
	}
}
