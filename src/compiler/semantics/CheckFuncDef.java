package compiler.semantics;

import compiler.ast.FuncDef;
import compiler.ast.PlnDeclrtr;
import compiler.symbol.Env;

public class CheckFuncDef {

	public static void checkFuncDef(FuncDef func, Env env) {
		Type returnType = CheckTypSpec.getType(func.typeSpec, env);
		Type funcType = null;
		PlnDeclrtr plnDeclrtr = func.plnDeclrtr;
		int starCnt = plnDeclrtr.starCnt;
		String name = plnDeclrtr.symbol.toString();
		
		while (starCnt>0)
		{
			returnType = new PtrType(returnType);
			--starCnt;
		}
		
		if (!(returnType.equals(VoidType.getInstance()) || returnType.isComplete()))
			CompilationInfo.addError("Return type of function must be complete.", 
								func.typeSpec.line, func.typeSpec.column);
		
		funcType = new FuncType(returnType, func.paramtrs, env);
		env.putToBasic(name, funcType,func.plnDeclrtr);
		
		func.cmpStmt.env = env = new Env(env);
		CheckParamtrs.funcDefCheck(func.paramtrs, env, 1);
		CheckCmpStmt.checkCmpStmt(func.cmpStmt, env, 0, returnType);
	}

}
