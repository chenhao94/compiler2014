package compiler.semantics;

import compiler.ast.WhileStmt;
import compiler.symbol.Env;

public class CheckWhileStmt {

	public static void checkWhileStmt(WhileStmt stmt, Env env, int loopLevel, Type returnType) {
		if (stmt.expr != null && !CheckExpr.getType(stmt.expr, env).isScalarType())
			CompilationInfo.addError("The controlling expression have to has a scalar type.",
					stmt.expr.line, stmt.expr.column);
		CheckStmt.checkStmt(stmt.stmt, env, loopLevel+1, returnType);
	}

}
