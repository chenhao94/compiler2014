package compiler.semantics;

import compiler.ast.Argmts;
import compiler.symbol.Env;

public class CheckArgmts {

	public static void checkArgmts(Argmts argmts, java.util.LinkedList<Type> list, Env env)
	{
		if (argmts == null)
			return;
		
		Type type = CheckExpr.getType(argmts.assExpr, env);
		list.add(type);
		
		checkArgmts(argmts.next, list, env);
	}
	
}
