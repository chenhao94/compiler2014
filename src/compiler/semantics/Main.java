package compiler.semantics;

import java.io.IOException;

import compiler.ast.Program;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Program program = CheckCode.checkCode(args[0]);
		
		CompilationInfo.printMsg();
		
		if (program == null)
		{
			System.err.println("Compilation Error.");
			System.exit(1);
		}
		
		System.exit(0);
	}

}
