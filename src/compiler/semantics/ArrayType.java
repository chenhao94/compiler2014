package compiler.semantics;

import compiler.ast.Expr;

public class ArrayType extends PtrType {

	int size;
	boolean complete;
	
	private ArrayType(Type t, int s)
	{
		super(t);
		if (size < 0) //incomplete
		{
			size = 0;
			complete = false;
		}
		else //complete
		{
			size = s;
			complete = t.isComplete();
		}
	}
	
	public static ArrayType getInstance(Type t)
	{
		return new ArrayType(t, -1);
	}
	
	public static ArrayType getInstance(Type t, int size, Expr expr)
	{
		if (size < 0)
		{
			CompilationInfo.addError("Size of array cannot be negative",
					expr.line, expr.column);
			size = 1;
		}
		return new ArrayType(t, size);
	}
	
	@Override
	public int getSize() {
		return size * type.getSize();
	}

	@Override
	public boolean isComplete() {
		return complete;
	}
	
	public boolean equals(Object t)
	{
		if (!(t instanceof ArrayType))
			return false;
		return (type.equals(((ArrayType)t).type) && 
				(size == ((ArrayType)t).size) && 
				(complete == ((ArrayType)t).complete));
	}

	@Override
	public boolean isIntegerType() {
		return false;
	}
	
	@Override
	public int alignSize()
	{
		return type.alignSize();
	}
	
	public int getCap()
	{
		return size;
	}
	
	@Override
	public boolean isPurePtr() {
		// TODO Auto-generated method stub
		return false;
	}

}
