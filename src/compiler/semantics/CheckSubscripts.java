package compiler.semantics;

import compiler.ast.Subscripts;
import compiler.symbol.Env;

public class CheckSubscripts {
	
	public static Type checkDeclrtr(Type t, Subscripts sub, Env env)
	{
		Type type = null;
		if (sub.next != null)
			type = checkDeclrtr(t, sub.next, env);
		else
			type = t;
		if (sub.sqbPstfx.expr == null)
			type = ArrayType.getInstance(type);
		else
			{
				int size = (int)CheckExpr.getIntConstValue(sub.sqbPstfx.expr, env);
				type = ArrayType.getInstance(type, size, sub.sqbPstfx.expr);
			}
		
		return type;
	}

}
