package compiler.semantics;

import compiler.ast.CmpStmt;
import compiler.ast.Decls;
import compiler.ast.Stmts;
import compiler.symbol.Env;

public class CheckCmpStmt {

	public static void checkCmpStmt(CmpStmt stmt, Env env, int loopLevel, Type returnType)
	{
		Decls decls = stmt.decls;
		Stmts stmts = stmt.stmts;
		while (decls != null)
		{
			CheckDecl.checkDecl(decls.decl, env);
			decls = decls.next;
		}
		while (stmts != null)
		{
			CheckStmt.checkStmt(stmts.stmt, env, loopLevel, returnType);
			stmts = stmts.next;
		}
	}
	
}
