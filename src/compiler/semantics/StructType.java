package compiler.semantics;

import compiler.ast.RecordComps;
import compiler.ast.TypSpec;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;

class EntryInfo
{
	Type type;
	String name;
	int shifting;
	
	EntryInfo(Type t, String n)
	{
		type = t;
		name = n;
		shifting = 0;
	}
}

public class StructType extends RecordType {
	
	private StructType(String n)
	{
		super(n);
	}
	
	public static StructType getInstance(String s, RecordComps comps, Env env, TypSpec typSpec)
	{
		Type type = null;
		if (comps == null)  // declaration
		{
			type = env.findRecordInAll(s).type;
			if (type == null)
				{
					type =  new StructType(s);
					env.putToRecord(s, type, typSpec);
					return (StructType)type;
				}
			else if (type instanceof StructType)
				return (StructType)type;
			else
			{
				CompilationInfo.addError("Ambigious declaration of struct \'" + s.toString() + "\'.",
						typSpec.line, typSpec.column);
				return new StructType(s);
			}
		}
		else // definition
		{
			if (s != null)
			 {
				EnvInfo envInfo = env.findRecordInThis(s);
				if (envInfo != null)
				type = envInfo.type;
				
				if (type == null)
				{
					type = new StructType(s);
					env.putToRecord(s, type, typSpec);
				}
				
				if (!(type instanceof StructType))
				{
					CompilationInfo.addError("Ambigious declaration of struct \'" + s.toString() + "\'.",
							typSpec.line, typSpec.column);
					type = new StructType(s);
				}
				
				if (type.isComplete())
				{
					CompilationInfo.addError("Redefinition of struct \'" + s.toString() + "\'.",
							typSpec.line, typSpec.column);
					return (StructType)type;
				}
			 }
			else
				type = new StructType(s);
			
			Type tmpType = null;
			compiler.ast.Declrtrs declrtrs = null;
			EntryInfo entry = null;
			((StructType)type).complete = true;
			
			int entrySize, entryAlign, structAlign = 1;
			
			while (comps != null)
			{
				tmpType = CheckTypSpec.getType(comps.typSpec, env);
				declrtrs = comps.declrtrs;
				while (declrtrs != null)
				{
					entry = CheckDeclrtr.recordCheck(tmpType, declrtrs.declrtr, env);
					
					entrySize = entry.type.getSize();
					entryAlign = entry.type.alignSize();
					structAlign = Math.max(structAlign, entryAlign);
					entry.shifting = ((StructType)type).size;
					if (entry.shifting % entryAlign != 0)
						entry.shifting += entryAlign - entry.shifting % entryAlign;
					((StructType)type).size = entry.shifting + entrySize;
					
					if (((StructType)type).entry.get(entry.name) != null)
						CompilationInfo.addError("Redeclare the members in record.", 
											declrtrs.declrtr.line, declrtrs.declrtr.column);
					((StructType)type).entry.put(entry.name, entry);
					
					declrtrs = declrtrs.next;
				}
				
				comps = comps.next;
			}
			
			((StructType)type).align = structAlign;
			
			if ( ((StructType)type).size % structAlign != 0 )
				((StructType)type).size += structAlign - ((StructType)type).size % structAlign;
			
			return (StructType)type;
		}
	}
	
	
}
