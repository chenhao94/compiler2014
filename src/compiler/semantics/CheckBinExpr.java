package compiler.semantics;

import compiler.ast.BinExpr;
import compiler.ast.BinExpr.OpType;
import compiler.symbol.Env;

public class CheckBinExpr {
	
	public static Object getIntConstValue(BinExpr expr, Env env)
	{
		if (expr.opType == BinExpr.OpType.NOP)
			return CheckExpr.getIntConstValue(expr.expr2, env);
		
		int a = (int)CheckExpr.getIntConstValue(expr.expr1, env);
		int b = (int)CheckExpr.getIntConstValue(expr.expr2, env);
		
		switch (expr.opType)
		{
			case OR			: return ((a!=0) || (b!=0))? 1:0;
			case AND			: return ((a!=0) && (b!=0))? 1:0;
			case BAR			: return a | b;
			case XOR			: return a ^ b;
			case AMP			: return a & b;
			case EQ			: return (a==b)? 1:0;
			case NE			: return (a!=b)? 1:0;
			case LT			: return (a<b)? 1:0;
			case GT			: return (a>b)? 1:0;
			case LE			: return (a<=b)? 1:0;
			case GE			: return (a>=b)? 1:0;
			case SHL			: return a << b;
			case SHR			: return a >> b;
			case PLUS		: return a + b;
			case MINUS	: return a - b;
			case MUL			: return a * b;
			case DIV			: return a / b;
			case MOD			: return a % b;
			default:
				CompilationInfo.addError("Have to be a valid constant here.", expr.line, expr.column);
				return 0;
		}
	}
	
	public static Type getType(BinExpr expr, Env env)
	{
		Type type1 = null, type2 = null;
		
		if (expr.expr1 != null)
			type1 = CheckExpr.getType(expr.expr1, env);
		else
			type1 = VoidType.getInstance();
		if (expr.expr2 != null)
			type2 = CheckExpr.getType(expr.expr2, env);
		else
			type2 = VoidType.getInstance();

		switch (expr.opType)
		{
		case COM			: return CheckExpr.getType(expr.expr2, env);
		
		case ASS: 
			if (type1 instanceof ArrayType)
				CompilationInfo.addError("Cannot reassign value to an array type variable", expr.expr1.line, expr.expr1.column);
			if (!type1.compatible(type2))
				CompilationInfo.addError("Incompatible types of assignment.", expr.expr1.line, expr.expr1.column);
			else if (!type1.noWarning(type2))
				CompilationInfo.addWarning("Incompatible assignment without cast.", expr.expr1.line, expr.expr1.column);
			if (!expr.expr1.lvalue())
				CompilationInfo.addError("Have to be a lvalue before the operator '='.", expr.expr1.line, expr.expr1.column); 
			return type1;
		
		case ADDAS: case SBAS: 
			if (type1 instanceof ArrayType)
			{
				CompilationInfo.addError("Cannot reassign value to an array type variable", expr.expr1.line, expr.expr1.column);
				return type1;
			}
			if (!expr.expr1.lvalue())
			{
				CompilationInfo.addError("Have to be a lvalue before the assignment operator.", expr.expr1.line, expr.expr1.column);
				return type1;
			}
			if (!type2.isIntegerType())
			{
				if (!(expr.opType == OpType.MINUS && type1.isPtrType() && type1.equals(type2)))
					CompilationInfo.addError("Illegal type on the right side of the compound assignment operator",expr.expr2.line, expr.expr2.column);
				return IntType.getInstance();
			}
			if (type1.isPtrType())
			{
				if (type1 instanceof ArrayType)
					CompilationInfo.addError("Cannot change the value of an array type variable", expr.expr1.line, expr.expr1.column);
				return type1;
			}
			else if (type1.isIntegerType())
				return type1;
			else
			{
				CompilationInfo.addError("Illegal type of variable on the left of the compound assignment operator.", expr.expr1.line, expr.expr1.column);
				return type1;
			}
				
		case MUAS: case DVAS: case MDAS: case SLAS: case SRAS: case ANDAS: case XRAS: case ORAS: 
			if (type1 instanceof ArrayType)
				CompilationInfo.addError("Cannot reassign value to an array type variable", expr.expr1.line, expr.expr1.column);
			if (!expr.expr1.lvalue())
				CompilationInfo.addError("Have to be a lvalue before the assignment operator.", expr.expr1.line, expr.expr1.column);
			if (!type2.isIntegerType())
				CompilationInfo.addError("Have to be an integer on the right side of the compound assignment operator",expr.expr2.line, expr.expr2.column);		
			if (!type1.isIntegerType())
				CompilationInfo.addError("Illegal type of variable on the left of the compound assignment operator.", expr.expr1.line, expr.expr1.column);
			return type1;	
			
		case OR: case AND:
			if (!(type1.isScalarType() && type2.isScalarType()))
				CompilationInfo.addError("Illegal type of operand(s) for the operator.", expr.line, expr.column);
			return IntType.getInstance();
		
		case BAR: case XOR: case AMP: case SHL: case SHR: case MUL: case DIV: case MOD:
			if (!(type1.isIntegerType() && type2.isIntegerType()))
				CompilationInfo.addError("Illegal type of operand(s) for the operator.", expr.line, expr.column);
			return IntType.getInstance();
			
		case EQ: case NE: 
			if (type1.isIntegerType())
			{
				if (type2.isPtrType())
					CompilationInfo.addWarning("Converting from pointer to integer without cast.", expr.line, expr.column);
				else if (!type2.isIntegerType())
					CompilationInfo.addError("Incompatible type of operands for the operator.", expr.line, expr.column);
			}
			else if (type1.isPtrType())
			{
				if (type2.isIntegerType())
					CompilationInfo.addWarning("Converting from integer to pointer without cast.", expr.line, expr.column);
				else
				{
					if (!type2.isPtrType())
						CompilationInfo.addError("Incompatible type of operands for the operator.", expr.line, expr.column);
					if ( !((PtrType)type1).type.equals(((PtrType)type2).type) && 
							!((PtrType)type1).type.equals(VoidType.getInstance()) &&  
							!((PtrType)type2).type.equals(VoidType.getInstance()))
						CompilationInfo.addError("Incompatible type of operands for the operator.", expr.line, expr.column);
				}
			}
			else
				CompilationInfo.addError("Illegal type of operand(s) for the operator.", expr.line, expr.column);
			return IntType.getInstance();
			
		case LT: case GT: case LE: case GE: 
			if (type1.isIntegerType())
			{
				if (!type2.isIntegerType())
					CompilationInfo.addError("Incompatible type of operands for the operator.", expr.line, expr.column);
			}
			else if (type1.isPtrType())
			{
				if (!type2.isPtrType() || !((PtrType)type1).type.equals(((PtrType)type2).type))
					CompilationInfo.addError("Incompatible type of operands for the operator.", expr.line, expr.column);
			}
			else
				CompilationInfo.addError("Illegal type of operand(s) for the operator.", expr.line, expr.column);
			return IntType.getInstance();
		
		case PLUS: case MINUS:
			if (!type1.isIntegerType() && !type2.isIntegerType())
			{
				if (expr.opType != OpType.MINUS || !type1.isPtrType() || !type1.equals(type2))
					CompilationInfo.addError("Incompatible two operands for '+' or '-'.", expr.line, expr.column);
				return IntType.getInstance();
			}
			if (type1.isPtrType())
				return type1;
			else if (type2.isPtrType())
			{
				if (expr.opType == OpType.MINUS)
					CompilationInfo.addError("Invalid operand for '-'.", expr.line, expr.column);
				return type2;
			}
			else if (type1.isIntegerType() && type2.isIntegerType())
				return IntType.getInstance();
			else
			{
				CompilationInfo.addError("Illegal type of operand(s) for the operator.", expr.line, expr.column);
				return type1;
			}
			
		case NOP:
			return type2;
		}
		
		return null;
	}
	
}
