package compiler.semantics;

import compiler.ast.JmpStmt;
import compiler.ast.JmpStmt.JmpType;
import compiler.ast.ReturnStmt;
import compiler.symbol.Env;

public class CheckJmpStmt {

	public static void checkJmpStmt(JmpStmt stmt, Env env, int loopLevel, Type returnType) {
		if (stmt.jmpType == JmpType.BREAK || stmt.jmpType == JmpType.CONTINUE)
		{
			if (loopLevel == 0)
				CompilationInfo.addError("'Break/continue' have to be in a loop.", stmt.line, stmt.column);
		}
		else //RETURN
			CheckJmpStmt.checkReturnStmt((ReturnStmt)stmt, env, loopLevel, returnType);
	}

	private static void checkReturnStmt(ReturnStmt stmt, Env env, int loopLevel, Type returnType) {
		Type type = CheckExpr.getType(stmt.expr, env);
		if (returnType.equals(VoidType.getInstance()))
		{
			if (type.equals(VoidType.getInstance()))
				return;
			CompilationInfo.addError("Return type unmatched.", 
					((ReturnStmt)stmt).expr.line, ((ReturnStmt)stmt).expr.column);
		}
		else
		{
			if (returnType.compatible(type))
				return;
			CompilationInfo.addError("Return type unmatched.", 
					((ReturnStmt)stmt).expr.line, ((ReturnStmt)stmt).expr.column);
		}
	}

}
