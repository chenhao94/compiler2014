package compiler.semantics;

import compiler.ast.RecordComps;
import compiler.ast.TypSpec;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;

public class UnionType extends RecordType {
	
	private UnionType(String n)
	{
		super(n);
	}
	
	public static UnionType getInstance(String s, RecordComps comps, Env env, TypSpec typSpec)
	{
		Type type = null;
		if (comps == null)  // declaration
		{
			type = env.findRecordInAll(s).type;
			if (type == null)
				{
					type =  new UnionType(s);
					env.putToRecord(s, type, typSpec);
					return (UnionType)type;
				}
			else if (type instanceof UnionType)
				return (UnionType)type;
			else
			{
				CompilationInfo.addError("Ambigious declaration of union \'" + s.toString() + "\'.",
						typSpec.line, typSpec.column);
				return new UnionType(s);
			}
		}
		else // definition
		{
			if (s != null)
			 {
				EnvInfo envInfo = env.findRecordInThis(s);
				if (envInfo != null)
					type = envInfo.type;
				
				if (type == null)
				{
					type = new UnionType(s);
					env.putToRecord(s, type, typSpec);
				}
				
				if (!(type instanceof UnionType))
				{
					CompilationInfo.addError("Ambigious declaration of union \'" + s.toString() + "\'.",
							typSpec.line, typSpec.column);
					type = new UnionType(s);
				}
				
				if (type.isComplete())
				{	
					CompilationInfo.addError("Redefinition of union \'" + s.toString() + "\'.",
							typSpec.line, typSpec.column);
					return (UnionType)type;
				}
			 }
			else
				type = new UnionType(s);
			
			Type tmpType = null;
			compiler.ast.Declrtrs declrtrs = null;
			EntryInfo entry = null;
			int maxSize = 0;
			((UnionType)type).complete = true;
			
			int entrySize, entryAlign, unionAlign = 1;
			
			while (comps != null)
			{
				tmpType = CheckTypSpec.getType(comps.typSpec, env);
				declrtrs = comps.declrtrs;
				while (declrtrs != null)
				{
					entry = CheckDeclrtr.recordCheck(tmpType, declrtrs.declrtr, env);
					
					entrySize = entry.type.getSize();
					entryAlign = entry.type.alignSize();
					unionAlign = Math.max(unionAlign, entryAlign);
					maxSize = Math.max(maxSize, entrySize);
					
					if (((UnionType)type).entry.get(entry.name) != null)
						CompilationInfo.addError("Redeclare the members in record.", 
								declrtrs.declrtr.line, declrtrs.declrtr.column);
					((UnionType)type).entry.put(entry.name, entry);
					
					declrtrs = declrtrs.next;
				}
				
				comps = comps.next;
			}
			
			((UnionType)type).align = unionAlign;
			((UnionType)type).size = maxSize;
			
			if ( ((UnionType)type).size % unionAlign != 0 )
				((UnionType)type).size += unionAlign - ((UnionType)type).size % unionAlign;
			
			return (UnionType)type;
		}
	}

}
