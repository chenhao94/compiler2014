package compiler.semantics;

abstract public class RecordType extends Type {
	
	String name;
	java.util.HashMap<String, EntryInfo> entry;
	int size, align;
	boolean complete;
	
	RecordType(String n)
	{
		name = n;
		entry = new java.util.HashMap<String, EntryInfo>();
		size = align = 0;
		complete = false;
	}

	public Type getEntry(String name)
	{
		EntryInfo info = entry.get(name);
		if (info == null)
			return null;
		return info.type;
	}
	
	public int getOffset(String name)
	{
		EntryInfo info = entry.get(name);
		if (info == null)
			return 0;
		return info.shifting;
	}
	
	@Override
	public int getSize() {
		return size;
	}

	@Override
	public boolean isComplete() {
		return complete;
	}

	public boolean equals(Object t)
	{
		return (this == t);
	}

	@Override
	public boolean isIntegerType() {
		return false;
	}

	@Override
	public boolean isPtrType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isScalarType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean compatible(Type type) {
		return equals(type);
	}

	@Override
	public boolean noWarning(Type type) {
		return equals(type);
	}

	@Override
	public int alignSize()
	{
		return align;
	}
	
}
