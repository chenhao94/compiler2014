package compiler.semantics;

import compiler.ast.Declrtr;
import compiler.ast.Paramtrs;
import compiler.ast.Subscripts;
import compiler.symbol.Env;

public class CheckDeclrtr {
	
	public static EntryInfo recordCheck(Type t, Declrtr declrtr, Env env)
	{
		int starCnt = declrtr.plnDeclrtr.starCnt;
		String name = declrtr.plnDeclrtr.symbol.toString();
		Type type = null;
		
		if (declrtr.hasParamtr())
		{
			CompilationInfo.addError("Cannot have parameters inside the definition of struct/union", 
								((Paramtrs)declrtr.suffix).line, ((Paramtrs)declrtr.suffix).column);
			return new EntryInfo(IntType.getInstance(), name);
		}
		
		while (starCnt>0)
		{
			t = new PtrType(t);
			--starCnt;
		}
		
		if (declrtr.suffix == null)
			type = t;
		else
			type = CheckSubscripts.checkDeclrtr(t, (Subscripts)declrtr.suffix, env);
		
		return new EntryInfo(type, name);
	}

	public static Type paramtrsCheck(Type t, Declrtr declrtr, Env env)
	{
		int starCnt = declrtr.plnDeclrtr.starCnt;
		Type type = null;
		
		if (declrtr.hasParamtr())
		{
			CompilationInfo.addError("No function type pointer support now.", 
					((Paramtrs)declrtr.suffix).line, ((Paramtrs)declrtr.suffix).column);
			return IntType.getInstance();
		}
		
		while (starCnt>0)
		{
			t = new PtrType(t);
			--starCnt;
		}
		
		if (declrtr.suffix == null)
			type = t;
		else
			type = CheckSubscripts.checkDeclrtr(t, (Subscripts)declrtr.suffix, env);
		
		return type;
	}

	public static EntryInfo initCheck(Type t, Declrtr declrtr, Env env) {
		int starCnt = declrtr.plnDeclrtr.starCnt;
		String name = declrtr.plnDeclrtr.symbol.toString();
		Type type = null;
		
		while (starCnt>0)
		{
			t = new PtrType(t);
			--starCnt;
		}
		
		if (declrtr.hasParamtr())
		 type = new FuncType(t, (Paramtrs)declrtr.suffix, env);
		else if (declrtr.suffix == null)
			type = t;
		else
			type = CheckSubscripts.checkDeclrtr(t, (Subscripts)declrtr.suffix, env);
		
		return new EntryInfo(type, name);
	}
}
