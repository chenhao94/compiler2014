package compiler.semantics;

import compiler.ast.IfStmt;
import compiler.symbol.Env;

public class CheckIfStmt {

	public static void checkIfStmt(IfStmt stmt, Env env, int loopLevel, Type returnType) {
		if (!CheckExpr.getType(stmt.expr, env).isScalarType())
			CompilationInfo.addError("Have to be a scalar type expression after 'if'.",
								stmt.expr.line, stmt.expr.column);
		CheckStmt.checkStmt(stmt.stmt, env, loopLevel, returnType);
		if (stmt.alter != null)
		CheckStmt.checkStmt(stmt.alter, env, loopLevel, returnType);
	}

}
