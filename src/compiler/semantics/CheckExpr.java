package compiler.semantics;

import compiler.ast.Expr;
import compiler.ast.BinExpr;
import compiler.ast.CstExpr;
import compiler.ast.UnaryExpr;
import compiler.ast.PstfxExpr;
import compiler.ast.PrmyExpr;
import compiler.symbol.Env;

public class CheckExpr {
	
	public static Object getIntConstValue(Expr expr, Env env)
	{
		if (expr instanceof BinExpr)
			return CheckBinExpr.getIntConstValue((BinExpr)expr, env);
		else if (expr instanceof CstExpr)
			return CheckCstExpr.getIntConstValue((CstExpr)expr, env);
		else if (expr instanceof UnaryExpr)
			return CheckUnaryExpr.getIntConstValue((UnaryExpr)expr, env);
		else if (expr instanceof PstfxExpr)
			return CheckPstfxExpr.getIntConstValue((PstfxExpr)expr, env);
		else if (expr instanceof PrmyExpr)
			return CheckPrmyExpr.getIntConstValue((PrmyExpr)expr, env);
		else
			CompilationInfo.addError("Have to be a valid constant here.", expr.line, expr.column);
		return 0;
	}

	public static Type getType(Expr expr, Env env)
	{
		if (expr == null)
			return VoidType.getInstance();
		if (expr instanceof BinExpr)
			return CheckBinExpr.getType((BinExpr)expr, env);
		else if (expr instanceof CstExpr)
			return CheckCstExpr.getType((CstExpr)expr, env);
		else if (expr instanceof UnaryExpr)
			return CheckUnaryExpr.getType((UnaryExpr)expr, env);
		else if (expr instanceof PstfxExpr)
			return CheckPstfxExpr.getType((PstfxExpr)expr, env);
		else	// if (expr instanceof PrmyExpr)
			return CheckPrmyExpr.getType((PrmyExpr)expr, env);
	}
	
}
