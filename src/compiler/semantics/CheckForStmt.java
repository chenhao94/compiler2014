package compiler.semantics;

import compiler.ast.ForStmt;
import compiler.symbol.Env;

public class CheckForStmt {

	public static void checkForStmt(ForStmt stmt, Env env, int loopLevel, Type returnType) {
		CheckExpr.getType(stmt.expr1, env);
		Type controllingType = CheckExpr.getType(stmt.expr2, env);
		if (controllingType != null && !controllingType.isScalarType())
			CompilationInfo.addError("The controlling expression have to has a scalar type.", 
								stmt.expr2.line, stmt.expr2.column);
		CheckExpr.getType(stmt.expr3, env);
		CheckStmt.checkStmt(stmt.stmt, env, loopLevel+1, returnType);
	}

}
