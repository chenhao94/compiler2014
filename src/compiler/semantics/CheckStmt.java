package compiler.semantics;

import compiler.ast.CmpStmt;
import compiler.ast.Expr;
import compiler.ast.ForStmt;
import compiler.ast.IfStmt;
import compiler.ast.JmpStmt;
import compiler.ast.Stmt;
import compiler.ast.WhileStmt;
import compiler.symbol.Env;

public class CheckStmt {
	
	public static void checkStmt(Stmt stmt, Env env, int loopLevel, Type returnType)
	{
		if (stmt instanceof Expr)
			CheckExpr.getType(((Expr)stmt), env);
		else if (stmt instanceof CmpStmt)
		{
			((CmpStmt)stmt).env = env = new Env(env);
			CheckCmpStmt.checkCmpStmt((CmpStmt)stmt, env, loopLevel, returnType);
		}
		else if (stmt instanceof IfStmt)
			CheckIfStmt.checkIfStmt((IfStmt)stmt, env, loopLevel, returnType);
		//		ItrStmt
		else if (stmt instanceof ForStmt)
			CheckForStmt.checkForStmt((ForStmt)stmt, env, loopLevel, returnType);
		else if (stmt instanceof WhileStmt)
			CheckWhileStmt.checkWhileStmt((WhileStmt)stmt, env, loopLevel, returnType);
		else if (stmt instanceof JmpStmt)
			CheckJmpStmt.checkJmpStmt((JmpStmt)stmt, env, loopLevel, returnType);
		else
			CompilationInfo.addError("Unexpected statement.", stmt.line, stmt.column);
	}

}
