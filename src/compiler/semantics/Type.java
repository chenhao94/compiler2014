package compiler.semantics;

public abstract class Type
{
	abstract public int getSize();
	
	abstract public boolean isComplete();
	
	abstract public boolean isIntegerType();
	
	abstract public boolean isPtrType();
	
	abstract public boolean isScalarType();
	
	abstract public boolean compatible(Type type);

	abstract public boolean noWarning(Type type);
	
	abstract public int alignSize();
	
	public boolean isPurePtr()
	{
		return false;
	}

}