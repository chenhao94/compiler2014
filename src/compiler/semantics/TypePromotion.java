package compiler.semantics;

public class TypePromotion {

	public static Type promote(Type type1, Type type2) {
		// TODO Auto-generated method stub
		if (type1 instanceof CharType)
			return type2;
		
		if (type1.isIntegerType() && type2.isPtrType())
			return type2;
		
		return type1;
	}

}
