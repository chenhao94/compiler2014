package compiler.semantics;

class ExceptionInfo
{
	String msg;
	int line, column;
	
	ExceptionInfo(String m, int l, int c)
	{
		msg = m;
		line = l;
		column = c;
	}
}

public class CompilationInfo {
	
	public static boolean error = false;
	public static java.util.ArrayList<ExceptionInfo> errorList = new java.util.ArrayList<ExceptionInfo>();
	public static java.util.ArrayList<ExceptionInfo> warningList = new java.util.ArrayList<ExceptionInfo>();
	
	public static void addError(String s, int line, int column)
	{
		error = true;
		errorList.add(new ExceptionInfo(s, line+1, column+1));
	}
	
	public static void addWarning(String s, int line, int column)
	{
		warningList.add(new ExceptionInfo(s, line+1, column+1));
	}

	public static void printMsg() {
		for (ExceptionInfo info : errorList)
		{
			System.err.printf("Error on :%d:%d: ", info.line, info.column);
			System.err.println(info.msg);
		}
		
		for (ExceptionInfo info : warningList)
		{
			System.err.printf("Warning on %d:%d: ", info.line, info.column);
			System.err.println(info.msg);
		}
	}
	
}
