package compiler.semantics;

import compiler.ast.InitDeclrtr;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;

public class CheckInitDeclrtr {

	public static void checkInitDeclrtr(Type type, InitDeclrtr initDeclrtr, 	Env env) {
		EntryInfo entry = CheckDeclrtr.initCheck(type, initDeclrtr.declrtr, env);
		EnvInfo info = null;
		info = env.putToBasic(entry.name, entry.type, initDeclrtr.declrtr);
		initDeclrtr.type = entry.type;
		if (initDeclrtr.initial != null)
		{
			CheckInitial.checkInitial(entry.type, initDeclrtr.initial , env);
			if (info.init == true)
				CompilationInfo.addError("Cannot be re-initialized.", 
						initDeclrtr.declrtr.line, initDeclrtr.declrtr.column);
			info.init = true;
		}
		initDeclrtr.envInfo = info;
	}

	public static void checkGlobalInitDeclrtr(Type type,
			InitDeclrtr initDeclrtr, Env env) {
		// TODO Auto-generated method stub
		EntryInfo entry = CheckDeclrtr.initCheck(type, initDeclrtr.declrtr, env);
		EnvInfo info = null;
		info = env.putToBasic(entry.name, entry.type, initDeclrtr.declrtr);
		initDeclrtr.type = entry.type;
		if (initDeclrtr.initial != null)
		{
			CheckInitial.checkGlobalInitial(entry.type, initDeclrtr.initial , env);
			if (info.init == true)
				CompilationInfo.addError("Cannot be re-initialized.", 
						initDeclrtr.declrtr.line, initDeclrtr.declrtr.column);
			info.init = true;
		}
		initDeclrtr.envInfo = info;
	}

}
