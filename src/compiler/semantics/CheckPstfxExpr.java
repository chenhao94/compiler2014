package compiler.semantics;

import compiler.ast.ArgPstfx;
import compiler.ast.DotPstfx;
import compiler.ast.OpPstfx;
import compiler.ast.Pstfx;
import compiler.ast.PstfxExpr;
import compiler.ast.PtrPstfx;
import compiler.ast.SqbPstfx;
import compiler.symbol.Env;

public class CheckPstfxExpr {

	public static Object getIntConstValue(PstfxExpr expr, Env env)
	{
		if (expr.pstfx != null)
			CompilationInfo.addError("Have to be a valid constant here.", expr.line, expr.column);
		return CheckExpr.getIntConstValue(expr.expr, env);
	}
	
	public static Type getType(PstfxExpr expr, Env env)
	{
		Type type = CheckExpr.getType(expr.expr, env);
		Pstfx pstfx = expr.pstfx;
		java.util.LinkedList<Type> list;
		
		if (pstfx != null)
		{
			if (pstfx instanceof ArgPstfx)
			{
				if (!(type instanceof FuncType))
				{
					CompilationInfo.addError("Have to be a function type here.", expr.line, expr.column);
					return type;
				}
				
				list = new java.util.LinkedList<Type>();
				CheckArgmts.checkArgmts(((ArgPstfx)pstfx).argmts, list, env);
				
				if (!((FuncType)type).match(list, ((ArgPstfx)pstfx).argmts))
					CompilationInfo.addError("Cannot match the arguments of the called function.", 
										pstfx.line, pstfx.column);
				return ((FuncType)type).returnType;	
			}
			else if (pstfx instanceof DotPstfx)
			{
				if (type instanceof RecordType)
				{
					type = ((RecordType)type).getEntry(((DotPstfx)pstfx).symbol.toString());
					if (type == null)
					{
						CompilationInfo.addError("There is no entry called \'" + 
																									((DotPstfx)pstfx).symbol.toString() +
																									"\' in the record.",
																									pstfx.line, pstfx.column);
						return IntType.getInstance();
					}
					return type;
				}
				else
					CompilationInfo.addError("Have to be a record type here.", expr.line, expr.column);		
				return IntType.getInstance();
			}
			else if (pstfx instanceof PtrPstfx)
			{
				if (!type.isPtrType())
				{
					CompilationInfo.addError("Have to be a pointer here.", expr.line, expr.column);
					return IntType.getInstance();
				}
				type = ((PtrType)type).type;
				
				if (type instanceof RecordType)
				{
					type = ((RecordType)type).getEntry(((PtrPstfx)pstfx).symbol.toString());
					if (type == null)
					{
						CompilationInfo.addError("There is no entry called \'" + 
																									((PtrPstfx)pstfx).symbol.toString() +
																									"\' in the record.",
																									pstfx.line, pstfx.column);
						return IntType.getInstance();
					}
					return type;
				}
				else
					CompilationInfo.addError("Have to be a record type pointer here.", expr.line, expr.column);		
				return IntType.getInstance();
			}
			else if (pstfx instanceof OpPstfx)
			{
				if (!expr.expr.lvalue())
					CompilationInfo.addError("Have to be a lvalue before the operator '++'/'--'", 
										expr.expr.line, expr.expr.column);
				if (type.isPtrType())
				{
					if (type instanceof ArrayType)
						CompilationInfo.addError("Cannot change the value of an array type variable", 
											expr.expr.line, expr.expr.column);
					return type;
				}
				else if (type.isIntegerType())
					return type;
				else
					CompilationInfo.addError("Have to be an arithmetic or pointer type before the operator '++'/'--'.", 
										expr.expr.line, expr.expr.column);
				return type;
			}
			else if (pstfx instanceof SqbPstfx)
			{
				if (!CheckExpr.getType(((SqbPstfx)pstfx).expr, env).isIntegerType())
					CompilationInfo.addError("Have to be an integer inside '[]'",
							((SqbPstfx)pstfx).expr.line, ((SqbPstfx)pstfx).expr.column);
				if (!type.isPtrType())
				{
					CompilationInfo.addError("Have to be a pointer before '[]'", 
							expr.expr.line, expr.expr.column);
					return type;
				}
				if (type.equals(VoidType.getInstance()))
					CompilationInfo.addError("Have to be a pointer to object type before '[]'", 
							expr.expr.line, expr.expr.column);
				expr.type = type;
				return ((PtrType)type).type;
			}
			else
				CompilationInfo.addError("Illegal postfix expression.", 
						expr.line, expr.column);
		}
		
		return type;
	}
}
