package compiler.semantics;

import java.util.LinkedList;

import compiler.ast.Paramtrs;
import compiler.symbol.Env;
import compiler.symbol.EnvInfo;

public class CheckParamtrs {
	public static void checkParamtrs(Paramtrs paramtrs, LinkedList<Type> list, Env env)
	{
		if (paramtrs == null)
			return;
		
		Type type = CheckTypSpec.getType(paramtrs.plnDecl.typSpec, env);
		type = CheckDeclrtr.paramtrsCheck(type, paramtrs.plnDecl.declrtr, env);
		if (type instanceof ArrayType)
			type = new PtrType(((ArrayType)type).type);
		list.add(type);
		
		checkParamtrs(paramtrs.next, list, env);
	}

	public static void funcDefCheck(Paramtrs paramtrs, Env env, int num) {
		if (paramtrs == null)
			return;
		
		Type type = CheckTypSpec.getType(paramtrs.plnDecl.typSpec, env);
		type = CheckDeclrtr.paramtrsCheck(type, paramtrs.plnDecl.declrtr, env);
		if (type instanceof ArrayType)
			type = new PtrType(((ArrayType)type).type);	
		
		EnvInfo envInfo = env.putToBasic(paramtrs.plnDecl.declrtr.plnDeclrtr.symbol.toString(), 
							type,paramtrs.plnDecl.declrtr.plnDeclrtr);
		envInfo.varInfo.setLoc(4 * (num - 1));
		envInfo.varInfo.setArgNum(num-1);
		funcDefCheck(paramtrs.next, env, num + 1);
	}
}
