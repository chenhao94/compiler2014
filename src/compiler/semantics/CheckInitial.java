package compiler.semantics;

import compiler.ast.BinExpr;
import compiler.ast.Initial;
import compiler.ast.Initials;
import compiler.symbol.Env;

public class CheckInitial {

	public static void checkInitial(Type type, Initial initial, Env env) {
		
		Type objType = null;
		if (initial.object instanceof BinExpr)
		{
			objType = CheckExpr.getType((BinExpr)initial.object, env);
			if (!type.compatible(objType))
				CompilationInfo.addError("Incompatible type.", initial.line, initial.column);
			else if (!type.noWarning(objType))
				CompilationInfo.addWarning("Incompatible initialization without cast.", initial.line, initial.column);
		}
		else // initials
		{
			int cnt = 0;
			Initials element = (Initials)initial.object;
			
			if (!(type instanceof ArrayType))
			{
				CompilationInfo.addError("Initializing list should only appear after an array.", initial.line, initial.column);
				return;
			}
			
			while (element != null)
			{
				CheckInitial.checkInitial(((ArrayType)type).type, element.initial, env);	
				++cnt;
				element = element.next;
			}
			
			if (type.isComplete())
			{
				if (((ArrayType)type).getCap() < cnt)
					CompilationInfo.addError("Excess the size of the array.", initial.line, initial.column);
			}
			else
			{
				((ArrayType)type).size = cnt;
				((ArrayType)type).complete = true;
			}
		}
		
	}

	public static void checkGlobalInitial(Type type, Initial initial, Env env) {
		// TODO Auto-generated method stub

		Type objType = null;
		if (initial.object instanceof BinExpr)
		{
			objType = CheckExpr.getType((BinExpr)initial.object, env);
			if (!type.compatible(objType))
				CompilationInfo.addError("Incompatible type.", initial.line, initial.column);
			else if (!type.noWarning(objType))
				CompilationInfo.addWarning("Incompatible initialization without cast.", initial.line, initial.column);
			initial.setValue(CheckExpr.getIntConstValue((BinExpr)initial.object, env));
		}
		else // initials
		{
			int cnt = 0;
			Initials element = (Initials)initial.object;
			
			if (!(type instanceof ArrayType))
			{
				CompilationInfo.addError("Initializing list should only appear after an array.", initial.line, initial.column);
				return;
			}
			
			while (element != null)
			{
				CheckInitial.checkGlobalInitial(((ArrayType)type).type, element.initial, env);	
				++cnt;
				element = element.next;
			}
			
			if (type.isComplete())
			{
				if (((ArrayType)type).getCap() < cnt)
					CompilationInfo.addError("Excess the size of the array.", initial.line, initial.column);
			}
			else
			{
				((ArrayType)type).size = cnt;
				((ArrayType)type).complete = true;
			}
		}
		
	}

}
