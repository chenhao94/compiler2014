package compiler.semantics;

public class VoidType extends Type {

	public static VoidType instance = new VoidType();
	
	private VoidType()
	{
	}
	
	@Override
	public int getSize() {
		return 0;
	}

	public static VoidType getInstance()
	{
		return instance;
	}

	@Override
	public boolean isComplete() {
		return false;
	}

	@Override
	public boolean isIntegerType() {
		return false;
	}

	@Override
	public boolean isPtrType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isScalarType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean compatible(Type type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean noWarning(Type type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int alignSize() {
		// TODO Auto-generated method stub
		return 0;
	}

}
