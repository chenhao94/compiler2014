package compiler.semantics;

import compiler.ast.CstExpr;
import compiler.symbol.Env;

public class CheckCstExpr {
	
	public static Object getIntConstValue(CstExpr expr, Env env)
	{
		Object _tmp = CheckExpr.getIntConstValue(expr.expr, env);
		if (expr.typeSpec == null)
			return _tmp;
		
		int tmp = (int)_tmp;
		Type type = CheckTypSpec.getType(expr.typeSpec, env);
		
		if (type.equals(IntType.getInstance()))
			return tmp;
		else if (type.equals(CharType.getInstance()))
			return tmp % 256;
		else
		{
			CompilationInfo.addError("Have to be a valid constant here.", expr.expr.line, expr.expr.column);
			return 0;
		}
	}
	
	public static Type getType(CstExpr expr, Env env)
	{
		Type type1 = CheckTypSpec.getType(expr.typeSpec, env);
		Type type2 = CheckExpr.getType(expr.expr, env);
		
		expr.type = type1;
		
		if (type1 == null)
			return type2;
		
		if (!(type1.isScalarType() && type2.isScalarType()))
			CompilationInfo.addError("Cannot convert (to) non-scalar type.", expr.line, expr.column);
		return type1;
	}

}
