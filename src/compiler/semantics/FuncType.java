package compiler.semantics;

import compiler.ast.Argmts;
import compiler.ast.Paramtrs;
import compiler.symbol.Env;

public class FuncType extends Type {
	
	Type returnType;
	private java.util.LinkedList<Type> arguments;
	final public boolean eclipse;
	
	public FuncType(Type r, Paramtrs p, Env env)
	{
		returnType = r;
		arguments = new java.util.LinkedList<Type>();
		CheckParamtrs.checkParamtrs(p, arguments, env);
		eclipse = false;
	}
	
	private FuncType(Type r, java.util.LinkedList<Type> list, boolean e)
	{
		returnType = r;
		arguments = list;
		eclipse = e;
	}
	
	static FuncType genStdLibFunc(Type t, java.util.LinkedList<Type> list, boolean b)
	{
		return new FuncType(t, list, b);
	}
	
	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public boolean isComplete() {
		return true;
	}
	
	public boolean equals(Object t)
	{
		if (!(t instanceof FuncType))
			return false;
		return (((FuncType)t).returnType.equals(returnType) && ((FuncType)t).arguments.equals(arguments));
	}
	
	public boolean match(java.util.LinkedList<Type> list, Argmts argmts)
	{
		int sizeDiff = list.size() - arguments.size();
		Type type1 = null, type2 = null;
		if (sizeDiff<0 || (sizeDiff>0 && eclipse == false))
		{	
			CompilationInfo.addError("Numbers of arguments mismatch in this function.",
								argmts.line, argmts.column);
			return false;
		}
		java.util.ListIterator<Type> itr1 = arguments.listIterator(0), itr2 = list.listIterator(0);
		while (itr1.hasNext() && itr2.hasNext())
		{
			type1 = itr1.next();
			type2 = itr2.next();
			if (!type1.compatible(type2))
				return false;
			else if (!type1.noWarning(type2))
				CompilationInfo.addWarning("Passing incompatible arguement without cast.",
									argmts.line, argmts.column);
			argmts = argmts.next;
		}
		return true;
	}
	
	public Type getReturnType()
	{
		return returnType;
	}
	
	public java.util.LinkedList<Type> getArguments()
	{
		return arguments;
	}

	@Override
	public boolean isIntegerType() {
		return false;
	}

	@Override
	public boolean isPtrType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isScalarType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean compatible(Type type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean noWarning(Type type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int alignSize() {
		// TODO Auto-generated method stub
		return 0;
	}

}
