package compiler.semantics;

import compiler.ast.Decl;
import compiler.ast.InitDeclrtrs;
import compiler.symbol.Env;

public class CheckDecl {

	public static void checkDecl(Decl decl, Env env) {
		Type type = CheckTypSpec.getType(decl.typeSpec, env);
		InitDeclrtrs initDeclrtrs = decl.initDeclrtrs;
		
		//decl.type = type;
		if (initDeclrtrs == null && !(type instanceof RecordType))
			CompilationInfo.addWarning("Useless declaration.", decl.line, decl.column);
		while (initDeclrtrs != null)
		{
			CheckInitDeclrtr.checkInitDeclrtr(type, initDeclrtrs.initDeclrtr, env);
			initDeclrtrs = initDeclrtrs.next;
		}
	}
	
	public static void checkGlobalDecl(Decl decl, Env env)
	{
		Type type = CheckTypSpec.getType(decl.typeSpec, env);
		InitDeclrtrs initDeclrtrs = decl.initDeclrtrs;
		
		//decl.type = type;
		if (initDeclrtrs == null && !(type instanceof RecordType))
			CompilationInfo.addWarning("Useless declaration.", decl.line, decl.column);
		while (initDeclrtrs != null)
		{
			CheckInitDeclrtr.checkGlobalInitDeclrtr(type, initDeclrtrs.initDeclrtr, env);
			initDeclrtrs = initDeclrtrs.next;
		}
	}

}
