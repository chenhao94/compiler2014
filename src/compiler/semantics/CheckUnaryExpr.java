package compiler.semantics;

import compiler.ast.UnaryExpr;
import compiler.symbol.Env;

public class CheckUnaryExpr {
	
	public static Object getIntConstValue(UnaryExpr expr, Env env)
	{
		switch (expr.opType)
		{
			case NOP: return CheckExpr.getIntConstValue(expr.expr, env);
			case POS: return (int)CheckExpr.getIntConstValue(expr.expr, env);
			case NEG: return -(int)CheckExpr.getIntConstValue(expr.expr, env);
			case TILD: return ~(int)CheckExpr.getIntConstValue(expr.expr, env);
			case EXCL: return ((int)CheckExpr.getIntConstValue(expr.expr, env) == 0) ? 1:0;
			case SIZEOF:
				if (expr.typName != null)
				{
					expr.type = CheckTypSpec.getType(expr.typName, env);
					return expr.type.getSize();
				}
				else
					CompilationInfo.addError("Have to be a valid constant here.", expr.expr.line, expr.expr.column);
			default:
				CompilationInfo.addError("Have to be a valid constant here.", expr.line, expr.column);
		}
		return 0;
	}

	public static Type getType(UnaryExpr expr, Env env)
	{
		if (expr.typName != null) // SIZEOF
		{	
			expr.type = CheckTypSpec.getType(expr.typName, env);
			return IntType.getInstance();
		}
		Type type = CheckExpr.getType(expr.expr, env);
		
		switch (expr.opType)
		{
			case PINC:
				if (!expr.expr.lvalue())
				{
					CompilationInfo.addError("Have to be a lvalue after the operator '++'",
										expr.expr.line, expr.expr.column);
					return type;
				}
				if (type.isPtrType())
				{
					if (type instanceof ArrayType)
						CompilationInfo.addError("Cannot change the value of an array type variable",
								expr.expr.line, expr.expr.column);
					return type;
				}
				if (!type.isIntegerType())
					CompilationInfo.addError("Have to be an arithmetic or pointer type after the operator '++'.",
							expr.expr.line, expr.expr.column);
				return type;
					
			case PDEC:
				if (!expr.expr.lvalue())
				{
					CompilationInfo.addError("Have to be a lvalue after the operator '--'",
							expr.expr.line, expr.expr.column);
					return type;
				}
				if (type.isPtrType())
				{
					if (type instanceof ArrayType)
						CompilationInfo.addError("Cannot change the value of an array type variable",
								expr.expr.line, expr.expr.column);
					return type;
				}
				else if (!type.isIntegerType())
					CompilationInfo.addError("Have to be an arithmetic or pointer type after the operator '--'.",
							expr.expr.line, expr.expr.column);
				return type;
					
			case AMP:
				if (!expr.expr.lvalue())
					CompilationInfo.addError("Have to be a lvalue after the operator '&'",
							expr.expr.line, expr.expr.column);
				return new PtrType(type);
					
			case STAR:
				if (type.isPtrType())
					return ((PtrType)type).type;
			case POS:
				if (!type.isIntegerType())
					CompilationInfo.addError("Have to be an arithmetic type after the operator '+'.",
							expr.expr.line, expr.expr.column);
				return IntType.getInstance();
					
			case NEG:
				if (!type.isIntegerType())
					CompilationInfo.addError("Have to be an arithmetic type after the operator '-'.",
							expr.expr.line, expr.expr.column);
				return IntType.getInstance();
					
			case TILD:
				if (!type.isIntegerType())
					CompilationInfo.addError("Have to be an integer after the operator '~'.",
							expr.expr.line, expr.expr.column);
				return IntType.getInstance();
					
			case EXCL:
				if (!type.isScalarType())
					CompilationInfo.addError("Have to be a scalar type after the operator '!'.",
							expr.expr.line, expr.expr.column);
				return IntType.getInstance();
				
			case SIZEOF:
				return IntType.getInstance();
					
			case NOP				: return type;
			
			default:
				CompilationInfo.addError("Impossible to reach here. Illegal unary expression.",
						expr.expr.line, expr.expr.column);
				return type;
		}
	}
	
}
