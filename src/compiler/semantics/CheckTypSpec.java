package compiler.semantics;

import compiler.ast.TypSpec;
import compiler.symbol.Env;

public class CheckTypSpec {
	
	static Type getType(TypSpec typSpec, Env env)
	{
		if (typSpec == null)
			return null;
		Type type = null;
		int cnt = typSpec.starCnt;
		switch (typSpec.typeID)
		{
			case INT			: type = IntType.getInstance(); break;
			case CHAR		: type = CharType.getInstance(); break;
			case VOID		: type = VoidType.getInstance(); break;
			case STRUCT	:
				if (typSpec.symbol == null)
					type = StructType.getInstance(null, typSpec.comps, env, typSpec);
				else
					type = StructType.getInstance(typSpec.symbol.toString(), typSpec.comps, env, typSpec); 
				break;
			case UNION	: 
				if (typSpec.symbol == null)
					type = UnionType.getInstance(null, typSpec.comps, env, typSpec);
				else
					type = UnionType.getInstance(typSpec.symbol.toString(), typSpec.comps, env, typSpec);
				break;
			default:  break;
		}
		while (cnt>0)
		{
			type = new PtrType(type);
			--cnt;
		}
		return type;
	}
	
}
