package compiler.semantics;

import compiler.ast.Decl;
import compiler.ast.FuncDef;
import compiler.ast.Program;
import compiler.symbol.Env;

public class CheckProgram {
	
	public static void checkProgram(Program program)
	{
		Env env = new Env(null);
		Object object = null;
		
		while (program != null)
		{
			object = program.object;
			program.env = env;
			
			if (object instanceof Decl)
				CheckDecl.checkGlobalDecl((Decl)object, env);
			else // FuncDef
				CheckFuncDef.checkFuncDef((FuncDef)object, env);
			
			program = program.next;
		}
		
	}

}
