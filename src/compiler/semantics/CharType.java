package compiler.semantics;

public class CharType extends Type {

	public static CharType instance = new CharType();
	
	private CharType()
	{
	}
	
	@Override
	public int getSize() {
		return 1;
	}

	public static CharType getInstance()
	{
		return instance;
	}

	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	public boolean isIntegerType() {
		return true;
	}

	@Override
	public boolean isPtrType() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isScalarType() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean compatible(Type type) {
		return (type.isIntegerType() || type.isPtrType());
	}

	@Override
	public boolean noWarning(Type type) {
		return type.isIntegerType();
	}

	@Override
	public int alignSize() {
		// TODO Auto-generated method stub
		return 1;
	}

}
