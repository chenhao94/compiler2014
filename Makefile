all:
	mkdir -p bin
	cp lib/stdlib bin/ -rf
	cd src/compiler/syntax && make
	javac src/compiler/*/*.java -classpath lib/java-cup-11a.jar -d bin
clean:
	cd src/compiler/syntax && make clean
	rm -rf bin
